import { browser, by, element } from 'protractor';

export class AppPage {
  public navigateTo(destination: any): any {
    return browser.get(destination);
  }

  public getTitle(): any {
    return browser.getTitle();
  }

  public getPageOneTitleText(): any {
    return element(by.tagName('app-home')).element(by.deepCss('ion-title')).getText();
  }
}
