#!/bin/sh
VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
APPNAME=`grep "<name>" config.xml | sed "s/.*<name>\(.*\)<\/name>.*/\1/"`
PROJNAME=calmio

export SENTRY_SKIP_AUTO_RELEASE=true

if [ ! -z "$BUILD_NUMBER" ]; then
  # build na jenkinsu = true
  export SENTRY_RELEASE=$VERSION-${BUILD_NUMBER}a
  ionic cordova build android --prod --device
else
  # build na jenkinsu = false
  ionic cordova build android --device # --prod
fi

mv platforms/android/app/build/outputs/apk/debug/app-debug.apk $PROJNAME-$VERSION-$BUILD_NUMBER.apk

