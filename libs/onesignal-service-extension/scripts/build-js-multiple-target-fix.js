/*
https://github.com/apache/cordova-ios/pull/956
*/
var fs = require('fs'), path = require('path');
module.exports = function (context) {
    console.log("running build-js-multiple-target-fix hook");
    var buildFile = path.join(context.opts.projectRoot, 'platforms', 'ios', 'cordova', 'lib', 'build.js');
    if (fs.existsSync(buildFile)) {
        var data = fs.readFileSync(buildFile, 'utf8');
        data = data.replace(
            "extraConfig += `PROVISIONING_PROFILE = ${buildOpts.provisioningProfile}\\n`;",
            `if (typeof buildOpts.provisioningProfile === 'string') {
                    extraConfig += \`PROVISIONING_PROFILE = \${buildOpts.provisioningProfile}\\n\`;
                } else {
                    const keys = Object.keys(buildOpts.provisioningProfile); // using keys[0] due to issue #955
                    extraConfig += \`PROVISIONING_PROFILE = \${buildOpts.provisioningProfile[keys[0]]}\\n\`;
                }`
        );
        data = data.replace(
            "exportOptions.provisioningProfiles = { [bundleIdentifier]: String(buildOpts.provisioningProfile) };",
            `if (typeof buildOpts.provisioningProfile === 'string') {
                    exportOptions.provisioningProfiles = { [bundleIdentifier]: String(buildOpts.provisioningProfile) };
                } else {
                    events.emit('log', 'Setting multiple provisioning profiles for signing');
                    exportOptions.provisioningProfiles = buildOpts.provisioningProfile;
                }`
        );
        fs.writeFileSync(buildFile, data, 'utf8');
    } else {
        throw new Error("Coudn't find buildFile.js ");
    }

}