var fs = require('fs');
var xcode = require('xcode');
var plist = require('plist');
var path = require('path');
var elementTree = require('elementtree');

module.exports = function (context) {
    console.log('Starting hook to add notification extension to project');

    var EXT_NAME = 'NotificationExtension';
    var ONESIGNAL_FRAMEWORK_DIR = '$(PROJECT_DIR)/Pods/OneSignal/iOS_SDK/OneSignalSDK/Framework';
    var EXT_FILES = [
        'NotificationService.h',
        'NotificationService.m',
        `NotificationExtension-Info.plist`,
    ];
    var extSourceDir = path.join(__dirname, '..', 'src', 'ios', EXT_NAME);
    var appBundleID = getBundleID();
    var iosFolder = path.join(context.opts.projectRoot, 'platforms', 'ios');
    var extBundleID = `${appBundleID}.${EXT_NAME}`;

    var projectFolder;
    var projectName;
    var data = fs.readdirSync(iosFolder);
    data.forEach(function (folder) {
        if (folder.match(/\.xcodeproj$/)) {
            projectFolder = path.join(iosFolder, folder);
            projectName = path.basename(folder, '.xcodeproj');
        }
    });

    var pbxProjPath = path.join(projectFolder, 'project.pbxproj');
    var projectPlistPath = path.join(iosFolder, projectName, projectName + '-Info.plist');
    var projectPlistJson = plist.parse(fs.readFileSync(projectPlistPath, 'utf8'));
    var buildJSON = JSON.parse(fs.readFileSync(
        path.join(context.opts.projectRoot, 'build.json'),
        'utf-8'
    ));

    let proj = xcode.project(pbxProjPath);

    console.log(`Adding ${EXT_NAME} notification extension to ${projectName}`);
    proj.parse(function (err) {
        if (err) {
            console.log(`Error parsing iOS project: ${JSON.parse(err)}`);
            return;
        }
        copyExtensionFiles();
        createPbxGroup();
        var target = addTarget();
        addBuildPhases(proj, target.uuid);
        addFrameworks(proj, target.uuid);
        fixBuildSetttings(proj);

        console.log('Write the changes to the iOS project file');
        fs.writeFileSync(pbxProjPath, proj.writeSync());
        console.log(`Added ${EXT_NAME} notification extension to project`);
    });

    function getBundleID() {
        var configXMLContents = fs.readFileSync(
            path.join(context.opts.projectRoot, 'config.xml'),
            'utf-8'
        );
        var etree = elementTree.parse(configXMLContents);
        return etree.getroot().get('id');
    }

    function copyExtensionFiles() {
        //Copy in the extension files
        console.log('Copying in the extension files to the iOS project');
        fs.mkdirSync(path.join(iosFolder, EXT_NAME));
        EXT_FILES.forEach(function (extFile) {
            fs.writeFileSync(path.join(iosFolder, EXT_NAME, extFile), fs.readFileSync(path.join(extSourceDir, extFile)));
        });
        // replace placeholder values in extension plist file
        var plistPath = path.join(iosFolder, EXT_NAME, EXT_NAME + '-Info.plist');
        var placeHolderValues = {
            __EXT_BUNDLE_ID__: extBundleID,
            __BUNDLE_SHORT_VERSION_STRING__: projectPlistJson['CFBundleShortVersionString'],
            __BUNDLE_VERSION__: projectPlistJson['CFBundleVersion'],
        };
        var plistContents = fs.readFileSync(plistPath, 'utf8');
        Object.keys(placeHolderValues).forEach(key => {
            var regexp = new RegExp(key, "g");
            plistContents = plistContents.replace(regexp, placeHolderValues[key]);
        })
        fs.writeFileSync(plistPath, plistContents);
    }

    function createPbxGroup() {
        // Create new PBXGroup for the extension
        console.log('Creating new PBXGroup for the extension');
        let extGroup = proj.addPbxGroup(EXT_FILES, EXT_NAME, EXT_NAME);
        // Add the new PBXGroup to the CustomTemplate group. This makes the
        // files appear in the file explorer in Xcode.
        console.log('Adding new PBXGroup to CustomTemplate PBXGroup');
        let groups = proj.hash.project.objects['PBXGroup'];
        Object.keys(groups).forEach(function (key) {
            if (groups[key].name === 'CustomTemplate') {
                proj.addToPbxGroup(extGroup.uuid, key);
            }
        });
    }

    function addTarget() {
        // Add a target for the extension
        console.log('Adding the new target');
        return proj.addTarget(EXT_NAME, 'app_extension');
    }

    function addBuildPhases(proj, uuid) {
        console.log('Adding build phases to the new target');
        proj.addBuildPhase(['NotificationService.m'], 'PBXSourcesBuildPhase', 'Sources', uuid);
        proj.addBuildPhase([], 'PBXResourcesBuildPhase', 'Resources', uuid);
        proj.addBuildPhase([], 'PBXFrameworksBuildPhase', 'Frameworks', uuid);
    }

    function addFrameworks(proj, uuid) {
        console.log('Adding frameworks to the new target');
        proj.removeFramework('SystemConfiguration.framework', { target: uuid })
        proj.addFramework('SystemConfiguration.framework', { target: uuid })
        proj.removeFramework('UIKit.framework', { target: uuid, })
        proj.addFramework('UIKit.framework', { target: uuid, })
        proj.removeFramework('WebKit.framework', { target: uuid })
        proj.addFramework('WebKit.framework', { target: uuid })
        proj.removeFramework('CoreGraphics.framework', { target: uuid, })
        proj.addFramework('CoreGraphics.framework', { target: uuid })
        proj.removeFramework(`${ONESIGNAL_FRAMEWORK_DIR}/OneSignal.framework`, { target: uuid, customFramework: true })
        proj.addFramework(`${ONESIGNAL_FRAMEWORK_DIR}/OneSignal.framework`, { target: uuid, customFramework: true })
    }

    function fixBuildSetttings(proj) {
        console.log('Adding framework search paths and signing to the new target');
        var config = proj.hash.project.objects['XCBuildConfiguration'];
        for (var ref in config) {
            if (config[ref].buildSettings && config[ref].buildSettings.PRODUCT_NAME && config[ref].buildSettings.PRODUCT_NAME.includes(EXT_NAME)) {
                var buildSettings = config[ref].buildSettings;
                var isRelease = config[ref].name === 'Release';
                var INHERITED = '"$(inherited)"';
                if (!buildSettings['FRAMEWORK_SEARCH_PATHS'] || buildSettings === INHERITED) {
                    buildSettings['FRAMEWORK_SEARCH_PATHS'] = [INHERITED];
                    buildSettings['FRAMEWORK_SEARCH_PATHS'].push(`"${ONESIGNAL_FRAMEWORK_DIR}"`);
                }
                buildSettings.PROVISIONING_PROFILE = isRelease ? buildJSON.ios.release.provisioningProfile[extBundleID] : buildJSON.ios.debug.provisioningProfile[extBundleID];
                buildSettings.DEVELOPMENT_TEAM = isRelease ? buildJSON.ios.release.developmentTeam : buildJSON.ios.debug.developmentTeam;
                buildSettings.CODE_SIGN_IDENTITY = isRelease ? '"iPhone Distribution"' : '"iPhone Developer"';
                buildSettings.PRODUCT_BUNDLE_IDENTIFIER = extBundleID;
                buildSettings.TARGETED_DEVICE_FAMILY = '"1,2"';
                buildSettings.IPHONEOS_DEPLOYMENT_TARGET = '11.0';
            }
        }
    }
}