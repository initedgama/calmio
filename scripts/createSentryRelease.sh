#!/bin/sh

if [ ! -z "$SENTRY_RELEASE" ]; then
    export SENTRY_PROPERTIES=./sentry.properties
    npx sentry-cli releases new $SENTRY_RELEASE --finalize
    npx sentry-cli releases files $SENTRY_RELEASE upload-sourcemaps ./www --dist=$SENTRY_RELEASE
    npx sentry-cli releases set-commits $SENTRY_RELEASE --auto --ignore-empty --ignore-missing
fi
