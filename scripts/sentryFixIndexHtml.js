module.exports = function (ctx) {
    const path = require('path');
    const fs = require('fs');
    console.log(`sentryFixIndexHtml hook running`, ctx.hook);

    const buildPaths =
        ctx.opts.paths ||
        ctx.opts.platforms
            .map(p => {
                // `require` needs absolute file path to prevent "Cannot find module", and
                // working directory is set to project root by upstream tools.
                const apiPath = path.join(process.cwd(), 'platforms', p, 'cordova', 'Api.js');
                if (!fs.existsSync(apiPath)) {
                    console.error(`Sentry: unable to locate build path for platform '${p}'`);
                    return;
                }
                const PlatformApi = require(apiPath);
                const platformApi = new PlatformApi();
                return platformApi.getPlatformInfo().locations.www;
            })
            .filter(x => x);

    for (let buildPath of buildPaths) {

        const indexHtml = path.join(buildPath, 'index.html');
        if (!fs.existsSync(indexHtml)) {
            console.error('sentryFixIndexHtml: index.html does not exist, skipping');
            return;
        }
        const release = process.env.SENTRY_RELEASE;
        if (!release) {
            console.error('sentryFixIndexHtml: no SENTRY_RELEASE variable, skipping');
            return;
        }

        const regex = /<head>(?:[\s\S]*(<!-- sentry-cordova -->))?/g;
        let contents = fs.readFileSync(indexHtml, {
            encoding: 'utf-8',
        });
        const releaseSentry = `
<script>
(function(w){var i=w.SENTRY_RELEASE=w.SENTRY_RELEASE||{};i.id='${release}';})(this);
</script>
<!-- sentry-cordova -->`;
        const replaceWith = `<head>${releaseSentry}`;
        fs.writeFileSync(indexHtml, contents.replace(regex, replaceWith));

        // This is allowed to fail because it's ionic specific
        const projectRootIndexHtml = path.join(ctx.opts.projectRoot, 'src', 'index.html');
        if (fs.existsSync(projectRootIndexHtml)) {
            contents = fs.readFileSync(projectRootIndexHtml, {
                encoding: 'utf-8',
            });
            fs.writeFileSync(projectRootIndexHtml, contents.replace(regex, replaceWith));
        }
        
        console.error('sentryFixIndexHtml: written release ' + release + ' to ' + indexHtml);
    }
    
}
