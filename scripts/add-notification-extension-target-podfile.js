#!/usr/bin/env node

// INITED: Pridani NotificationExtension targetu do Podfile.
// INITED: Diky tomu je mozne v extension pouzivat OneSignal knihovnu.

console.log("running add-notification-extension-target-podfile hook");

const fs = require("fs");
const path = require("path");

module.exports = (context) => {
  const platformPath = path.resolve(context.opts.projectRoot, "platforms/ios");
  const podfilePath = path.resolve(platformPath, "Podfile");

  if (!fs.existsSync(podfilePath)) {
    console.log(`'${podfilePath}' does not exist. Firebase deployment fix skipped.`);
    return;
  }

  let podfileContent = fs.readFileSync(podfilePath, "utf-8");
  if (podfileContent.indexOf("NotificationsExtension") == -1) {
    podfileContent += `
target 'NotificationExtension' do
  pod 'OneSignal', '3.12.9'
end

`;

    fs.writeFileSync(podfilePath, podfileContent, "utf-8");
  }
};

