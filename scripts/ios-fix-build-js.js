/*
https://github.com/apache/cordova-ios/issues/659
https://github.com/dpa99c/cordova-plugin-firebasex#cordova-cli-builds 
*/
var fs = require('fs'), path = require('path');
module.exports = function (context) {
    console.log("running ios-fix-build-js hook");
    var buildFile = path.join(context.opts.projectRoot, 'platforms', 'ios', 'cordova', 'lib', 'build.js');
    if (fs.existsSync(buildFile)) {
        var data = fs.readFileSync(buildFile, 'utf8');
        data = data.replace(
            "customArgs.configuration_build_dir || `CONFIGURATION_BUILD_DIR=${path.join(projectPath, 'build', 'device')}`",
            "//customArgs.configuration_build_dir || `CONFIGURATION_BUILD_DIR=${path.join(projectPath, 'build', 'device')}`"
        );
        fs.writeFileSync(buildFile, data, 'utf8');
    } else {
        throw new Error("Coudn't find build.js ");
    }

}