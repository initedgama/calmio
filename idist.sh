#!/bin/sh -x

VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
export SENTRY_SKIP_AUTO_RELEASE=true
export SENTRY_RELEASE=$VERSION-idist

cd platforms/ios
pod install
cd ../..

security unlock-keychain -p h login.keychain
ionic cordova build --prod --release --device ios

mv platforms/ios/build/device/*.ipa $(pwd)/release.ipa
