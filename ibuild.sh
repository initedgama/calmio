#!/bin/sh
VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
APPNAME=`grep "<name>" config.xml | sed "s/.*<name>\(.*\)<\/name>.*/\1/"`
PROJNAME=calmio

export SENTRY_SKIP_AUTO_RELEASE=true

cd platforms/ios
pod install
cd ../..
security unlock-keychain -p h

if [ ! -z "$BUILD_NUMBER" ]; then
  # build na jenkinsu = true
  export SENTRY_RELEASE=$VERSION-${BUILD_NUMBER}i
  ionic cordova build ios --prod --device
else
  # build na jenkinsu = false
  ionic cordova build ios --device # --prod
fi

mv platforms/ios/build/device/*.ipa $PROJNAME-$VERSION-$BUILD_NUMBER.ipa

