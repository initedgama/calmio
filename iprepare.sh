#!/bin/sh
export SENTRY_SKIP_WIZARD=true

rm -rf platforms plugins node_modules www
mkdir platforms plugins www
npm install
npm prune
cordova platform add ios

# fix swiperu ve starem Ionicu - prekopirovano z nove verze @ionic/core - https://github.com/nolimits4web/swiper/issues/3275#issuecomment-562113342
cp libs/swiper/swiper.bundle-dedc14bc.js node_modules/@ionic/core/dist/cjs/
cp libs/swiper/swiper.bundle-8bab85e6.js node_modules/@ionic/core/dist/esm/
cp libs/swiper/swiper.bundle-8bab85e6.js node_modules/@ionic/core/dist/esm-es5/

# sentry wizard je nutne spustit po inicializaci projektu, jinak se nezalozi spravne build step skripty - https://github.com/getsentry/sentry-cordova/issues/86
./node_modules/.bin/sentry-wizard --quiet true --integration cordova --platform ios --skip-connect true