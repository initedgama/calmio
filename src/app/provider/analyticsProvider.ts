import { Injectable } from '@angular/core';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Router, NavigationEnd } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { AnalyticsEvent } from '../pojo/analyticsEvent';
import { AnalyticsEventsEnum } from '../pojo/analyticsEventsEnum';
import { AnalyticsUserPropertyEnum } from '../pojo/analyticsUserPropertyEnum';
import { Subject } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { ApiProvider } from './apiProvider';
import { environment } from 'src/environments/environment';
import { PlatformService } from '../service/PlatformService';

const ANALYTICS_SCREEN_NAMES = {
  intro: 'onboarding_uvodni',
  about: 'O aplikaci',
  'course-detail': 'kurz_detail_odemceny',
  'course-purchase': 'kurz_detail_zamceny',
  'full-version': 'Plná verze',
  list: 'Seznam kurzů',
  playback: 'Přehrávač',
  profile: 'Profil uživatele',
  'sign-in': 'onboarding_prihlaseni',
  'sign-up': 'onboarding_registrace',
  terms: 'Podmínky používání',
  'onboarding-0': 'onboarding_ahoj',
  'onboarding-1': 'onboarding_zasady_meditace',
  'onboarding-2': 'onboarding_predplatne',
  'onboarding-3': 'onboarding_zkusime_prvni_lekci',
};

@Injectable()
export class AnalyticsProvider {

  private reportActivitySubject: Subject<void> = new Subject();


  constructor(
    private fa: FirebaseAnalytics,
    private platform: PlatformService,
    private router: Router,
    private apiProvider: ApiProvider
  ) {
  }

  public async init(): Promise<void> {
    try {
      console.log('Analytics init');
      if (this.platform.is('cordova')) {
        await this.fa.setEnabled(environment.production);
        await this.fa.setUserId('');
        this.logEvent({ name: AnalyticsEventsEnum.APP_OPEN });
      }
    } catch (e) {
      console.log('Analytics init error', e);
    }
    this.router.events.subscribe(async evt => {
      if (evt instanceof NavigationEnd) {
        const screen = evt.url.substring(1, evt.url.indexOf(';') > -1 ? evt.url.indexOf(';') : evt.url.length);
        this.onRouteChange(screen);
      }
    });

    this.reportActivitySubject.pipe(
      debounceTime(1000),
      tap(() => {
        this.serverReportActivity();
      })
    ).subscribe();
  }

  public async setRouteCustom(route: string): Promise<void> {
    this.onRouteChange(route);
  }

  public async setCurrentUserId(userEmail: string): Promise<void> {
    console.log('Analytics setUserId', userEmail);
    try {
      if (this.platform.is('cordova')) {
        // Do Firebase Analytics je zakazano v userId zasilat email uzivatele,
        // nebo cokoliv co by ho mohlo identifikovat mimo nas system, proto zasilame hash.
        const hash = userEmail ? Md5.hashStr(userEmail) as string : '';
        await this.fa.setUserId(hash);
        await this.fa.setUserProperty(AnalyticsUserPropertyEnum.HASH, hash);
      }
    } catch (e) {
      console.log('Analytics setUserId error', e);
    }
  }

  public async logEvent(event: AnalyticsEvent): Promise<void> {
    console.log('Analytics logEvent', event);
    try {
      this.reportActivitySubject.next();
      if (this.platform.is('cordova')) {
        await this.fa.logEvent(event.name, event.params || {});
      }
    } catch (e) {
      console.log('Analytics logEvent error', e);
    }
  }

  public async setUserProperty(name: AnalyticsUserPropertyEnum, value: string): Promise<void> {
    console.log('Analytics setUserProperty', name, value);
    try {
      if (this.platform.is('cordova')) {
        await this.fa.setUserProperty(name, value);
      }
    } catch (e) {
      console.log('Analytics setUserProperty error', e);
    }
  }

  // hlaseni aktivity uzivatele na server
  // pouze pokud je prihlasen
  public async serverReportActivity(): Promise<boolean> {
    if (!this.apiProvider.getToken()) {
      return false;
    }
    try {
      await this.apiProvider.callRequest('user/ping', 'post', undefined, true).toPromise();
      return true;
    } catch (e) {
      console.log('ERROR SERVER REPORT ACTIVITY', e);
      return false;
    }
  }

  public async reportCampaign(details: { source: string, campaign: string, medium: string, content: string }): Promise<void> {
    console.log('Analytics reportCampaign', details);
    try {
      await this.fa.logEvent('firebase_campaign', details);
      await this.fa.logEvent('app_open', details);
    } catch (err) {
      console.log('Analytics reportCampaign error', err);
    }
  }

  private async onRouteChange(screen: string): Promise<void> {
    if (ANALYTICS_SCREEN_NAMES[screen]) {
      screen = ANALYTICS_SCREEN_NAMES[screen];
    }
    if (screen === 'onboarding-slides') {
      // tuto obrazovku nereportujeme - view odesila eventy samostatne dle aktualniho slidu
      return;
    }
    if (screen) {
      console.log('Analytics setCurrentScreen', screen);
      try {
        if (this.platform.is('cordova')) {
          await this.fa.setCurrentScreen(screen);
        }
      } catch (e) {
        console.log('Analytics setCurrentScreen error', e);
      }
    }
  }
}
