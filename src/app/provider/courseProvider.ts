import { Injectable } from '@angular/core';
import { CourseShort } from '../pojo/courseShort';
import { CourseDetail } from '../pojo/courseDetail';
import { ApiProvider } from './apiProvider';
import { RouterDirection } from '@ionic/core';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../pojo/user';
@Injectable()
export class CourseProvider {

    private dailyMeditations: CourseShort[];
    private courseList: CourseShort[];
    private activeCourse: CourseShort;
    private activeCourseDetail: CourseDetail;
    private activeLessonIndex: number;
    private returnDetailCourse: CourseShort;
    private playbackPageReturnInfo: {
        page: string;
        direction: RouterDirection
    };

    constructor(
        private apiProvider: ApiProvider,
        private storage: Storage,
        private translateService: TranslateService,
    ) {
    }

    public getCurrentDailyMediation(): CourseShort {
        if (this.dailyMeditations.length) {
            return this.dailyMeditations[0];
        }
        return undefined;
    }

    public getDailyMeditations(): CourseShort[] {
        return this.dailyMeditations;
    }
    public setDailyMeditations(meditations: CourseShort[]): void {
        this.dailyMeditations = meditations;
    }

    public getAllCourses(): CourseShort[] {
        return this.courseList;
    }
    public setAllCourses(courses: CourseShort[]): void {
        this.courseList = courses;
    }

    public getActiveCourse(): CourseShort {
        return this.activeCourse;
    }
    public setActiveCourse(course: CourseShort, returnDetailCourse?: CourseShort): void {
        this.activeCourse = course;
        this.returnDetailCourse = returnDetailCourse;
    }

    public getActiveCourseDetail(): CourseDetail {
        return this.activeCourseDetail;
    }
    public setActiveCourseDetail(course: CourseDetail): void {
        this.activeCourseDetail = course;
    }

    public getActiveLessonIndex(): number {
        return this.activeLessonIndex;
    }
    public setActiveLessonIndex(index: number): void {
        this.activeLessonIndex = index;
    }

    public getPlaybackPageReturnInfo(): { page: string; direction: RouterDirection } {
        return this.playbackPageReturnInfo;
    }
    public setPlaybackPageReturnInfo(info: { page: string; direction: RouterDirection }): void {
        this.playbackPageReturnInfo = info;
    }

    public getReturnDetailCourse(): CourseShort {
        return this.returnDetailCourse;
    }

    public getSimilarCourses(course: CourseShort): CourseShort[] {
        return this.courseList.filter(crse => crse.tags.some(tag => course.tags.indexOf(tag) > -1)).slice(0, 10);
    }

    public getCourseById(id: string): CourseShort {
        return this.courseList.find(c => c.id === id);
    }

    public async loadCourseDetail(course: CourseShort): Promise<CourseDetail> {
        try {
            const result = await this.serverLoadCourseDetail(course);
            await this.storage.set(`course-detail-${course.id}`, result);
            return result;
        } catch (e) {
            console.log(e);
            const stored = await this.storage.get(`course-detail-${course.id}`);
            if (stored) {
                return stored;
            } else {
                throw e;
            }
        }
    }

    public async serverLoadCourseDetail(course: CourseShort): Promise<CourseDetail> {
        try {
            let result: CourseDetail;
            result = await this.apiProvider.callRequest(`courses/${course.id}`, 'get', undefined, true).toPromise();
            result.type = course.type;
            result.lessons = result.lessons.map(l => { l.type = l.type.toUpperCase() as any; return l; });
            result.lessons.sort((a, b) => a.order - b.order);
            return result;
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    // vyber prvniho kurzu po onboardingu
    public async initOnboardingCourse(): Promise<boolean> {
        try {
            // vybiram mezi kurzy zdarma
            const freeCourses = this.courseList.filter(crse => crse.free);
            // zkusim vybrat prvni kurz odpovidajici jazyku usera
            let courseShort = freeCourses.find(crse => crse.languages.includes(this.translateService.currentLang));
            if (!courseShort) {
                courseShort = freeCourses[0];
            }
            const courseDetail = await this.loadCourseDetail(courseShort);
            this.activeCourse = courseShort;
            this.activeCourseDetail = courseDetail;
            this.activeLessonIndex = 0;
            return true;
        } catch (e) {
            return false;
        }
    }

    public getResumeLessonIndex(course: CourseShort | CourseDetail, user: User): number {
        const ownedCourse = user.coursesOwned.find(c => c.id === course.id);
        if (!ownedCourse) {
            // uzivatel nema zaznam o vlastnictvi kurzu
            console.log('CourseProvider: resumeLessonIndex not found');
            return 0;
        }

        const lessonsPlayedByTimestamp = ownedCourse.lessonsFinished
            .filter(lesson => course.lessons.find(l => l.id === lesson.id))
            .sort((a, b) => b.timestamp - a.timestamp);

        const lastLessonPlayed = lessonsPlayedByTimestamp[0];
        if (!lastLessonPlayed) {
            // uzivatel nema zaznam o prehrani jakekoliv lekce z kurzu
            console.log('CourseProvider: resumeLessonIndex not found');
            return 0;
        }

        const lastLessonPlayedIndex = course.lessons.findIndex(l => l.id === lastLessonPlayed.id);
        if (!lastLessonPlayed.currentSessionCompleted) {
            // pokud posledni prehravana lekce nebyla dokoncena, vybereme ji
            console.log('CourseProvider: last played lesson not finished, selecting it: ', lastLessonPlayedIndex);
            return lastLessonPlayedIndex;
        }

        // jinak vybereme nasledujici
        const nextLessonIndex = (lastLessonPlayedIndex + 1) % course.lessons.length;
        console.log('CourseProvider: last played lesson finished, selecting next: ', nextLessonIndex);
        return nextLessonIndex;
    }

    public getIsCourseComplete(course: CourseDetail, user: User): boolean {
        const ownedCourse = user.coursesOwned.find(c => c.id === course.id);
        if (!ownedCourse) {
            return false;
        }
        if (ownedCourse.lessonsFinished.length >= course.lessons.length &&
            ownedCourse.lessonsFinished.every(l => l.completed)) {
            return true;
        }
        return false;
    }
}
