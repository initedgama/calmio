import { Injectable } from '@angular/core';
import { User } from '../pojo/user';
import { CourseProvider } from './courseProvider';
import { Events, NavController } from '@ionic/angular';
import { CalmioEvents } from '../pojo/eventsEnum';
import { ApiProvider } from './apiProvider';
import { SignUpPasswordApiRequest } from '../pojo/signUpPasswordApiRequest';
import { SignInPasswordApiRequest } from '../pojo/signInPasswordApiRequest';
import { AlertService } from '../service/AlertService';
import { Config } from '../config';
import { GetUserApiResponse } from '../pojo/getUserApiResponse';
import { Storage } from '@ionic/storage';
import { CoursesAllApiResponse } from '../pojo/coursesAllApiResponse';
import { CourseShort } from '../pojo/courseShort';
import { CourseTypeEnum } from '../pojo/courseTypeEnum';
import { InstallReasonEnum } from '../pojo/installReasonEnum';
import { ExperienceAnswerEnum } from '../pojo/experienceAnswerEnum';
import { InAppPurchaseService } from '../service/InAppPurchaseService';
import { LoaderService } from '../service/LoaderService';
import { AnalyticsProvider } from './analyticsProvider';
import { AnalyticsEventsEnum } from '../pojo/analyticsEventsEnum';
import { AnalyticsEvent } from '../pojo/analyticsEvent';
import { AnalyticsUserPropertyEnum } from '../pojo/analyticsUserPropertyEnum';
import { Lesson } from '../pojo/lesson';
import { ExternalProviderUser } from '../pojo/externalProviderUser';
import { SyncService } from '../service/SyncService';
import { FacebookAuthService } from '../service/FacebookAuthService';
import { GoogleAuthService } from '../service/GoogleAuthService';
import { TranslateService } from '@ngx-translate/core';
import { CourseDetail } from '../pojo/courseDetail';
import { VoiceService } from '../service/VoiceService';
import { Voice } from '../pojo/voice';
import { VoiceGender } from '../pojo/voiceGender';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';
import { PlaybackEvent } from '../pojo/playbackEvent';
import { DateTime } from 'luxon';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { EmailVerificationService } from '../service/EmailVerificationService';
import { Subject } from 'rxjs';
import { UserEvent } from '../pojo/userEventEnum';
import { PlatformService } from '../service/PlatformService';

@Injectable()
export class UserProvider {

    private _showPositionFromMenu: boolean;   // priznak, jestli mam zobrazit meditacni pozice z menu nebo z registrace
    private _videoUrl: string;   // video url, ktere se ma spustit
    public pendingSignUpData: SignUpPasswordApiRequest;
    public pendingWizardIntroData: {
        installReason: InstallReasonEnum[],
        experience: ExperienceAnswerEnum,
        preferredVoice: VoiceGender
    } = {
            installReason: [],
            experience: ExperienceAnswerEnum.NO_EXPERIENCE,
            preferredVoice: VoiceGender.WOMAN
        };
    public userEvents$: Subject<UserEvent> = new Subject();

    // priznak, jestli se uzivatel prihlasil aktivne pres formular (true), nebo automaticky (false)
    public isActiveAuthenticationSession: boolean = false;

    // priznak, jestli uzivatel prisel do aplikace pres onboarding
    public isOnboardingAuthenticationSession: boolean = false;

    private user: User;

    constructor(
        public events: Events,
        private courseProvider: CourseProvider,
        private navCtrl: NavController,
        private apiProvider: ApiProvider,
        private alerts: AlertService,
        private storage: Storage,
        private inAppPurchaseService: InAppPurchaseService,
        private platform: PlatformService,
        private loading: LoaderService,
        private analyticsProvider: AnalyticsProvider,
        private syncService: SyncService,
        private fbAuthService: FacebookAuthService,
        private googleAuthService: GoogleAuthService,
        private translateSvc: TranslateService,
        private voiceService: VoiceService,
        private appVersion: AppVersion,
        private emailVerificationSvc: EmailVerificationService,
    ) {
        this.events.subscribe(CalmioEvents.HTTP_ERROR_UNAUTHORIZED, this.onHttpUnauthorizedError.bind(this));

    }

    public getUser(): User {
        if (this.user) {
            return JSON.parse(JSON.stringify(this.user));
        }
    }

    public getIsFullVersionUnlocked(): boolean {
        if (!this.user) {
            return false;
        }
        const hasActiveSubscription = !!this.user.activeSubscriptions.length;
        const hasActivePromoCode = !!this.user.promo;
        return hasActiveSubscription || hasActivePromoCode;
    }

    public get userName(): string {
        if (this.user) {
            return `${this.user.name} ${this.user.surname}`;
        } else {
            return undefined;
        }
    }

    public setUserDefaultVoiceGender(gender: VoiceGender): void {
        this.pendingWizardIntroData.preferredVoice = gender;
        if (this.user) {
            this.user.preferredVoice = gender;
        }
    }

    public async performPendingSignUpRequest(): Promise<boolean> {
        if (!this.pendingSignUpData) {
            return false;
        }
        // ulozit aktivni jazyk k uzivateli
        this.pendingSignUpData.language = this.translateSvc.currentLang;
        return this.signUpWithPassword(this.pendingSignUpData);
    }

    public async signUpWithPassword(signUpData: SignUpPasswordApiRequest): Promise<boolean> {
        try {
            await this.apiProvider.setToken('');
            const { token } = await this.apiProvider.callRequest('user/registration-password', 'post', signUpData).toPromise();
            await this.apiProvider.setToken(token);
            return await this.initializeUserAndCourses();
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    public async signInWithPassword(signInData: SignInPasswordApiRequest): Promise<boolean> {
        try {
            await this.apiProvider.setToken('');
            const { token } = await this.apiProvider.callRequest('user/login-password', 'post', signInData).toPromise();
            await this.apiProvider.setToken(token);
            return await this.initializeUserAndCourses();
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    public async signInOAuth(userData: ExternalProviderUser): Promise<boolean> {
        try {
            // ulozit aktivni jazyk k uzivateli
            const signInData = {
                ...userData,
                language: this.translateSvc.currentLang,
            };
            await this.apiProvider.setToken('');
            const { token } = await this.apiProvider.callRequest('user/login-oauth', 'post', signInData, true).toPromise();
            await this.apiProvider.setToken(token);
            return await this.initializeUserAndCourses();
        } catch (err) {
            console.log(err);
            if (err && err.error && err.error.localizationKey) {
                throw new Error(this.translateSvc.instant(`common.server-error-response.${err.error.localizationKey}`));
            }
            return false;
        }
    }

    public async signOut(): Promise<void> {
        this.user = undefined;
        await this.apiProvider.setToken('');
        await this.storage.set('user', undefined);
        this.analyticsProvider.logEvent({ name: AnalyticsEventsEnum.LOGOUT });
        this.analyticsProvider.setCurrentUserId('');
        this.fbAuthService.signOut();
        this.googleAuthService.signOut();
        this.syncService.cleanQueue();
        this.navCtrl.navigateRoot('intro');
        this.userEvents$.next(UserEvent.SIGNED_OUT);
    }

    public async deleteAccount(): Promise<void> {
        try {
            await this.apiProvider.callRequest('user', 'delete', undefined).toPromise();
        } catch (e) {
            console.log(e);
            return;
        }
        this.signOut();
        this.alerts.displayToast(this.translateSvc.instant('delete-account.toast-success'), 5000);
    }

    public async requestPasswordReset(requestData: { email: string }): Promise<boolean> {
        try {
            await this.apiProvider.setToken('');
            await this.apiProvider.callRequest('user/reset-password', 'post', requestData, true).toPromise();
            return true;
        } catch (e) {
            console.log(e);
            if (e && e.status && e.status === 404) {
                this.alerts.displayToast(this.translateSvc.instant('view.sign-in.toast-resetPasswordUserNotFound'));
            } else {
                this.alerts.displayToast(this.translateSvc.instant(Config.errorMessageMap.generic.default));
            }
            return false;
        }
    }

    public async initializeUserAndCourses(): Promise<boolean> {
        await this.syncService.initialize();
        let successRestorePurchases = true;
        if (this.platform.is('android')) {
            // behem kazdem prihlaseni na Androidu zjistujeme zda je predplatne aktivni
            successRestorePurchases = await this.inAppPurchaseService.restorePurchasesAndroid(true);
        } else if (this.platform.is('ios')) {
            await this.inAppPurchaseService.serverSyncPurchases(true);
        }

        const successUser = await this.loadUser();
        const successCourses = await this.loadCourses();

        await this.updateActiveSubscriptions();

        return successUser && successCourses && successRestorePurchases;
    }

    public async updateUser(userStub: any): Promise<boolean> {
        try {
            const version = this.platform.is('cordova') ? await this.appVersion.getVersionNumber() : 'browser';
            const resp: GetUserApiResponse = await this.apiProvider
                .callRequest(`user?version=${version}`, 'putWithBody', {
                    ...userStub,
                    language: this.translateSvc.currentLang, // jazyk posilam vzdy, at je na BE aktualni stav
                }).toPromise();
            this.user = this.parseGetUserResp(resp);
            await this.updateActiveSubscriptions();
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    public async onAuthComplete(animateNav: boolean, isActiveAuth: boolean, event?: AnalyticsEvent): Promise<void> {
        console.log('userProvider onAuthComplete');
        await this.analyticsProvider.setCurrentUserId(this.user.email);
        if (event) {
            await this.analyticsProvider.logEvent(event);
        }
        this.navigateAfterAuth(animateNav, isActiveAuth);
    }

    public onOnboardingComplete(skipFirstCourse: boolean): void {
        console.log('userProvider onOnboardingComplete');
        this.navigateAfterOnboarding(skipFirstCourse);
    }

    public onEmailVerificationComplete(): void {
        console.log('userProvider onEmailVerificationComplete');
        this.navigateAfterEmailVerification();
    }

    /*
        Event pro prvni vstup uzivatele do sekce s kurzy po prihlaseni nebo spusteni app
    */
    public onUserEnterAppCore(): void {
        console.log('userProvider onUserEnterAppCore');
        this.userEvents$.next(UserEvent.ENTER_APP_CORE);
        this.userEvents$.next(UserEvent.ACTIVATE_PROMO);
    }

    public onPushSettingsChange(): void {
        console.log('userProvider onPushSettingsChange');
        this.userEvents$.next(UserEvent.PUSH_SETTINGS_CHANGE);
    }

    // pri kazdem prihlaseni, nakupu nebo obnove plateb
    // nacte stav predplatneho ze storage a upravi podle toho stav usera, param activeSubscriptions
    public async updateActiveSubscriptions(): Promise<void> {

        if (!this.user) {
            return;
        }

        if (this.user.activeSubscriptions !== null) {
            // kontrola na BE je aktivni, aplikace pouzije data obdrzena odtamtud
            console.log('userProvider updateActiveSubscriptions BE verification is active');
            return;
        }

        console.log('userProvider updateActiveSubscriptions BE verification is disabled, starting local check');
        // kontrola na BE je vypnuta, aplikace pouzije vlastni data o predplatnem
        const storedActiveSubscriptions = await this.inAppPurchaseService.restoreSubscriptionsFromStorage();
        const notExpired = storedActiveSubscriptions.filter(it => it.expirationTimestamp > Date.now());
        this.user.activeSubscriptions = notExpired;
    }

    public async signInCourse(course: CourseShort): Promise<boolean> {
        try {
            await this.apiProvider
                .callRequest(`courses/${course.id}/sign`, 'get', undefined).toPromise();
            if (this.user.coursesOwned.findIndex(c => c.id === course.id) === -1) {
                this.user.coursesOwned.push({
                    id: course.id,
                    lessonsFinished: [],
                });
            }
            this.analyticsProvider.logEvent({
                name: AnalyticsEventsEnum.COURSE_PURCHASE,
                params: { course_id: course.id, course_name: course.name }
            });
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    public async addPlaybackHistoryEvent(): Promise<void> {
        let playbackHistory: PlaybackEvent[] = await this.storage.get(StorageKeysEnum.PLAYBACK_HISTORY);
        if (!playbackHistory) {
            playbackHistory = [];
        }
        playbackHistory.push({
            timestamp: Date.now(),
        });
        await this.storage.set(StorageKeysEnum.PLAYBACK_HISTORY, playbackHistory);
    }

    public async getContinuousMeditationDaysCount(): Promise<number> {
        let prevPlaybackHistory: PlaybackEvent[] = await this.storage.get(StorageKeysEnum.PLAYBACK_HISTORY);
        if (!prevPlaybackHistory) {
            prevPlaybackHistory = [];
        }

        // get continous days count and filter only events relevant to continous days computation
        const now = DateTime.local();
        const relevantEvents: PlaybackEvent[] = [];
        let currentDay = now;
        let continuousDaysCount = 0;
        while (true) {
            const entriesForCurrentDay = prevPlaybackHistory.filter(it => {
                const datetime = DateTime.fromMillis(it.timestamp);
                return datetime > currentDay.startOf('day') && datetime < currentDay.endOf('day');
            });
            if (entriesForCurrentDay.length) {
                relevantEvents.push(...entriesForCurrentDay);
                if (currentDay !== now) {
                    continuousDaysCount += 1;
                }
                currentDay = currentDay.minus({ days: 1 });
            } else {
                break;
            }
        }

        // keep only relevant events in history
        await this.storage.set(StorageKeysEnum.PLAYBACK_HISTORY, relevantEvents);

        return continuousDaysCount;
    }

    public async saveLessonEnded(
        course: CourseDetail,
        lesson: Lesson,
        secondsMeditated: number = 0,
        completed: boolean,
        lessonDurationSeconds: number,
        voice: Voice,
    ): Promise<void> {
        const timestamp = Math.round(Date.now() / 1000);
        const ownedCourse = this.user.coursesOwned.find(c => c.id === course.id);
        const ownedCourseLesson = ownedCourse.lessonsFinished.find(item => item.id === lesson.id);
        const ownedCourseLessonTime = ownedCourseLesson && !isNaN(+ownedCourseLesson.time) ? +ownedCourseLesson.time : 0;
        const timeTotalInLesson = secondsMeditated + ownedCourseLessonTime;
        const isLessonCompleted = (ownedCourseLesson && ownedCourseLesson.completed) || completed;
        const continousMeditationDaysCount = await this.getContinuousMeditationDaysCount();
        let unfinishedTime;
        if (completed) {
            unfinishedTime = ownedCourseLesson ? ownedCourseLesson.unfinishedTime : 0;
        } else {
            unfinishedTime = secondsMeditated;
        }

        const postData = {
            courseId: course.id,
            id: lesson.id,
            time: timeTotalInLesson,
            timestamp,
            unfinishedTime,
            completed: isLessonCompleted,
            lessonDurationSeconds, // celkovy cas lekce
            voice: voice && voice.gender, // hlas pruvodce - pohlavi
            lecturerId: voice && voice.id, // hlas pruvodce - id
            currentSessionCompleted: completed, // aktualni prehrani lekce - dokonceno
            currentSessionTime: secondsMeditated, // aktualni prehrani lekce - promeditovany cas
            continuousDays: continousMeditationDaysCount,
        };
        if (ownedCourseLesson) {
            ownedCourseLesson.timestamp = postData.timestamp;
            ownedCourseLesson.time = postData.time;
            ownedCourseLesson.unfinishedTime = postData.unfinishedTime;
            ownedCourseLesson.completed = postData.completed;
            ownedCourseLesson.currentSessionCompleted = postData.currentSessionCompleted;
        } else {
            ownedCourse.lessonsFinished.push(postData);
        }

        this.persistUser();

        this.syncService.addSyncItem({
            method: 'post',
            endpoint: 'lessons/finish',
            data: postData,
        });

        let totalTimeMeditated = 0;
        this.user.coursesOwned.forEach(c => {
            c.lessonsFinished.forEach(l => {
                totalTimeMeditated += !isNaN(+l.time) ? +l.time : 0;
            });
        });

        this.analyticsProvider.setUserProperty(AnalyticsUserPropertyEnum.TOTAL_TIME_MEDITATED, '' + totalTimeMeditated);
        if (completed) {
            this.analyticsProvider.logEvent({
                name: AnalyticsEventsEnum.LESSON_FINISHED,
                params: { course_id: course.id, course_name: course.name, lesson_id: lesson.id, lesson_order: lesson.order, },

            });
        } else {
            this.analyticsProvider.logEvent({
                name: AnalyticsEventsEnum.LESSON_UNFINISHED,
                params: { course_id: course.id, course_name: course.name, lesson_id: lesson.id, lesson_order: lesson.order, },
            });
        }
    }

    public async saveIntroWizardCompleted(): Promise<boolean> {
        this.analyticsProvider.setUserProperty(AnalyticsUserPropertyEnum.VOICE, this.pendingWizardIntroData.preferredVoice);
        return await this.updateUser(this.pendingWizardIntroData);
    }

    public async maybeServerReloadUserAndCourses(forceReload: boolean): Promise<void> {
        if (this.syncService.hasPendingItems()) {
            // reload delat pouze po dokonceni synchronizace
            return;
        }
        const lastLoadCoursesDatetime = DateTime.fromMillis(await this.storage.get(StorageKeysEnum.SERVER_LOAD_COURSES_TIMESTAMP));
        const lastLoadUserDatetime = DateTime.fromMillis(await this.storage.get(StorageKeysEnum.SERVER_LOAD_USER_TIMESTAMP));
        const lastLoadCoursesLang = await this.storage.get(StorageKeysEnum.SERVER_LOAD_COURSES_LANG);
        const nowDatetime = DateTime.local();
        const currentLang = this.translateSvc.currentLang;
        // reload min 1x denne, pocitat od pulnoci (meni se denni meditace)
        const shouldReload = forceReload
            || nowDatetime.hasSame(lastLoadCoursesDatetime, 'day') === false
            || nowDatetime.hasSame(lastLoadUserDatetime, 'day') === false
            || currentLang !== lastLoadCoursesLang;
        if (shouldReload) {
            await this.loading.presentLoader();
            await this.loadUserFromServer();
            await this.loadCoursesFromServer();
            await this.loading.dismissLoader();
            await this.updateActiveSubscriptions();
            this.updateDefaultVoice();
        }
    }

    private onHttpUnauthorizedError(): void {
        if (this.user) {
            this.signOut();
        }
    }

    private async loadUser(): Promise<boolean> {
        let authError = false;
        let loadSuccess = false;
        try {
            loadSuccess = await this.loadUserFromServer(true);
        } catch (e) {
            if (e.message === CalmioEvents.HTTP_ERROR_UNAUTHORIZED) {
                authError = true;
            }
        }
        if (authError) {
            return false;
        }
        if (!loadSuccess) {
            const stored = await this.storage.get('user');
            if (stored) {
                this.user = stored;
                loadSuccess = true;
            }
        }
        if (loadSuccess) {
            this.updateDefaultVoice();
        }
        return loadSuccess;
    }

    public async loadCourses(): Promise<boolean> {
        let loadSuccess = await this.loadCoursesFromServer();
        if (loadSuccess) {
            await this.persistCourses();
        } else {
            const stored = await this.storage.get('courses');
            if (stored) {
                this.courseProvider.setAllCourses(stored.coursesAll);
                this.courseProvider.setDailyMeditations(stored.dailyMeditations);
                loadSuccess = true;
            }
        }
        return loadSuccess;
    }

    public async reloadUser(): Promise<User> {
        const success = await this.loadUserFromServer();
        if (!success) {
            return undefined;
        }
        await this.updateActiveSubscriptions();
        return this.user;
    }

    private async loadUserFromServer(throwOnUnauthorized: boolean = false): Promise<boolean> {
        try {
            const version = this.platform.is('cordova') ? await this.appVersion.getVersionNumber() : '1.1.10';
            const result: GetUserApiResponse = await this.apiProvider
                .callRequest(`user?version=${version}`, 'get', undefined, true).toPromise();
            this.user = this.parseGetUserResp(result);
            if (this.user.language !== this.translateSvc.currentLang) {
                await this.updateUser({}); // aktualizuji jazyk na BE
            }
            await this.storage.set(StorageKeysEnum.SERVER_LOAD_USER_TIMESTAMP, Date.now());
            await this.persistUser();
            return true;
        } catch (e) {
            console.log('ERROR LOAD USER FROM SERVER', e);
            if (e && e.status === 401 && throwOnUnauthorized) {
                this.alerts.displayToast(this.translateSvc.instant('common.error.toast-userLoggedOut'));
                throw new Error(CalmioEvents.HTTP_ERROR_UNAUTHORIZED);
            }
            return false;
        }
    }

    private parseGetUserResp(resp: GetUserApiResponse): User {
        // @ts-ignore
        // resp.subscriptions = [{ productId: 'medium2_renewable_test', type: 'ANNUAL', validTo: 1678120962 }];
        const user: User = {
            email: resp.email,
            name: resp.name,
            surname: resp.surname,
            auth: resp.auth || [],
            activeSubscriptions: resp.subscriptions && resp.subscriptions.map(sub => ({
                expirationTimestamp: sub.validTo * 1000,
                id: sub.productId,
                type: sub.type,
                receipt: undefined,
            })),
            experience: resp.experience,
            installReason: resp.installReason,
            coursesOwned: resp.coursesOwned || [],
            language: resp.language,
            preferredVoice: resp.preferredVoice,
            roles: resp.roles,
            pushNotification: resp.pushNotification,
            verificationRequired: resp.verificationRequired,
            promo: resp.promo ? {
                ...resp.promo,
                expires: resp.promo.expires * 1000 // convert to ms timestamp
            } : undefined
        };
        return user;
    }

    private async loadCoursesFromServer(): Promise<boolean> {
        try {
            const version = this.platform.is('cordova') ? await this.appVersion.getVersionNumber() : '1.1.18';
            const result: CoursesAllApiResponse = await this.apiProvider
                .callRequest(`courses-all?version=${version}`, 'get', undefined, true).toPromise();
            this.courseProvider.setAllCourses(result.coursesAll.map(c => {
                c.type = CourseTypeEnum.COURSE;
                return c;
            }));
            this.courseProvider.setDailyMeditations(result.dailyMeditations.map(c => {
                c.type = CourseTypeEnum.DAILY_MEDITATION;
                return c;
            }));
            await this.storage.set(StorageKeysEnum.SERVER_LOAD_COURSES_TIMESTAMP, Date.now());
            await this.storage.set(StorageKeysEnum.SERVER_LOAD_COURSES_LANG, this.translateSvc.currentLang);
            return true;
        } catch (e) {
            console.log('ERROR LOAD COURSES FROM SERVER', e);
            return false;
        }
    }

    public getTotalMeditationTime(): number {
        let total = 0;
        this.user.coursesOwned.forEach(course => {
            course.lessonsFinished.forEach(lesson => {
                total += isNaN(+lesson.time) ? 0 : +lesson.time;
            });
        });
        return total;
    }

    private async persistUser(): Promise<void> {
        await this.storage.set('user', this.user);
    }
    private async persistCourses(): Promise<void> {
        await this.storage.set('courses', {
            coursesAll: this.courseProvider.getAllCourses(),
            dailyMeditations: this.courseProvider.getDailyMeditations(),
        });
    }

    private updateDefaultVoice(): void {
        // nastaveni defaultni hlasu po prihalseni
        if (this.user) {
            this.voiceService.setDefaultVoiceGender(this.user.preferredVoice);
        }
    }

    private navigateAfterAuth(animated: boolean, isActiveAuth: boolean): void {
        const user = this.user;
        const isOnboaringComplete = !!user.experience;
        const isVerificationRequired = user.verificationRequired;

        this.isActiveAuthenticationSession = isActiveAuth;
        this.isOnboardingAuthenticationSession = !isOnboaringComplete;

        if (isOnboaringComplete) {
            if (isVerificationRequired) {
                this.emailVerificationSvc.setOnVerifiedNavigation('list');
                this.navCtrl.navigateRoot('verify-email', { animated, animationDirection: 'forward' });
            } else {
                this.navCtrl.navigateRoot('list', { animated, animationDirection: 'forward' });
                this.onUserEnterAppCore();
            }
        } else {
            this.navCtrl.navigateRoot('onboarding-slides', { animated, animationDirection: 'forward' });
        }
    }

    private navigateAfterOnboarding(skipFirstCourse: boolean): void {
        const user = this.user;
        const isVerificationRequired = user.verificationRequired;
        const route = skipFirstCourse ? 'list' : 'course-detail';

        if (isVerificationRequired) {
            this.emailVerificationSvc.setOnVerifiedNavigation(route);
            this.navCtrl.navigateRoot('verify-email', { animated: true, animationDirection: 'forward' });
        } else {
            this.navCtrl.navigateRoot(route, { animated: true, animationDirection: 'forward' });
            this.onUserEnterAppCore();
        }
    }

    private navigateAfterEmailVerification(): void {
        const nav = this.emailVerificationSvc.getOnVerifiedNavigation();
        this.navCtrl.navigateRoot(nav, { animated: true, animationDirection: 'forward' });
        this.onUserEnterAppCore();
    }

    get showPositionFromMenu(): boolean {
        return this._showPositionFromMenu;
    }

    set showPositionFromMenu(value: boolean) {
        this._showPositionFromMenu = value;
    }

    get videoUrl(): string {
        return this._videoUrl;
    }

    set videoUrl(value: string) {
        this._videoUrl = value;
    }
}
