import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AlertService } from '../service/AlertService';
import { Events } from '@ionic/angular';
import { CalmioEvents } from '../pojo/eventsEnum';
import { Config } from '../config';
import { Storage } from '@ionic/storage';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';
import { TranslateService } from '@ngx-translate/core';
import { DeviceIdService } from '../service/DeviceIdService';
import { SentryService } from '../service/SentryService';
import { LocalizationKeyEnum } from '../pojo/localizationKeyEnum';

@Injectable()
export class ApiProvider {
    private apiUrl: string = 'https://api.calmio.cz/api/';
    // private apiUrl: string = 'http://p5011.dev.inited.cz/api/';
    private token: string = null;

    constructor(
        public events: Events,
        private http: HttpClient,
        private alertService: AlertService,
        private storage: Storage,
        private translateSvc: TranslateService,
        private deviceIdService: DeviceIdService,
        private sentryService: SentryService,
    ) {
    }

    public getToken(): string {
        return this.token;
    }

    public async setToken(token: string): Promise<void> {
        await this.storage.set(StorageKeysEnum.API_TOKEN, token);
        this.token = token;
    }


    public callRequest(action: string, method: string = 'get', params: any, disableGlobalErrorHandling?: boolean): Observable<any> {

        let paramsString;
        try {
            paramsString = JSON.stringify(params);
        } catch (e) {
            // ok
        }
        console.log('ApiProvider callRequest', action, method, params, paramsString);

        const httpOptions: any = {
            headers: {
                'Content-Type': 'application/json',
                'X-Locale': this.translateSvc.currentLang,
                'X-Device-Uuid': this.deviceIdService.uuid,
            }
        };
        if (this.token) {
            httpOptions.headers = Object.assign(httpOptions.headers, {
                Authorization: 'Bearer ' + this.token
            });
        }
        const errorHandler = catchError((err) => this.handleError(err, disableGlobalErrorHandling));


        if (method === 'get') {
            if (params) {
                httpOptions.params = params;
            }
            return this.http.get(this.apiUrl + action, httpOptions)
                .pipe(errorHandler);
        } else if (method === 'post') {
            return this.http.post(this.apiUrl + action, params, httpOptions)
                .pipe(errorHandler);
        } else if (method === 'put') {
            params = !params ? '' : ('?' + params);
            return this.http.put(this.apiUrl + action + params, httpOptions)
                .pipe(errorHandler);
        } else if (method === 'delete') {
            httpOptions.body = params;
            return this.http.delete(this.apiUrl + action, httpOptions)
                .pipe(errorHandler);
        } else if (method === 'postWithUrlParams') {
            params = !params ? '' : ('?' + params);
            return this.http.post(this.apiUrl + action + params, null, httpOptions)
                .pipe(errorHandler);
        } else if (method === 'putWithBody') {
            return this.http.put(this.apiUrl + action, params, httpOptions)
                .pipe(errorHandler);
        }
    }

    private handleError(errorInput: Response | any, disableGlobalErrorHandling?: boolean): Observable<never> {
        console.log('API ERR', JSON.stringify(errorInput));

        if (errorInput && errorInput.status !== 0 && errorInput.status !== 401) {
            // Zname chyby a uzivatel je o nich informovan. Neloguji se do Sentry.
            if (errorInput.error && errorInput.error.localizationKey === LocalizationKeyEnum.INVALID_CREDENTIALS) {
                console.log('API KNOWN ERR: ' + LocalizationKeyEnum.INVALID_CREDENTIALS);
            } else if (errorInput.error && errorInput.error.localizationKey === LocalizationKeyEnum.FACEBOOK_AUTH_MISSING_EMAIL) {
                console.log('API KNOWN ERR: ' + LocalizationKeyEnum.FACEBOOK_AUTH_MISSING_EMAIL);
            } else if (errorInput.error && errorInput.error.localizationKey === LocalizationKeyEnum.USER_ALREADY_EXISTS) {
                console.log('API KNOWN ERR: ' + LocalizationKeyEnum.USER_ALREADY_EXISTS);
            } else if (errorInput.error && errorInput.error.localizationKey === LocalizationKeyEnum.PROMO_CODE_INVALID) {
                console.log('API KNOWN ERR: ' + LocalizationKeyEnum.PROMO_CODE_INVALID);
            } else if (errorInput.status === 404 && errorInput.url.includes('reset-password')) {
                console.log('API KNOWN ERR: 404 user not found - reset-password');
            } else {
                console.log('SENTRY LOG ERROR');
                this.sentryService.captureException(errorInput);
            }
        }

        if (!disableGlobalErrorHandling) {
            this.handleApiErrorResponse(errorInput);
        }
        return throwError(JSON.parse(JSON.stringify(errorInput)));
    }

    private async handleApiErrorResponse(response: any): Promise<void> {
        console.log('API ERROR', response);

        // na zaklade statusu nebo debugMessage v chybe zobrazit odpovidajici hlasku,
        // pokud zadna neni dostupna zobrazit defaultni
        if (!response) {
            this.presentApiError(Config.errorMessageMap.generic.default);
        } else if (response.status === 0 && !navigator.onLine) {
            // chyba neni pristup k internetu
            this.presentApiError(Config.errorMessageMap.generic.noInternetAccess);
        } else if (response.error && response.error.error) {
            if (response.error.localizationKey) {
                const translationKey = `common.server-error-response.${response.error.localizationKey}`;
                const translation = this.translateSvc.instant(translationKey);
                if (translation !== translationKey) {
                    // existuje preklad klice
                    this.presentApiError(translationKey);
                } else {
                    this.presentApiError(response.error.error);
                }
            } else {
                this.presentApiError(response.error.error);
            }
        } else if (response.status !== undefined && Config.errorMessageMap.generic.status.exact[response.status]) {
            this.presentApiError(Config.errorMessageMap.generic.status.exact[response.status]);
        } else if (response.status && Config.errorMessageMap.generic.status.startsWith[('' + response.status)[0]]) {
            this.presentApiError(Config.errorMessageMap.generic.status.startsWith[('' + response.status)[0]]);
        } else {
            this.presentApiError(Config.errorMessageMap.generic.default);
        }

        if (response && response.status === 401) {
            // userProvider reaguje odhlasenim uzivatele
            this.events.publish(CalmioEvents.HTTP_ERROR_UNAUTHORIZED);
        }

    }

    private presentApiError(translateKey: string): void {
        if (!!Config.apiErrorsWithAlert.find(err => translateKey.includes(err))) {
            this.alertService.displayErrorAlert(this.translateSvc.instant(translateKey));
        } else {
            this.alertService.displayToast(this.translateSvc.instant(translateKey), undefined, true);
        }
    }

}
