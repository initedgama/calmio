import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ApiProvider } from 'src/app/provider/apiProvider';
import { retryWhen, delay } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage {

  public aboutHtml: string;
  private loadSub: Subscription;

  constructor(
    public navCtrl: NavController,
    public platform: PlatformService,
    private apiProvider: ApiProvider,
    private translateSvc: TranslateService,
  ) {
  }

  public ionViewDidEnter(): void {
    this.loadContent();
  }

  public ionViewWillLeave(): void {
    if (this.loadSub) {
      this.loadSub.unsubscribe();
    }
  }

  public async loadContent(): Promise<void> {
    this.loadSub = this.apiProvider.callRequest('static', 'get', { id: 'ABOUT', language: this.translateSvc.currentLang })
      .pipe(retryWhen(errors =>
        errors.pipe(delay(1000))
      ))
      .subscribe((result: { html: string }) => {
        this.aboutHtml = result.html;
      });
  }
}
