import { Component, ViewChild } from '@angular/core';
import { NavController, IonContent, ModalController } from '@ionic/angular';
import { CourseProvider } from 'src/app/provider/courseProvider';
import { UserProvider } from 'src/app/provider/userProvider';
import { Subscription } from 'rxjs';
import { CourseDetail } from 'src/app/pojo/courseDetail';
import { AnalyticsProvider } from 'src/app/provider/analyticsProvider';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { Voice } from '../../pojo/voice';
import { VoiceService } from '../../service/VoiceService';
import { PlatformService } from 'src/app/service/PlatformService';
import { CourseTypeEnum } from 'src/app/pojo/courseTypeEnum';
import { MenuService } from 'src/app/service/MenuService';
import { LessonPickerComponent } from 'src/app/components/lesson-picker/lesson-picker.component';
import { VoicePickerComponent } from 'src/app/components/voice-picker/voice-picker.component';

@Component({
  selector: 'app-course-detail',
  templateUrl: 'course-detail.page.html',
  styleUrls: ['course-detail.page.scss']
})
export class CourseDetailPage {
  @ViewChild(IonContent) public contentRef: IonContent;

  public hardwareBackSubscription: Subscription;
  public loading: boolean = true;
  public error: boolean = false;
  public courseDetail: CourseDetail;
  public selectedLessonIndex: number = 0;
  public selectedVoice: Voice;
  public courseComplete: boolean = false;
  public CourseTypeEnum: typeof CourseTypeEnum = CourseTypeEnum;
  private availableVoices: Voice[];
  private retryTimeout: any;
  private modal: any;

  constructor(
    public platform: PlatformService,
    private userProvider: UserProvider,
    private navCtrl: NavController,
    private courseProvider: CourseProvider,
    private analyticsProvider: AnalyticsProvider,
    private voiceService: VoiceService,
    private menuService: MenuService,
    private modalCtrl: ModalController,
  ) { }

  public ionViewDidEnter(): void {
    this.initContent();
    this.hardwareBackSubscription = this.platform.backButton.subscribeWithPriority(9999, async () => {
      if (this.modal) {
        this.modal.dismiss();
        return true;
      }
      this.onBackButtonClicked();
      return true;
    });
  }

  public ionViewWillLeave(): void {
    if (this.hardwareBackSubscription) {
      this.hardwareBackSubscription.unsubscribe();
      this.hardwareBackSubscription = undefined;
    }
    clearTimeout(this.retryTimeout);
  }

  public toggleMenu(): void {
    this.menuService.open();
  }

  public onBackButtonClicked(): void {
    this.navCtrl.navigateRoot('list', { animated: true, animationDirection: 'back' });
  }

  public onPlayButtonClick(): void {
    this.courseProvider.setActiveCourseDetail(this.courseDetail);
    this.courseProvider.setActiveLessonIndex(this.selectedLessonIndex);
    this.courseProvider.setPlaybackPageReturnInfo({
      direction: 'back',
      page: 'course-detail',
    });
    this.navCtrl.navigateForward('playback');
  }

  public async onPickLessonClick(): Promise<void> {
    this.modal = await this.modalCtrl.create({
      cssClass: 'item-picker-modal-wrapper',
      component: LessonPickerComponent,
      mode: 'ios',
      componentProps: {
        lessonsTotal: this.courseDetail.lessons.length,
      }
    });
    this.modal.present();
    const selectedIndex = (await this.modal.onWillDismiss()).data;
    if (selectedIndex !== undefined) {
      this.selectedLessonIndex = selectedIndex;
      this.updateVoiceOptions();
    }
    this.modal = undefined;
  }

  public async onPickVoiceClick(): Promise<void> {
    this.modal = await this.modalCtrl.create({
      cssClass: 'item-picker-modal-wrapper',
      component: VoicePickerComponent,
      mode: 'ios',
      componentProps: {
        voices: this.availableVoices,
        selected: this.selectedVoice ? this.selectedVoice.id : undefined,
      }
    });
    this.modal.present();
    const selectedId = (await this.modal.onWillDismiss()).data;
    if (selectedId !== undefined) {
      this.selectedVoice = this.availableVoices.find(voice => voice.id === selectedId);
      this.voiceService.setCourseVoice(this.courseDetail.id, this.selectedVoice);
    }
    this.modal = undefined;
  }

  private async initContent(): Promise<void> {
    if (this.courseDetail) {
      // pri navratu na stranku z prehravace pouze aktualizujeme informace o aktualni lekci/dokonceni kurzu/vyberu hlasu
      this.selectedLessonIndex = this.courseProvider.getResumeLessonIndex(this.courseDetail, this.userProvider.getUser());
      this.courseComplete = this.courseProvider.getIsCourseComplete(this.courseDetail, this.userProvider.getUser());
      this.updateVoiceOptions();
      return;
    }
    this.loading = true;
    this.error = false;
    const course = this.courseProvider.getActiveCourse();
    try {
      this.courseDetail = await this.courseProvider.loadCourseDetail(course);
      this.selectedLessonIndex = this.courseProvider.getResumeLessonIndex(this.courseDetail, this.userProvider.getUser());
      this.courseComplete = this.courseProvider.getIsCourseComplete(this.courseDetail, this.userProvider.getUser());
      await this.updateVoiceOptions();
    } catch (e) {
      this.error = true;
      console.log('err', e);
      // retry in 1s
      this.retryTimeout = setTimeout(() => {
        this.initContent();
      }, 1000);
    }
    this.loading = false;
    this.contentRef.scrollToTop(200);

    if (this.courseDetail) {
      this.analyticsProvider.logEvent({
        name: AnalyticsEventsEnum.SELECT_COURSE_OWNED_DETAIL,
        params: { course_id: course.id, course_name: course.name }
      });
    }
  }

  private async updateVoiceOptions(): Promise<void> {
    const activeLesson = this.courseDetail.lessons[this.selectedLessonIndex];
    this.availableVoices = this.voiceService.getLessonAvailableVoices(activeLesson);
    const preferredVoiceId = await this.voiceService.getLessonPreferredVoiceId(activeLesson, this.courseDetail.id);
    this.selectedVoice = this.availableVoices.find(voice => voice.id === preferredVoiceId) || this.availableVoices[0];
  }
}
