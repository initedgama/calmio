import { Component } from '@angular/core';
import { UserProvider } from 'src/app/provider/userProvider';
import { User } from 'src/app/pojo/user';
import { AlertController, NavController } from '@ionic/angular';
import { InAppPurchaseProduct } from 'src/app/pojo/inAppPurchaseProduct';
import { InAppSubscriptionType } from 'src/app/pojo/inAppSubscriptionTypeEnum';
import { PlatformService } from 'src/app/service/PlatformService';
import { InAppPurchaseFunctions } from '../../service/InAppPurchaseFunctions';
import { File } from '@ionic-native/file/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-full-version',
  templateUrl: './full-version.page.html',
  styleUrls: ['./full-version.page.scss'],
})
export class FullVersionPage {

  public isLoading: boolean = true;
  public user: User = this.userProvider.getUser();
  public products: InAppPurchaseProduct[];
  public InAppSubscriptionType: typeof InAppSubscriptionType = InAppSubscriptionType;
  public selectedItem: InAppPurchaseProduct;

  constructor(
    public platform: PlatformService,
    public alertController: AlertController,
    public navCtrl: NavController,
    private userProvider: UserProvider,
    public inAppPurchaseFunctionsService: InAppPurchaseFunctions,
  ) { }

  public async ionViewWillEnter(): Promise<void> {
    this.user = this.userProvider.getUser();
    this.loadProducts();
  }

  public async onPurchaseSelectedClick(): Promise<void> {
    this.selectedItem = await this.inAppPurchaseFunctionsService.onPurchaseSelectedClick(this.selectedItem, true);
    this.ionViewWillEnter();
  }

  public async onRefreshPurchasesButtonClick(): Promise<void> {
    await this.inAppPurchaseFunctionsService.onRefreshPurchasesButtonClick();
    this.ionViewWillEnter();
  }

  private async loadProducts(): Promise<void> {
    this.isLoading = true;
    const products1 = await this.inAppPurchaseFunctionsService.loadProducts();
    if (products1) {
      this.products = products1;
    }
    this.isLoading = false;
  }

}
