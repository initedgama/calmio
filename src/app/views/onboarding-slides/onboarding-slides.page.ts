import { Component, ViewChild } from '@angular/core';
import { AlertController, IonSlides, NavController, Platform } from '@ionic/angular';
import { UserProvider } from 'src/app/provider/userProvider';
import { AnalyticsProvider } from 'src/app/provider/analyticsProvider';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { File } from '@ionic-native/file/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { VoiceService } from 'src/app/service/VoiceService';
import { VoiceGender } from 'src/app/pojo/voiceGender';
import { TranslateService } from '@ngx-translate/core';
import { LoaderService } from 'src/app/service/LoaderService';
import { CourseProvider } from 'src/app/provider/courseProvider';
import { AlertService } from 'src/app/service/AlertService';
import { CourseShort } from 'src/app/pojo/courseShort';
import { Subscription } from 'rxjs';
import { PushNotificationService } from 'src/app/service/PushNotificationService';
import { User } from '../../pojo/user';
import { InAppPurchaseProduct } from '../../pojo/inAppPurchaseProduct';
import { InAppSubscriptionType } from 'src/app/pojo/inAppSubscriptionTypeEnum';
import { InAppPurchaseService } from '../../service/InAppPurchaseService';
import { InAppSubscriptionId } from '../../pojo/inAppSubscriptionIdEnum';
import { DateTime } from 'luxon';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { UserEvent } from '../../pojo/userEventEnum';
import { InAppPurchaseFunctions } from '../../service/InAppPurchaseFunctions';

@Component({
  selector: 'app-onboarding-slides',
  templateUrl: './onboarding-slides.page.html',
  styleUrls: ['./onboarding-slides.page.scss'],
})
export class OnboardingSlidesPage {
  @ViewChild('slides') public slides: IonSlides;
  public slideOpts: any = {
    direction: 'horizontal',
  };
  public activeIndex: number = 0;
  public forwardEnabled: boolean = true;
  private hardwareBackSubscription: Subscription;
  private slidesSubEnd: Subscription;
  public isLoading: boolean = true;
  public user: User = this.userProvider.getUser();
  public products: InAppPurchaseProduct[];
  public InAppSubscriptionType: typeof InAppSubscriptionType = InAppSubscriptionType;
  public selectedItem: InAppPurchaseProduct;
  public math: Math = Math;

  constructor(
    private userProvider: UserProvider,
    public navCtrl: NavController,
    private analyticsProvider: AnalyticsProvider,
    private file: File,
    private streamingMedia: StreamingMedia,
    private voiceService: VoiceService,
    private translateSvc: TranslateService,
    private loaderService: LoaderService,
    private courseProvider: CourseProvider,
    private alerts: AlertService,
    private platform: Platform,
    private pushNotificationSvc: PushNotificationService,
    private inAppPurchaseService: InAppPurchaseService,
    private iab: InAppBrowser,
    private loading: LoaderService,
    private alertService: AlertService,
    public alertController: AlertController,
    public inAppPurchaseFunctionsService: InAppPurchaseFunctions,
  ) { }

  public ionViewDidEnter(): void {

    this.analyticsProvider.logEvent({ name: AnalyticsEventsEnum.TUTORIAL_BEGIN });
    this.logAnalyticsPageIndex(0);
    this.slidesSubEnd = this.slides.ionSlideTransitionEnd.subscribe(async (evt) => {
      this.activeIndex = await this.slides.getActiveIndex();
      if (this.activeIndex >= 2) {
        this.forwardEnabled = this.userProvider.getIsFullVersionUnlocked();
      } else {
        this.forwardEnabled = true;
      }
      this.slides.lockSwipeToNext(!this.forwardEnabled);
      this.logAnalyticsPageIndex(this.activeIndex);
    });
    this.hardwareBackSubscription = this.platform.backButton.subscribeWithPriority(9999, async () => {
      this.onBackButtonClicked();
      return true;
    });

    this.user = this.userProvider.getUser();
    this.loadProducts();
    this.userProvider.userEvents$.subscribe(evt => {
      switch (evt) {
        case UserEvent.APPLY_PROMO_CODE_SUCCESS:
          this.updateUserAndCheckPurchases();
      }
    });
    this.userProvider.userEvents$.next(UserEvent.ACTIVATE_PROMO);
  }

  public ionViewDidLeave(): void {
    this.hardwareBackSubscription.unsubscribe();
    this.slidesSubEnd.unsubscribe();
  }

  public async onBackButtonClicked(): Promise<void> {
    if (this.activeIndex > 0) {
      this.slides.slidePrev();
    }
  }

  public onContinueButtonClick(): void {
    if (this.activeIndex === 3) {
      this.finalizeOnboarding(false);
    } else {
      this.slides.slideNext();
    }
  }

  public async onVideoClick(): Promise<void> {
    const videoName = 'onboarding.mp4';
    if (this.platform.is('ios')) {
      const url = `${this.file.applicationDirectory}/www/assets/videos/${videoName}`;
      this.streamingMedia.playVideo(url, {
        orientation: 'portrait',
      });
    } else {
      this.userProvider.showPositionFromMenu = true;
      this.userProvider.videoUrl = `../../../../assets/videos/${videoName}`;
      this.navCtrl.navigateForward('onboarding-video');
    }
  }

  public onSkipFirstCourseClick(): void {
    this.finalizeOnboarding(true);
  }

  private async finalizeOnboarding(skipFirstCourse: boolean): Promise<void> {
    if (!this.userProvider.getIsFullVersionUnlocked()) {
      return;
    }
    this.voiceService.setDefaultVoiceGender(VoiceGender.WOMAN);
    this.userProvider.setUserDefaultVoiceGender(VoiceGender.WOMAN);

    await this.loaderService.presentLoader();
    let success = true;
    success = await this.courseProvider.initOnboardingCourse();
    if (success) {
      success = await this.userProvider.signInCourse(this.courseProvider.getActiveCourse() as CourseShort);
    }
    if (success) {
      success = await this.userProvider.saveIntroWizardCompleted();
    }
    await this.loaderService.dismissLoader();
    if (!success) {
      this.alerts.displayToast(this.translateSvc.instant('common.error.toast-connection'), undefined, true);
      return;
    }
    this.analyticsProvider.logEvent({ name: AnalyticsEventsEnum.TUTORIAL_COMPLETE });

    await this.pushNotificationSvc.tryEnable();

    this.userProvider.onOnboardingComplete(skipFirstCourse);
  }

  public async onPurchaseSelectedClick(): Promise<void> {
    this.selectedItem = await this.inAppPurchaseFunctionsService.onPurchaseSelectedClick(this.selectedItem, false);
    this.updateUserAndCheckPurchases();
  }

  public async onRefreshPurchasesButtonClick(): Promise<void> {
    await this.inAppPurchaseFunctionsService.onRefreshPurchasesButtonClick();
    this.updateUserAndCheckPurchases();
    this.loadProducts();
  }

  private updateUserAndCheckPurchases(): void {
    this.user = this.userProvider.getUser();
    this.forwardEnabled = this.userProvider.getIsFullVersionUnlocked();
    this.slides.lockSwipeToNext(!this.forwardEnabled);
  }

  private logAnalyticsPageIndex(index: number): void {
    this.analyticsProvider.setRouteCustom(`onboarding-${index}`);
  }

  private async loadProducts(): Promise<void> {
    this.isLoading = true;
    const products1 = await this.inAppPurchaseFunctionsService.loadProducts();
    if (products1) {
      this.products = products1;
    }
    this.isLoading = false;
  }

}
