import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserProvider } from 'src/app/provider/userProvider';
import { User } from 'src/app/pojo/user';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.page.html',
  styleUrls: ['welcome.page.scss']
})
export class WelcomePage {

  public user: User;

  constructor(
    public platform: PlatformService,
    private navCtrl: NavController,
    private userProvider: UserProvider,
  ) { }

  public ionViewWillEnter(): void {
    this.user = this.userProvider.getUser();
  }

  public async onContinueButtonClick(): Promise<void> {
    this.navCtrl.navigateRoot('list', { animated: true, animationDirection: 'forward' });
  }
}
