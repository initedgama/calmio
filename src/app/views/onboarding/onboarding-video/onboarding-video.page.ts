import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserProvider } from '../../../provider/userProvider';
import { RouterOutletService } from 'src/app/service/RouterOutletService';

@Component({
  selector: 'app-onboarding-video',
  templateUrl: './onboarding-video.page.html',
  styleUrls: ['./onboarding-video.page.scss'],
})
export class OnboardingVideoPage {

  @ViewChild('video') private video?: ElementRef;
  public videoSrc: string = '../../../../assets/videos/onboarding.mp4';
  public showCloseButton: boolean = false;

  constructor(
    public navCtrl: NavController,
    public userProvider: UserProvider,
    private routerOutletService: RouterOutletService,
  ) { }

  public async ionViewDidEnter(): Promise<void> {
    console.log('video ionViewDidEnter');

    this.routerOutletService.swipebackEnabled = false;
    // Kdyz poustim video z menu, muzu zobrazit krizek a odejit z videa.
    this.showCloseButton = this.userProvider.showPositionFromMenu;
    if (this.userProvider.videoUrl) {
      this.videoSrc = this.userProvider.videoUrl;
    }
    this.userProvider.videoUrl = undefined;
    if (this.video) {
      try {
        this.video.nativeElement.src = this.videoSrc;
        await this.video.nativeElement.load();
        await this.video.nativeElement.play();
      } catch (e) {
        console.log(e);
        this.showCloseButton = true;
      }

      this.video.nativeElement.onended = () => {
        this.onContinueButtonClick();
      };
    } else {
      // Nemam video, neco se stalo, nejaka chyba. Pro jistotu zobrazim krizek, abych mohl pokracovat v aplikaci.
      this.showCloseButton = true;
    }
  }

  public ionViewWillLeave(): void {
    console.log('video ionViewWillLeave');
    this.routerOutletService.swipebackEnabled = true;
    this.destroyVideo();
  }

  public async onContinueButtonClick(): Promise<void> {
    console.log(this.userProvider.showPositionFromMenu);
    if (this.userProvider.showPositionFromMenu) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.navigateForward('onboarding-voice', { animated: true, animationDirection: 'forward' });
    }
  }

  public destroyVideo(): void {
    console.log('video ON DESTROY');
    if (this.video) {
      this.video.nativeElement.src = '';
    }
  }

}
