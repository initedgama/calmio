import { Component } from '@angular/core';
import { CourseProvider } from 'src/app/provider/courseProvider';
import { UserProvider } from 'src/app/provider/userProvider';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { User } from 'src/app/pojo/user';
import { CourseShort } from 'src/app/pojo/courseShort';
import { CourseDetail } from 'src/app/pojo/courseDetail';
import { AnalyticsProvider } from 'src/app/provider/analyticsProvider';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { PlatformService } from 'src/app/service/PlatformService';
import { MenuService } from 'src/app/service/MenuService';

@Component({
  selector: 'app-course-purchase',
  templateUrl: './course-purchase.page.html',
  styleUrls: ['./course-purchase.page.scss'],
})
export class CoursePurchasePage {
  public course: CourseShort;
  public courseDetail: CourseDetail;
  public user: User;
  private hardwareBackSubscription: Subscription;

  constructor(
    public platform: PlatformService,
    public userProvider: UserProvider,
    private courseProvider: CourseProvider,
    private navCtrl: NavController,
    private analyticsProvider: AnalyticsProvider,
    private menuService: MenuService,
  ) {
  }

  public ionViewDidEnter(): void {
    this.initContent();
    this.hardwareBackSubscription = this.platform.backButton.subscribeWithPriority(9999, async () => {
      this.onBackButtonClicked();
      return true;
    });
  }

  public ionViewWillLeave(): void {
    this.hardwareBackSubscription.unsubscribe();
  }

  public toggleMenu(): void {
    this.menuService.open();
  }

  public onBackButtonClicked(): void {
    const returnDetailCourse = this.courseProvider.getReturnDetailCourse();
    if (returnDetailCourse) {
      this.courseProvider.setActiveCourse(returnDetailCourse);
      this.navCtrl.navigateBack('course-detail');
    } else {
      this.navCtrl.navigateBack('list');
    }
  }

  public onPurchaseButtonClick(): void {
    if (this.course.free || this.userProvider.getIsFullVersionUnlocked()) {
      const success = this.userProvider.signInCourse(this.course);
      if (success) {
        this.navCtrl.navigateForward('course-detail');
      }
    } else {
      this.navCtrl.navigateForward('full-version');
    }
  }

  private async initContent(): Promise<void> {
    this.course = this.courseProvider.getActiveCourse() as CourseShort;
    this.analyticsProvider.logEvent({
      name: AnalyticsEventsEnum.SELECT_COURSE_PURCHASE_DETAIL,
      params: { course_id: this.course.id, course_name: this.course.name }
    });
    this.user = this.userProvider.getUser();
    this.courseDetail = undefined;
    try {
      this.courseDetail = await this.courseProvider.loadCourseDetail(this.course);
    } catch (e) {
      console.log(e);
      // retry in 1s
      setTimeout(() => {
        if (this.hardwareBackSubscription) { // kontrola ze jsem stale na teto strance
          this.initContent();
        }
      }, 1000);
    }
  }

}
