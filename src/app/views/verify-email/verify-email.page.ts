import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { User } from '../../pojo/user';
import { ApiProvider } from 'src/app/provider/apiProvider';
import { UserProvider } from 'src/app/provider/userProvider';
import { AlertService } from 'src/app/service/AlertService';
import { EmailVerificationService } from 'src/app/service/EmailVerificationService';
import { LoaderService } from 'src/app/service/LoaderService';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage {

  public user: User;
  private hardwareBackSubscription: Subscription;

  constructor(
    private userPvd: UserProvider,
    private apiPvd: ApiProvider,
    private loaderSvc: LoaderService,
    private alertSvc: AlertService,
    private translateSvc: TranslateService,
    private emailVerificationSvc: EmailVerificationService,
    private navCtrl: NavController,
    private platform: PlatformService,
  ) { }

  public ionViewWillEnter(): void {
    this.user = this.userPvd.getUser();
    this.hardwareBackSubscription = this.platform.backButton.subscribeWithPriority(9999, async () => {
      (navigator as any).app.exitApp();
      return true;
    });
  }

  public ionViewDidLeave(): void {
    this.hardwareBackSubscription.unsubscribe();
  }

  public onEmailConfirmedClick(): void {
    this.checkVerificationStatus();
  }

  public onResendEmailClick(): void {
    this.resendVerificationEmail();
  }

  public onSignOutClick(): void {
    this.userPvd.signOut();
  }

  private async checkVerificationStatus(): Promise<void> {
    await this.loaderSvc.presentLoader();
    const user = await this.userPvd.reloadUser();
    await this.loaderSvc.dismissLoader();
    if (!user) {
      this.alertSvc.displayToast(this.translateSvc.instant('common.error.toast-default'));
      return;
    }
    if (user.verificationRequired) {
      this.alertSvc.displayToast(this.translateSvc.instant('view.verify-email.toast-confirmFailed'), 3500);
      return;
    }
    // success
    this.alertSvc.displayToast(this.translateSvc.instant('view.verify-email.toast-confirmSuccess'));
    this.userPvd.onEmailVerificationComplete();
  }

  private async resendVerificationEmail(): Promise<boolean> {
    await this.loaderSvc.presentLoader();
    try {
      await this.apiPvd.callRequest('user/resend-verification-email', 'post', {}).toPromise();
    } catch (err) {
      console.log(err);
      return;
    } finally {
      await this.loaderSvc.dismissLoader();
    }
    // success
    this.alertSvc.displayToast(this.translateSvc.instant('view.verify-email.toast-resendSuccess', { email: this.user.email }), 3500);
  }

}
