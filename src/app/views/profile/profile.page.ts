import { Component, NgZone } from '@angular/core';
import { UserProvider } from 'src/app/provider/userProvider';
import { User } from 'src/app/pojo/user';
import { AlertController, NavController } from '@ionic/angular';
import { AlertService } from 'src/app/service/AlertService';
import { Config } from 'src/app/config';
import { TranslateService } from '@ngx-translate/core';
import { HelperService } from 'src/app/service/HelperService';
import { LoaderService } from 'src/app/service/LoaderService';
import { AuthType } from 'src/app/pojo/authTypeEnum';
import { PlatformService } from 'src/app/service/PlatformService';
import { PushNotificationService } from 'src/app/service/PushNotificationService';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {

  public hideFullVersion: boolean = Config.hideFullVersion;
  public user: User = this.userProvider.getUser();
  public pushNotificationsToggle: boolean = false;
  public AuthType: typeof AuthType = AuthType;
  private pushPermissionSub: Subscription;

  constructor(
    public platform: PlatformService,
    public navCtrl: NavController,
    private userProvider: UserProvider,
    public alertController: AlertController,
    private alertService: AlertService,
    private translateSvc: TranslateService,
    private helperSvc: HelperService,
    private loaderSvc: LoaderService,
    private pushNotificationSvc: PushNotificationService,
    private oneSignal: OneSignal,
    private ngZone: NgZone,
  ) {
  }

  public async ionViewWillEnter(): Promise<void> {
    this.user = this.userProvider.getUser();
    this.updatePushToggleValue();
    this.pushPermissionSub = this.oneSignal.addPermissionObserver().subscribe(evt => {
      this.ngZone.run(() => {
        this.updatePushToggleValue();
      });
    });
  }

  public ionViewDidLeave(): void {
    this.pushPermissionSub.unsubscribe();
  }

  public onEditNameClick(): void {
    this.editSimpleValue('message-name', 'placeholder-name', 'name', 'text');
  }

  public onEditSurameClick(): void {
    this.editSimpleValue('message-surname', 'placeholder-surame', 'surname', 'text');
  }

  // zmena emailu zatim nefunguje na backendu
  /*   public onEditEmailClick(): void {
    this.editSimpleValue('label-newEmail', 'email', 'email', 'email');
  } */

  public onEditPasswordClick(): void {
    this.editPassword();
  }

  public async onSignOutButtonClick(): Promise<void> {
    await this.userProvider.signOut();
  }

  public async onDeleteAccountClick(): Promise<void> {
    const prompt = await this.alertController.create({
      header: this.translateSvc.instant('delete-account.alert-confirmDeleteAccount.title'),
      message: this.translateSvc.instant('delete-account.alert-confirmDeleteAccount.text'),
      buttons: [
        {
          text: this.translateSvc.instant('delete-account.alert-confirmDeleteAccount.button-cancel'),
          role: 'cancel',
        },
        {
          text: this.translateSvc.instant('delete-account.alert-confirmDeleteAccount.button-confirm'),
          role: 'confirm'
        },
      ],
      cssClass: 'delete-account-alert',
    });
    await prompt.present();
    const result = await prompt.onDidDismiss();
    if (result.role === 'confirm') {
      this.userProvider.deleteAccount();
    }
  }

  public getUserInitials(): string {
    return (this.user.name ? this.user.name[0] : '') + (this.user.surname ? this.user.surname[0] : '');
  }

  public async onTogglePushClick(): Promise<void> {
    const shouldEnable = !this.pushNotificationsToggle;
    await this.pushNotificationSvc.setEnabled(shouldEnable);
    this.user = this.userProvider.getUser();
    this.updatePushToggleValue();
  }

  public onLangChange(): void {
    // aktualizuji nastaveni jazyka na BE, jazyk se posila vzdy, tak neni potreba plnit zde
    this.userProvider.updateUser({});
  }

  private async editSimpleValue(
    msgTranslateKey: string,
    placeholderTranslateKey: string,
    userKey: string,
    inputMode: 'email' | 'text',
    validator?: 'email',
    initialPromptValue?: string
  ): Promise<void> {
    const newValue = await this.promptSimpleValue(msgTranslateKey, placeholderTranslateKey, inputMode, initialPromptValue);
    if (newValue === undefined) {
      // user cancelled
      return;
    }
    if (!newValue) {
      this.alertService.displayToast(this.translateSvc.instant('view.profile.toast-missingFields'));
      this.editSimpleValue(msgTranslateKey, placeholderTranslateKey, userKey, inputMode, validator);
      return;
    }
    if (validator === 'email' && !this.helperSvc.validateEmail(newValue)) {
      this.alertService.displayToast(this.translateSvc.instant('view.profile.toast-invalidEmail'));
      this.editSimpleValue(msgTranslateKey, placeholderTranslateKey, userKey, inputMode, validator, newValue);
      return;
    }
    await this.loaderSvc.presentLoader();
    const success = await this.userProvider.updateUser({ [userKey]: newValue });
    await this.loaderSvc.dismissLoader();
    if (success) {
      this.user = this.userProvider.getUser();
      this.alertService.displayToast(this.translateSvc.instant('view.profile.toast-editSuccess'));
    }
  }

  private async promptSimpleValue(
    messageTranslateKey: string,
    placeholderTranslateKey: string,
    inputMode: 'text' | 'email',
    initialValue: string = ''
  ): Promise<string | undefined> {
    const header = this.translateSvc.instant('view.profile.alert-simpleChange.header');
    const label = this.translateSvc.instant('view.profile.alert-simpleChange.' + messageTranslateKey);
    const prompt = await this.alertController.create({
      header,
      message: label,
      backdropDismiss: false,
      inputs: [
        {
          type: inputMode,
          name: 'input',
          value: initialValue,
          placeholder: this.translateSvc.instant('view.profile.alert-simpleChange.' + placeholderTranslateKey),
        }
      ],
      buttons: [
        {
          text: this.translateSvc.instant('view.profile.alert-simpleChange.button-cancel'),
          role: 'cancel',
        },
        {
          text: this.translateSvc.instant('view.profile.alert-simpleChange.button-confirm'),
          role: 'confirm'
        },
      ]
    });
    await prompt.present();
    const details = await prompt.onDidDismiss();
    if (details.role === 'cancel') {
      return undefined;
    } else {
      return details.data.values.input;
    }
  }

  private async editPassword(): Promise<void> {
    const values = await this.promptNewPassword();
    if (values === undefined) {
      // user cancelled
      return;
    }
    if (!values.old || !values.new) {
      this.alertService.displayToast(this.translateSvc.instant('view.profile.toast-missingFields'));
      this.promptNewPassword();
      return;
    }
    await this.loaderSvc.presentLoader();
    const success = await this.userProvider.updateUser({ passwordOld: values.old, passwordNew: values.new });
    await this.loaderSvc.dismissLoader();
    if (success) {
      this.user = this.userProvider.getUser();
      this.alertService.displayToast(this.translateSvc.instant('view.profile.toast-editSuccess'));
    }
  }

  private async promptNewPassword(
  ): Promise<{ old: string, new: string } | undefined> {
    const header = this.translateSvc.instant('view.profile.alert-passwordChange.header');
    const label = this.translateSvc.instant('view.profile.alert-passwordChange.message');
    const prompt = await this.alertController.create({
      header,
      message: label,
      backdropDismiss: false,
      inputs: [
        {
          type: 'password',
          name: 'inputOld',
          placeholder: this.translateSvc.instant('view.profile.alert-passwordChange.placeholder-oldPassword')
        },
        {
          type: 'password',
          name: 'inputNew',
          placeholder: this.translateSvc.instant('view.profile.alert-passwordChange.placeholder-newPassword')
        },
      ],
      buttons: [
        {
          text: this.translateSvc.instant('view.profile.alert-passwordChange.button-cancel'),
          role: 'cancel',
        },
        {
          text: this.translateSvc.instant('view.profile.alert-passwordChange.button-confirm'),
          role: 'confirm'
        },
      ]
    });
    await prompt.present();
    const details = await prompt.onDidDismiss();
    if (details.role === 'cancel') {
      return undefined;
    } else {
      return {
        old: details.data.values.inputOld,
        new: details.data.values.inputNew,
      };
    }
  }

  private async updatePushToggleValue(): Promise<void> {
    this.pushNotificationsToggle = this.user.pushNotification && (await this.pushNotificationSvc.isSystemEnabled());
  }
}
