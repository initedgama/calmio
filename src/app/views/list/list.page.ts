import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CourseProvider } from 'src/app/provider/courseProvider';
import { UserProvider } from 'src/app/provider/userProvider';
import { User } from 'src/app/pojo/user';
import { CourseShort } from 'src/app/pojo/courseShort';
import { CourseDetail } from 'src/app/pojo/courseDetail';
import { AlertService } from 'src/app/service/AlertService';
import { TranslateService } from '@ngx-translate/core';
import { PlatformService } from 'src/app/service/PlatformService';
import { MenuService } from 'src/app/service/MenuService';
import { LoaderService } from 'src/app/service/LoaderService';
import { HelperService } from 'src/app/service/HelperService';
import { Config } from 'src/app/config';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage {

  public resumeCourse: CourseShort;
  public resumeCourseLessonIndex: number;
  public dailyMeditation: CourseShort;
  public ownedCourses: CourseShort[];
  public recommendedCourses: CourseShort[];
  public nextCourses: CourseShort[];
  public user: User;
  public nowTimestamp: number = Date.now();

  constructor(
    public navCtrl: NavController,
    public platform: PlatformService,
    public userProvider: UserProvider,
    public translate: TranslateService,
    private courseProvider: CourseProvider,
    private alerts: AlertService,
    private translateSvc: TranslateService,
    private menuService: MenuService,
    private loaderService: LoaderService,
    private helperService: HelperService,
  ) {
  }

  public async ionViewWillEnter(): Promise<void> {
    this.initialize();
  }

  public toggleMenu(): void {
    this.menuService.open();
  }

  public onOwnedCourseItemClick(course: CourseShort): void {
    this.courseProvider.setActiveCourse(course);
    if (course.free || this.userProvider.getIsFullVersionUnlocked()) {
      this.navCtrl.navigateForward('course-detail');
    } else {
      this.navCtrl.navigateForward('course-purchase');
    }
  }

  public async onNextCourseItemClick(course: CourseShort): Promise<void> {
    if (!course.free && !this.userProvider.getIsFullVersionUnlocked()) {
      this.courseProvider.setActiveCourse(course);
      this.navCtrl.navigateForward('course-purchase');
      return;
    }
    await this.loaderService.presentLoader();
    const successSignIn = await this.userProvider.signInCourse(course);
    await this.loaderService.dismissLoader();
    if (!successSignIn) {
      this.alerts.displayToast(this.translateSvc.instant('common.error.toast-connection'));
      return;
    }
    this.courseProvider.setActiveCourse(course);
    this.navCtrl.navigateForward('course-detail');
  }

  public onConsultationItemClicked(): void {
    if (this.userProvider.getIsFullVersionUnlocked()) {
      this.helperService.launchNestedBrowser(Config.consultationUrl, '#f5f7fa');
    } else {
      this.navCtrl.navigateForward('full-version');
    }
  }

  public async onDailyCourseClick(): Promise<void> {
    if (!this.userProvider.getIsFullVersionUnlocked()) {
      this.courseProvider.setActiveCourse(this.dailyMeditation);
      this.navCtrl.navigateForward('course-purchase');
      return;
    }
    const user = this.userProvider.getUser();
    if (!user.coursesOwned.find(it => it.id === this.dailyMeditation.id)) {
      await this.loaderService.presentLoader();
      const successSignIn = await this.userProvider.signInCourse(this.dailyMeditation);
      await this.loaderService.dismissLoader();
      if (!successSignIn) {
        this.alerts.displayToast(this.translateSvc.instant('common.error.toast-connection'));
        return;
      }
    }
    this.courseProvider.setActiveCourse(this.dailyMeditation);
    this.navCtrl.navigateForward('course-detail');
  }

  public async onResumeCourseClick(): Promise<void> {
    try {
      await this.loaderService.presentLoader();
      this.courseProvider.setActiveCourse(this.resumeCourse);
      const detail: CourseDetail = await this.courseProvider.loadCourseDetail(this.resumeCourse);
      this.courseProvider.setActiveCourseDetail(detail);
      this.courseProvider.setActiveLessonIndex(this.resumeCourseLessonIndex);
      this.courseProvider.setPlaybackPageReturnInfo({
        direction: 'back',
        page: 'list',
      });
      this.navCtrl.navigateForward('playback');
    } catch (e) {
      this.alerts.displayToast(this.translateSvc.instant('common.error.toast-connection'));
    } finally {
      await this.loaderService.dismissLoader();
    }
  }

  public onPullRefresh(event: { target: { complete: () => {} } }): void {
    this.initialize(true);
    setTimeout(() => event.target.complete());
  }

  public onBlogClick(): void {
    this.helperService.launchNestedBrowser(
      this.userProvider.getIsFullVersionUnlocked() ? Config.premiumUserBlogUrl : Config.freeUserBlogUrl
    );
  }

  private async initialize(forceReload: boolean = false): Promise<void> {
    await this.userProvider.maybeServerReloadUserAndCourses(forceReload);
    this.loadDailyMeditation();
    this.loadContent();
  }

  private loadDailyMeditation(): void {
    this.dailyMeditation = this.courseProvider.getCurrentDailyMediation();
  }

  private loadContent(): void {
    this.user = this.userProvider.getUser();
    const allCourses = this.courseProvider.getAllCourses();
    const activeLangCoursesOwned = this.user.coursesOwned
      .filter(crse => allCourses.some(c => crse.id === c.id && (!c.languages || c.languages.includes(this.translateSvc.currentLang))));
    const activeLangCoursesAll = allCourses
      .filter(c => !c.languages || c.languages.includes(this.translateSvc.currentLang));
    this.ownedCourses = activeLangCoursesOwned.map(crse => allCourses.find(c => crse.id === c.id));

    // TODO nova logika doporucenych kurzu, podle srovnani s tagy uzivatele
    const allUserTags = this.ownedCourses.reduce((a, b) => {
      b.tags.forEach(t => {
        if (a.indexOf(t) === -1) {
          a.push(t);
        }
      });
      return a;
    }, []);
    this.recommendedCourses = activeLangCoursesAll
      .filter(course => {
        return this.ownedCourses.find(c => c.id === course.id) === undefined
          && course.tags.find(t => allUserTags.indexOf(t) > -1);
      });
    this.nextCourses = activeLangCoursesAll
      .filter(course => {
        return this.ownedCourses.find(c => c.id === course.id) === undefined
          && this.recommendedCourses.find(c => c.id === course.id) === undefined;
      });

    // nastavit ktery kurz bude nabizen k pokracovani, vybrat ten ktery byl navstiveny jako posledni
    this.resumeCourse = undefined;
    this.resumeCourseLessonIndex = undefined;
    const unfinishedOwnedCourses = activeLangCoursesOwned
      .filter(crse => {
        const courseShort = allCourses.find(it => it.id === crse.id);
        return crse.lessonsFinished.length
          && (courseShort.free || this.userProvider.getIsFullVersionUnlocked())
          && crse.lessonsFinished.filter(l => l.completed).length < courseShort.lessons.length;
      });
    if (unfinishedOwnedCourses.length) {
      const highestLessonFinishedTimestampCourseList = unfinishedOwnedCourses
        .map(course => ({ course, timestamp: Math.max(...course.lessonsFinished.map(l => l.timestamp)) }));
      const highestTimestamp = Math.max(...highestLessonFinishedTimestampCourseList.map(item => item.timestamp));
      const highestTimestampCourse = highestLessonFinishedTimestampCourseList.find(item => item.timestamp === highestTimestamp).course;
      if (highestTimestampCourse) {
        this.resumeCourse = allCourses.find(crse => crse.id === highestTimestampCourse.id);
        this.resumeCourseLessonIndex = this.courseProvider.getResumeLessonIndex(this.resumeCourse, this.user);
      }
    }
  }
}
