import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FacebookAuthService } from 'src/app/service/FacebookAuthService';
import { GoogleAuthService } from 'src/app/service/GoogleAuthService';
import { UserProvider } from 'src/app/provider/userProvider';
import { AlertService } from 'src/app/service/AlertService';
import { LoaderService } from 'src/app/service/LoaderService';
import { ExternalProviderUser } from 'src/app/pojo/externalProviderUser';
import { OAuthProviderEnum } from 'src/app/pojo/oAuthProviderEnum';
import { AnalyticsLoginMethod } from 'src/app/pojo/analyticsLoginMethodEnum';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { AnalyticsEvent } from 'src/app/pojo/analyticsEvent';
import { TranslateService } from '@ngx-translate/core';
import { AppleAuthService } from 'src/app/service/AppleAuthService';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-intro',
  templateUrl: 'intro.page.html',
  styleUrls: ['intro.page.scss']
})
export class IntroPage {
  public langSelectValue: 'cs' | 'sk' = 'cs';

  constructor(
    public platform: PlatformService,
    public navCtrl: NavController,
    private facebookAuthService: FacebookAuthService,
    private googleAuthService: GoogleAuthService,
    private userProvider: UserProvider,
    private alertService: AlertService,
    private loaderService: LoaderService,
    private translateSvc: TranslateService,
    private appleAuthService: AppleAuthService,
  ) { }

  public onSignUpButtonClick(): void {
    this.navCtrl.navigateForward('sign-up');
  }
  public onSignInButtonClick(): void {
    this.navCtrl.navigateForward('sign-in');
  }

  public async onSignInWithFacebookButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.facebookAuthService.signIn();
    } catch (err) {
      console.log('FB LOGIN ERROR', err);
      let errMsg = this.translateSvc.instant('view.intro.toast-loginFailed');
      if (err && err.errorMessage) {
        errMsg += `\n\n${err.errorMessage}`;
      }
      this.alertService.displayToast(errMsg, 4000, true);
    }
    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.FACEBOOK } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.intro.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
      } finally {
        await this.loaderService.dismissLoader();
      }
    }
  }

  public async onSignInWithGoogleButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.googleAuthService.signIn();
    } catch (e) {
      this.alertService.displayToast(this.translateSvc.instant('view.intro.toast-loginFailed'), undefined, true);
      console.log('GOOGLE LOGIN ERROR', e);
    }
    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.GOOGLE } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.intro.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
      } finally {
        await this.loaderService.dismissLoader();
      }
    }
  }

  public async onSignInWithAppleButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.appleAuthService.signIn();
    } catch (e) {
      this.alertService.displayToast(this.translateSvc.instant('view.intro.toast-loginFailed'), undefined, true);
      console.log('APPLE LOGIN ERROR', e);
    }
    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        await this.loaderService.dismissLoader();
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.APPLE } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.intro.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
      } finally {
        await this.loaderService.dismissLoader();
      }
    }
  }

  private async onSignInSuccess(event: AnalyticsEvent): Promise<void> {
    this.userProvider.onAuthComplete(true, true, event);
  }

}
