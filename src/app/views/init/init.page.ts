import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiProvider } from 'src/app/provider/apiProvider';
import { NavController } from '@ionic/angular';
import { UserProvider } from 'src/app/provider/userProvider';
import { StorageKeysEnum } from 'src/app/pojo/storageKeysEnum';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AnalyticsProvider } from 'src/app/provider/analyticsProvider';
import { AppVersionService } from 'src/app/service/AppVersionService';
import { TranslateService } from '@ngx-translate/core';
import { Config } from 'src/app/config';
import { AppInitService } from 'src/app/service/AppInitService';
import { PromoService } from 'src/app/service/PromoService';
import { DeeplinkService } from 'src/app/service/DeeplinkService';
import { SmartlookService } from 'src/app/service/SmartlookService';
import { PushNotificationService } from 'src/app/service/PushNotificationService';
import { PlatformService } from 'src/app/service/PlatformService';
import { AppRateService } from 'src/app/service/AppRateService';
import { InAppTriggerService } from 'src/app/service/InAppTriggerService';
import { DeviceIdService } from 'src/app/service/DeviceIdService';
import localeCs from '@angular/common/locales/cs';
import { registerLocaleData } from '@angular/common';

@Component({
  selector: 'app-init',
  templateUrl: './init.page.html',
  styleUrls: ['./init.page.scss'],
})
export class InitPage implements OnInit {

  constructor(
    private storage: Storage,
    private apiProvider: ApiProvider,
    private navCtrl: NavController,
    private userProvider: UserProvider,
    private splashScreen: SplashScreen,
    private analyticsProvider: AnalyticsProvider,
    private appVersionSvc: AppVersionService,
    private translateSvc: TranslateService,
    private platform: PlatformService,
    private appInitSvc: AppInitService,
    private deeplinksSvc: DeeplinkService,
    private promoSvc: PromoService,
    private smartlookSvc: SmartlookService,
    private pushService: PushNotificationService,
    private appRateService: AppRateService,
    private inAppTriggerService: InAppTriggerService,
    private deviceIdService: DeviceIdService,
  ) { }

  public ngOnInit(): void {
    this.platform.ready().then(async () => {
      this.appInitSvc.ready = true;

      this.analyticsProvider.init();
      this.deeplinksSvc.initialize();
      this.promoSvc.initialize();
      this.smartlookSvc.init();
      this.pushService.init();
      this.setLanguage();
      this.appRateService.initialize();
      this.inAppTriggerService.initialize();
      await this.deviceIdService.initialize();

      const token = await this.storage.get(StorageKeysEnum.API_TOKEN);

      if (token) {
        await this.apiProvider.setToken(token);
        const successInit = await this.userProvider.initializeUserAndCourses();
        if (successInit) {
          this.userProvider.onAuthComplete(false, false);
        } else {
          await this.navCtrl.navigateRoot('intro');
        }
      } else {
        await this.navCtrl.navigateRoot('intro');
      }
      this.splashScreen.hide();
      this.appVersionSvc.initialize();
    });
  }

  private async setLanguage(): Promise<void> {
    registerLocaleData(localeCs, 'cs');

    // nastaveni jazyka podle locale uzivatele
    const storedLang = await this.storage.get(StorageKeysEnum.LANGUAGE);
    const browserLang = this.translateSvc.getBrowserLang();

    let langToUse: string;
    if (Config.availableLanguages.includes(storedLang)) {
      langToUse = storedLang;
    } else if (Config.availableLanguages.includes(browserLang)) {
      langToUse = browserLang;
    } else {
      langToUse = this.translateSvc.getDefaultLang();
    }
    this.translateSvc.use(langToUse);

    this.translateSvc.onLangChange.subscribe(({ lang }) => {
      // ulozim pro priste zmene jazyka userem
      this.storage.set(StorageKeysEnum.LANGUAGE, lang);
    });
  }
}
