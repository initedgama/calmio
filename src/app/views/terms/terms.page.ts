import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiProvider } from 'src/app/provider/apiProvider';
import { Subscription } from 'rxjs';
import { retryWhen, delay } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-terms',
  templateUrl: 'terms.page.html',
  styleUrls: ['terms.page.scss'],
})
export class TermsPage {

  public termsHtml: string;
  private loadSub: Subscription;

  constructor(
    public platform: PlatformService,
    public navCtrl: NavController,
    private apiProvider: ApiProvider,
    private translateSvc: TranslateService,
  ) {
  }

  public ionViewDidEnter(): void {
    this.loadContent();
  }

  public ionViewWillLeave(): void {
    if (this.loadSub) {
      this.loadSub.unsubscribe();
    }
  }

  public async loadContent(): Promise<void> {
    this.loadSub = this.apiProvider.callRequest('static', 'get', { id: 'TERMS_AND_CONDITIONS', language: this.translateSvc.currentLang })
      .pipe(retryWhen(errors =>
        errors.pipe(delay(1000))
      ))
      .subscribe((result: { html: string }) => {
        this.termsHtml = result.html;
      });
  }

}
