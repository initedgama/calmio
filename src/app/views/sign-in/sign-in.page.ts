import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonContent, IonInput, NavController } from '@ionic/angular';
import { UserProvider } from 'src/app/provider/userProvider';
import { AlertService } from 'src/app/service/AlertService';
import { LoaderService } from 'src/app/service/LoaderService';
import { HelperService } from 'src/app/service/HelperService';
import { AnalyticsEvent } from 'src/app/pojo/analyticsEvent';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { AnalyticsLoginMethod } from 'src/app/pojo/analyticsLoginMethodEnum';
import { TranslateService } from '@ngx-translate/core';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-sign-in',
  templateUrl: 'sign-in.page.html',
  styleUrls: ['sign-in.page.scss']
})
export class SignInPage {
  @ViewChild('container', { read: ElementRef }) public container: ElementRef;
  @ViewChild('content') public contentRef: IonContent;
  @ViewChild('formWrap') public formWrap: ElementRef;

  public inputs: { email: string, password: string } = {
    email: '',
    password: ''
  };

  constructor(
    public platform: PlatformService,
    public navCtrl: NavController,
    private userProvider: UserProvider,
    private alertService: AlertService,
    private loaderService: LoaderService,
    private helperService: HelperService,
    private translateSvc: TranslateService,
  ) { }

  public onInputFocus(evt: any): void {
    if (this.platform.is('ios')) {
      if (!(window as any).Keyboard.isVisible) {
        setTimeout(() => {
          this.contentRef.scrollToPoint(0, this.formWrap.nativeElement.getBoundingClientRect().y - 10, 400);
        }, 500);
      }
    }
  }

  public async onInputKeyDown(evt: KeyboardEvent, nextIonInput?: IonInput): Promise<void> {
    if (evt.key !== 'Enter') {
      return;
    }
    evt.preventDefault();
    if (nextIonInput) {
      nextIonInput.getInputElement().then(input => input.focus());
    } else {
      this.onSubmitButtonClick();
    }
  }

  public async onSubmitButtonClick(): Promise<void> {
    if (!this.inputs.email || !this.inputs.password) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-in.toast-enterEmailAndPassword'), undefined, true);
      return;
    } else if (!this.helperService.validateEmail(this.inputs.email)) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-in.toast-enterValidEmail'), undefined, true);
      return;
    }
    await this.loaderService.presentLoader();
    const success = await this.userProvider.signInWithPassword({ email: this.inputs.email, password: this.inputs.password });
    await this.loaderService.dismissLoader();
    if (success) {
      const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.EMAIL } };
      this.onSignInSuccess(event);
    }
  }

  public async onForgotPasswordButtonClick(): Promise<void> {
    if (!this.inputs.email) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-in.toast-enterEmail'), undefined, true);
      return;
    } else if (!this.helperService.validateEmail(this.inputs.email)) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-in.toast-enterValidEmail'), undefined, true);
      return;
    }
    await this.loaderService.presentLoader();
    const success = await this.userProvider.requestPasswordReset({ email: this.inputs.email });
    await this.loaderService.dismissLoader();
    if (success) {
      await this.alertService.displayAlert({
        header: this.translateSvc.instant('view.sign-in.alert-resetPasswordSuccess.title'),
        message: this.translateSvc.instant('view.sign-in.alert-resetPasswordSuccess.text'),
        buttons: [
          {
            text: this.translateSvc.instant('view.sign-in.alert-resetPasswordSuccess.button')
          }
        ]
      });
    }
  }

  public isSubmitEnabled(): boolean {
    return !!this.inputs.email && !!this.inputs.password;
  }

  private async onSignInSuccess(event: AnalyticsEvent): Promise<void> {
    this.userProvider.onAuthComplete(true, true, event);
  }
}
