import { VoiceGender } from '../../pojo/voiceGender';
import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { AlertController, IonSlides, NavController } from '@ionic/angular';
import { CourseProvider } from 'src/app/provider/courseProvider';
import { Lesson } from 'src/app/pojo/lesson';
import { UserProvider } from 'src/app/provider/userProvider';
import { AudioService } from 'src/app/service/audioService';
import { Subscription } from 'rxjs';
import { CourseDetail } from 'src/app/pojo/courseDetail';
import { AlertService } from 'src/app/service/AlertService';
import { Brightness } from '@ionic-native/brightness/ngx';
import { AnalyticsProvider } from 'src/app/provider/analyticsProvider';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { ShareService } from 'src/app/service/ShareService';
import { FileStorageService } from 'src/app/service/FileStorageService';
import { VideoService } from 'src/app/service/VideoService';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { RouterOutletService } from 'src/app/service/RouterOutletService';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { TranslateService } from '@ngx-translate/core';
import { VoiceService } from '../../service/VoiceService';
import { Source } from '../../pojo/source';
import { PlatformService } from 'src/app/service/PlatformService';
import { CourseTypeEnum } from 'src/app/pojo/courseTypeEnum';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

declare var require: any;

const ProgressBar = require('progressbar.js');
const AFTER_PLAYBACK_CLEANUP_TIMEOUT_MS = 25 * 1000;
@Component({
  selector: 'app-playback',
  templateUrl: 'playback.page.html',
  styleUrls: ['playback.page.scss']
})
export class PlaybackPage {
  @ViewChild('slides') public slides: IonSlides;

  public course: CourseDetail;
  public lesson: Lesson;
  public lessonLoading: boolean = false;
  public lessonLoaded: boolean = false;
  public lessonStarted: boolean = false;
  public lessonCompleted: boolean = false;
  public totalUserMeditationTimeIncludingCurrentSessionMinutes: number;
  public totalUserContinuousMeditationDays: number;
  public lessonPlaying: boolean = false;
  public lessonCloseRequested: boolean = false;
  public hasError: boolean = false;
  public selectedLessonVariantUrl: string;
  public currentTime: string;
  public introImageUrl: string | SafeResourceUrl;
  public pageInitialized: boolean = false;
  public progressBar: { animate: (progress: number) => void };
  public slideOpts: any = {
    slidesPerView: 5,
    centeredSlides: true,
    initialSlide: 0,
  };
  public downloadProgress: number;
  private currentLessonVariantDuration: number;
  private preloadingSub: Subscription;
  private afterPlaybackCleanupTimeout: any;

  private positionCheckerInterval: any;
  private hardwareBackSubscription: Subscription;

  public preferredVoiceId: number;
  public sources: Array<{ title: string, sources: Array<Source> }> = [];
  public CourseTypeEnum: typeof CourseTypeEnum = CourseTypeEnum;

  @ViewChild('video') private video?: ElementRef;
  @ViewChild('videoIntroElement') private videoIntroElement?: ElementRef;

  constructor(
    public platform: PlatformService,
    public alertController: AlertController,
    public courseProvider: CourseProvider,
    private alerts: AlertService,
    private navCtrl: NavController,
    private userProvider: UserProvider,
    private audioService: AudioService,
    private videoService: VideoService,
    private ngZone: NgZone,
    private brightness: Brightness,
    private analyticsProvider: AnalyticsProvider,
    private shareService: ShareService,
    private fileStorage: FileStorageService,
    private sanitizer: DomSanitizer,
    private routerOutletService: RouterOutletService,
    private backgroundMode: BackgroundMode,
    private translateSvc: TranslateService,
    private voiceService: VoiceService,
    private screenOrientation: ScreenOrientation,
  ) {
  }

  public async ionViewDidEnter(): Promise<void> {
    this.hardwareBackSubscription = this.platform.backButton.subscribeWithPriority(9999, async () => {
      this.lessonCompleted ? this.navigateAway() : this.onBackButtonClicked();
      return true;
    });
    if (this.platform.is('android')) {
      document.addEventListener('fullscreenchange', this.onFullScreenChange, false);
    }
    this.course = this.courseProvider.getActiveCourseDetail();
    this.lesson = this.course.lessons[this.courseProvider.getActiveLessonIndex()];

    for (const source of this.lesson.source) {
      if (!this.sources.find(v => v.title === source.title)) {
        const object: any = { title: source.title, sources: [] };
        for (const source2 of this.lesson.source) {
          if (source2.title === source.title) {
            object.sources.push(source2);
          }
        }
        this.sources.push(object);
      }
    }
    this.preferredVoiceId = await this.voiceService.getLessonPreferredVoiceId(this.lesson, this.course.id);

    // tslint:disable-next-line: triple-equals
    if (this.preferredVoiceId != undefined) {
      // pokud existuje nahravka/nahravky v hlase preferovaneho lektora, omezim vyber pouze na tyto
      const sourcesFilteredByPrefVoice = [];
      this.sources.forEach(source => {
        const filtered = source.sources.filter(src => src.lecturer.id === this.preferredVoiceId);
        if (filtered.length) {
          sourcesFilteredByPrefVoice.push({ ...source, sources: filtered });
        }
      });
      if (sourcesFilteredByPrefVoice.length) {
        this.sources = sourcesFilteredByPrefVoice;
      }
    }

    this.slideOpts.initialSlide = 0;
    this.selectedLessonVariantUrl = this.sources[0].sources[0].src;

    this.routerOutletService.swipebackEnabled = false;
    this.updateMediaCenterTextIOS();

    this.analyticsProvider.logEvent({
      name: AnalyticsEventsEnum.SELECT_LESSON_DETAIL,
      params: { course_id: this.course.id, course_name: this.course.name, lesson_id: this.lesson.id, lesson_order: this.lesson.order, }
    });

    this.positionCheckerInterval = setInterval(async () => {
      let timeProgress;
      if (this.lesson.type === 'VIDEO') {
        if (this.video) {
          if (this.video.nativeElement.currentTime) {
            this.currentTime = this.getReadableTime(this.video.nativeElement.currentTime, true);
            timeProgress = (this.video.nativeElement.currentTime + 0.25) / this.video.nativeElement.duration;
          }
        }
      } else {
        const currentSecs = this.audioService.getCurrentPosition(this.selectedLessonVariantUrl);
        if (currentSecs && currentSecs > 0.01) {
          const duration = this.audioService.getDuration(this.selectedLessonVariantUrl);
          this.currentTime = this.getReadableTime(currentSecs, true);
          timeProgress = (currentSecs + 0.25) / duration;
        }
      }
      if (timeProgress && !isNaN(timeProgress)) {
        this.progressBar.animate(timeProgress);
      }
    }, 250);

    this.maybeShowIntroImage();

    this.pageInitialized = true;
    setTimeout(() => {
      this.initializeProgressBar();
    });
  }

  public ionViewWillLeave(): void {
    this.backgroundMode.disable();
    if (this.hardwareBackSubscription) {
      this.hardwareBackSubscription.unsubscribe();
    }
    if (this.preloadingSub) {
      this.preloadingSub.unsubscribe();
    }
    clearInterval(this.positionCheckerInterval);
    this.lesson = undefined;
    this.pageInitialized = false;
    clearTimeout(this.afterPlaybackCleanupTimeout);
    this.brightness.setKeepScreenOn(false);
    this.routerOutletService.swipebackEnabled = true;
    if (this.platform.is('android')) {
      document.removeEventListener('fullscreenchange', this.onFullScreenChange);
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY);
    }
  }

  public async onBackButtonClicked(): Promise<void> {
    if (this.lessonCloseRequested) {
      // pokud uz uzivatel jednou klepnul zpet, nezkousim znovu odeslat info na server a jdu rovnou pryc
      this.navigateAway();
      return;
    }
    this.lessonCloseRequested = true;
    let currentTime, duration, voice;
    try {
      currentTime = this.getCurrentTime();
      duration = this.getCurrentLessonVariantDuration();
      voice = this.lesson.source.find(it => it.src === this.selectedLessonVariantUrl).lecturer;
      if (this.lessonPlaying) {
        if (this.lesson) {
          if (this.lesson.type === 'VIDEO') {
            this.video.nativeElement.pause();
          } else {
            this.stopAudio();
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
    if (currentTime >= duration) {
      await this.userProvider.saveLessonEnded(this.course, this.lesson, duration, true, duration, voice);
    } else {
      // nedokoncena lekce, ulozime na server
      await this.userProvider.saveLessonEnded(this.course, this.lesson, currentTime, false, duration, voice);
    }
    this.navigateAway();
  }

  public onShareButtonClicked(): void {
    this.shareService.shareLessonFinished(
      this.totalUserMeditationTimeIncludingCurrentSessionMinutes,
      this.courseProvider.getActiveLessonIndex(),
      this.courseProvider.getActiveCourse(),
    );
    if (this.lesson && this.course) {
      this.analyticsProvider.logEvent({
        name: AnalyticsEventsEnum.SHARE,
        params: {
          course_id: this.course.id,
          course_name: this.course.name,
          lesson_id: this.lesson.id,
          lesson_order: this.lesson.order,
          total_minutes_meditated: this.totalUserMeditationTimeIncludingCurrentSessionMinutes
        }
      });
    }
  }

  public onControlButtonClick(): void {
    if (!this.lessonStarted) {
      this.analyticsProvider.logEvent({
        name: AnalyticsEventsEnum.LESSON_STARTED,
        params: { course_id: this.course.id, course_name: this.course.name, lesson_id: this.lesson.id, lesson_order: this.lesson.order, }
      });
      this.userProvider.addPlaybackHistoryEvent();
    }
    if (this.lessonPlaying) {
      this.brightness.setKeepScreenOn(false);
      this.backgroundMode.disable();
    } else {
      this.brightness.setKeepScreenOn(true);
      this.backgroundMode.enable();
    }

    if (this.lesson) {
      if (this.lesson.type === 'VIDEO') {
        if (this.lessonPlaying) {
          this.pauseVideo();
        } else {
          this.playVideo();
        }
      } else {
        if (this.lessonPlaying) {
          this.pauseAudio();
        } else {
          this.playAudio();
        }
      }
    }
  }

  public async onLessonCompletedOverlayClick(): Promise<void> {
    this.navigateAway();
  }

  public async onVariantSlideChanged(): Promise<void> {
    const slideIndex = await this.slides.getActiveIndex();
    this.selectedLessonVariantUrl = this.sources[slideIndex].sources[0].src;
  }

  private onPlaybackEnded(): void {
    this.ngZone.run(() => {
      this.lessonPlaying = false;
      this.finalizeLesson();
    });
    this.afterPlaybackCleanupTimeout = setTimeout(() => {
      this.brightness.setKeepScreenOn(false);
      this.backgroundMode.disable();
    }, AFTER_PLAYBACK_CLEANUP_TIMEOUT_MS);
  }

  private onPlaybackError(err: string | Event): void {
    this.ngZone.run(() => {
      if (this.lessonPlaying) {
        this.hasError = true;
        if (this.lesson.type === 'AUDIO') {
          this.lessonPlaying = false;
          this.audioService.pause(this.selectedLessonVariantUrl);
        } else {
          // TODO handle video error
        }
      }
    });
  }

  private async finalizeLesson(): Promise<void> {
    this.currentLessonVariantDuration = this.getCurrentLessonVariantDuration();
    this.totalUserMeditationTimeIncludingCurrentSessionMinutes =
      Math.ceil((this.userProvider.getTotalMeditationTime() + this.currentLessonVariantDuration) / 60);
    this.totalUserContinuousMeditationDays = await this.userProvider.getContinuousMeditationDaysCount();
    // Pokud medituji prvni den, tak je to 0 dni v rade
    // Pokud medituji druhy den, tak uz je to 2 dny v rade (vcera a dneska)
    if (this.totalUserContinuousMeditationDays > 0) {
      this.totalUserContinuousMeditationDays++;
    }
    this.lessonCompleted = true;
    if (this.lesson.type === 'VIDEO') {
      this.video.nativeElement.pause();
    } else {
      this.stopAudio();
    }
    const voice = this.lesson.source.find(it => it.src === this.selectedLessonVariantUrl).lecturer;
    await this.userProvider.saveLessonEnded(this.course, this.lesson, this.currentLessonVariantDuration, true,
      this.currentLessonVariantDuration, voice);
  }

  private async playVideo(): Promise<void> {
    const onLoaded = () => {
      this.lessonStarted = true;
      this.lessonPlaying = true;
      if (this.video.nativeElement.src !== this.videoService.currentLessonSrc) {
        this.video.nativeElement.src = this.videoService.currentLessonSrc;
        this.video.nativeElement.onended = () => {
          this.onPlaybackEnded();
        };
        this.video.nativeElement.onpause = () => {
          this.lessonPlaying = false;
        };
        this.video.nativeElement.onplay = () => {
          this.lessonPlaying = true;
        };
      }
      this.video.nativeElement.play();
      if (this.platform.is('android')) {
        try {
          this.video.nativeElement.requestFullscreen();
        } catch(e) {
          console.error(e);
        }
      }
    };
    this.slides.lockSwipes(true);
    if (this.lessonLoaded) {
      onLoaded();
    } else {
      this.lessonLoading = true;
      this.downloadProgress = undefined;
      this.preloadingSub =
        this.videoService.preload(this.selectedLessonVariantUrl)
          .subscribe({
            next: (progress: number) => {
              this.ngZone.run(() => {
                this.downloadProgress = progress ? Math.ceil(progress) : undefined;
              });
            },
            complete: () => {
              this.lessonLoading = false;
              this.lessonLoaded = true;
              onLoaded();
            },
            error: () => {
              this.lessonLoading = false;
              this.alerts.displayAlert({
                header: this.translateSvc.instant('view.playback.alert-lessonDownloadError.title'),
                message: this.translateSvc.instant('view.playback.alert-lessonDownloadError.text'),
                buttons: [this.translateSvc.instant('view.playback.alert-lessonDownloadError.button')]
              });
            },
          });
    }
  }

  private pauseVideo(): void {
    this.video.nativeElement.pause();
    this.lessonPlaying = false;
  }

  private async playAudio(): Promise<void> {
    const onLoaded = () => {
      this.lessonStarted = true;
      this.lessonPlaying = true;
      this.audioService.play(
        this.selectedLessonVariantUrl,
        () => { this.onPlaybackEnded(); },
        (err: string | Event) => { this.onPlaybackError(err); }
      );
    };
    this.slides.lockSwipes(true);
    if (this.lessonLoaded) {
      onLoaded();
    } else {
      this.lessonLoading = true;
      this.downloadProgress = undefined;
      this.preloadingSub =
        this.audioService.preload(this.selectedLessonVariantUrl)
          .subscribe({
            next: (progress: number) => {
              this.ngZone.run(() => {
                this.downloadProgress = progress ? Math.ceil(progress) : undefined;
              });
            },
            complete: () => {
              this.lessonLoading = false;
              this.lessonLoaded = true;
              onLoaded();
            },
            error: () => {
              this.lessonLoading = false;
              this.alerts.displayAlert({
                header: this.translateSvc.instant('view.playback.alert-lessonDownloadError.title'),
                message: this.translateSvc.instant('view.playback.alert-lessonDownloadError.text'),
                buttons: [this.translateSvc.instant('view.playback.alert-lessonDownloadError.button')]
              });
            },
          });
    }
  }

  private async pauseAudio(): Promise<void> {
    if (this.lessonPlaying) {
      this.lessonPlaying = false;
      this.hasError = false;
      this.audioService.pause(this.selectedLessonVariantUrl);
    }
  }

  private stopAudio(): void {
    if (this.lessonPlaying) {
      this.lessonPlaying = false;
      this.hasError = false;
      this.audioService.stop(this.selectedLessonVariantUrl);
    }
  }

  /*
    private async playLessonIntro(): Promise<void> {
      // Logika pro intro video a text, momentalne nepouzivame

      if (this.lesson.intro.video) {
       this.introVideoDisplayed = true;
        this.pageInitialized = true;
        setTimeout(() => {
          this.videoIntroElement.nativeElement.onended = () => {
            this.introVideoDisplayed = false;
            setTimeout(() => {
              this.initializeProgressBar();
            });
          };
        });
      }
      if (this.lesson.intro.text) {
        const alert = await this.alertController.create({
          header: 'Úvod k lekci',
          message: this.lesson.intro.text,
          buttons: ['OK']
        });
        await alert.present();
        if (this.lesson.intro.video) {
          await alert.onDidDismiss();
          this.videoIntroElement.nativeElement.play();
        } else {
          this.pageInitialized = true;
          setTimeout(() => {
            this.initializeProgressBar();
          });
        }
      } else if (this.lesson.intro.video) {
        setTimeout(() => {
          this.videoIntroElement.nativeElement.play();
        });
      }
    }
  */

  private getReadableTime(timeSecs: number, showSeconds: boolean): string {
    if (!timeSecs) {
      return undefined;
    }
    if (!showSeconds) {
      return '' + Math.ceil(timeSecs / 60);
    } else {
      const minutes: any = Math.floor(timeSecs / 60);
      let seconds: any = Math.floor(timeSecs % 60);
      if (seconds < 10) { seconds = '0' + seconds; }
      return `${minutes}:${seconds}`;
    }
  }

  private initializeProgressBar(): void {
    this.progressBar = new ProgressBar.Circle('#progressBar', {
      strokeWidth: 6,
      duration: 250,
      color: '#F3AB59',
      trailColor: '#e4e4e4',
      trailWidth: 2,
      svgStyle: null
    });
  }

  private navigateAway(): void {
    this.audioService.resetAll();
    const returnInfo = this.courseProvider.getPlaybackPageReturnInfo();
    if (returnInfo.direction === 'root') {
      this.navCtrl.navigateRoot(returnInfo.page, { animated: true, animationDirection: 'back' });
    } else {
      this.navCtrl.navigateBack(returnInfo.page);
    }
  }

  private getCurrentTime(): number {
    try {
      if (this.lesson.type === 'VIDEO') {
        return this.video.nativeElement.currentTime;
      } else {
        return this.audioService.getCurrentPosition(this.selectedLessonVariantUrl);
      }
    } catch (e) {
      console.log(e);
    }
    return 0;
  }
  private getCurrentLessonVariantDuration(): number {
    try {
      if (this.lesson.type === 'VIDEO') {
        return this.video.nativeElement.duration;
      } else {
        return this.audioService.getDuration(this.selectedLessonVariantUrl);
      }
    } catch (e) {
      console.log(e);
    }
    return 0;
  }

  private updateMediaCenterTextIOS(): void {
    const title = document.getElementsByTagName('title')[0];
    if (title && this.lesson) {
      title.innerText = `${this.translateSvc.instant('view.playback.label-lesson')} ${this.lesson.order}`;
    }
  }

  private async maybeShowIntroImage(): Promise<void> {
    if (this.lesson.intro && this.lesson.intro.video) {
      if (this.platform.is('cordova') === false) {
        this.introImageUrl = this.lesson.intro.video;
      } else {
        // pouzit lokalni soubor pokud ho stazeny, jinak pouzit vzdaleny a zkusit stahnout
        const localUrl = await this.fileStorage.getFileNativeUrl(this.lesson.intro.video);
        if (localUrl) {
          this.introImageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(localUrl);
        } else {
          this.introImageUrl = this.lesson.intro.video;
          this.fileStorage.download(this.lesson.intro.video);
        }
      }
    }
  }

  private onFullScreenChange = (evt: any) => {
    if ((document as any).fullscreenElement) {
      // fullscreen video on
      this.screenOrientation.unlock();
    } else {
      // fullscreen off
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY);
      this.pauseVideo();
    }
  }
}
