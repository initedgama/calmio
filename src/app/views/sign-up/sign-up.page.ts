import { Component } from '@angular/core';
import { IonInput, NavController } from '@ionic/angular';
import { FacebookAuthService } from 'src/app/service/FacebookAuthService';
import { UserProvider } from 'src/app/provider/userProvider';
import { GoogleAuthService } from 'src/app/service/GoogleAuthService';
import { AlertService } from 'src/app/service/AlertService';
import { SignUpPasswordApiRequest } from 'src/app/pojo/signUpPasswordApiRequest';
import { OAuthProviderEnum } from 'src/app/pojo/oAuthProviderEnum';
import { ExternalProviderUser } from 'src/app/pojo/externalProviderUser';
import { LoaderService } from 'src/app/service/LoaderService';
import { HelperService } from 'src/app/service/HelperService';
import { Storage } from '@ionic/storage';
import { AnalyticsEventsEnum } from 'src/app/pojo/analyticsEventsEnum';
import { AnalyticsLoginMethod } from 'src/app/pojo/analyticsLoginMethodEnum';
import { AnalyticsEvent } from 'src/app/pojo/analyticsEvent';
import { TranslateService } from '@ngx-translate/core';
import { AppleAuthService } from 'src/app/service/AppleAuthService';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-sign-up',
  templateUrl: 'sign-up.page.html',
  styleUrls: ['sign-up.page.scss']
})
export class SignUpPage {

  public inputs: {
    name: string, email: string,
    password: string, conditions: boolean
  } = {
      email: '',
      name: '',
      password: '',
      conditions: false
    };

  public typeText: string = 'password';
  public nameIcon: string = 'eye';
  public showPass: boolean = false;

  constructor(
    public platform: PlatformService,
    public navCtrl: NavController,
    private facebookAuthService: FacebookAuthService,
    private googleAuthService: GoogleAuthService,
    private appleAuthService: AppleAuthService,
    private userProvider: UserProvider,
    private alertService: AlertService,
    private helperService: HelperService,
    private loaderService: LoaderService,
    private storage: Storage,
    private translateSvc: TranslateService,
  ) { }

  public async onInputKeyDown(evt: KeyboardEvent, nextIonInput?: IonInput): Promise<void> {
    if (evt.key !== 'Enter') {
      return;
    }
    evt.preventDefault();
    console.log('onInputKeyDown', evt, nextIonInput);
    if (nextIonInput) {
      nextIonInput.getInputElement().then(input => input.focus());
    } else {
      this.onSubmitButtonClick();
    }
  }

  public async onSubmitButtonClick(): Promise<void> {
    if (!this.inputs.email || !this.inputs.name || !this.inputs.password) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-fillEveryField'), undefined, true);
      return;
    } else if (this.inputs.name.indexOf(' ') < 1 || this.inputs.name.indexOf(' ') + 1 === this.inputs.name.length) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-nameIncomplete'), undefined, true);
      return;
    } else if (!this.helperService.validateEmail(this.inputs.email)) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-enterValidEmail'), undefined, true);
      return;
    } else if (!this.inputs.conditions) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-missingAgreement'), undefined, true);
      return;
    }

    const indexSpace = this.inputs.name.indexOf(' ');

    const signUpData: SignUpPasswordApiRequest = {
      email: this.inputs.email,
      name: this.inputs.name.substring(0, indexSpace),
      surname: this.inputs.name.substring(indexSpace + 1),
      password: this.inputs.password,
      language: undefined,
    };
    this.userProvider.pendingSignUpData = signUpData as any;
    const event: AnalyticsEvent = { name: AnalyticsEventsEnum.SIGN_UP, params: { method: AnalyticsLoginMethod.EMAIL } };
    this.doSignUp(event);
  }

  public async onSignUpWithFacebookButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.facebookAuthService.signIn();
    } catch (err) {
      console.log('FB LOGIN ERROR', err);
      let errMsg = this.translateSvc.instant('view.sign-up.toast-loginFailed');
      if (err && err.errorMessage) {
        errMsg += `\n\n${err.errorMessage}`;
      }
      this.alertService.displayToast(errMsg, 4000, true);
    }

    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.FACEBOOK } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.sign-up.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
      } finally {
        await this.loaderService.dismissLoader();
      }
    }
  }

  public async onSignUpWithGoogleButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.googleAuthService.signIn();
    } catch (e) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-loginFailed'), undefined, true);
      console.log('GOOGLE LOGIN ERROR', e);
    }
    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.GOOGLE } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.sign-up.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
        return;
      } finally {
        await this.loaderService.dismissLoader();
      }

    }
  }

  public async onSignUpWithAppleButtonClick(): Promise<void> {
    let result: ExternalProviderUser;
    try {
      result = await this.appleAuthService.signIn();
    } catch (e) {
      this.alertService.displayToast(this.translateSvc.instant('view.sign-up.toast-loginFailed'), undefined, true);
      console.log('APPLE LOGIN ERROR', e);
    }
    if (result) {
      await this.loaderService.presentLoader();
      try {
        const successSignIn = await this.userProvider.signInOAuth(result);
        await this.loaderService.dismissLoader();
        if (successSignIn) {
          const event: AnalyticsEvent = { name: AnalyticsEventsEnum.LOGIN, params: { method: AnalyticsLoginMethod.APPLE } };
          this.onSignInSuccess(event);
        } else {
          throw new Error(undefined);
        }
      } catch (err) {
        let errMsg = this.translateSvc.instant('view.sign-up.toast-loginFailed');
        if (err && err.message) {
          errMsg += `\n\n${err.message}`;
        }
        this.alertService.displayToast(errMsg, 4000, true);
      } finally {
        await this.loaderService.dismissLoader();
      }
    }
  }

  public showPassword(): void {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.typeText = 'text';
      this.nameIcon = 'eye-slash';
    } else {
      this.typeText = 'password';
      this.nameIcon = 'eye';
    }
  }

  public showAgreements(): void {
    this.navCtrl.navigateForward('terms');
  }

  public async doSignUp(event: AnalyticsEvent): Promise<void> {
    await this.loaderService.presentLoader();
    const success = await this.userProvider.performPendingSignUpRequest();
    await this.loaderService.dismissLoader();
    if (success) {
      await this.storage.set(`TAC_agreedTimestamp_${this.userProvider.pendingSignUpData.email}`, Date.now());
      this.userProvider.pendingSignUpData = undefined;
      this.userProvider.onAuthComplete(true, true, event);
    }
  }

  public isSubmitEnabled(): boolean {
    return !!this.inputs.email && !!this.inputs.password && !!this.inputs.name && !!this.inputs.conditions;
  }

  private async onSignInSuccess(event: AnalyticsEvent): Promise<void> {
    this.userProvider.onAuthComplete(true, true, event);
  }

}
