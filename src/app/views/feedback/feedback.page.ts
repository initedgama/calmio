import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';
import { IonInput, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/pojo/user';
import { ApiProvider } from 'src/app/provider/apiProvider';
import { UserProvider } from 'src/app/provider/userProvider';
import { AlertService } from 'src/app/service/AlertService';
import { LoaderService } from 'src/app/service/LoaderService';
import { PlatformService } from 'src/app/service/PlatformService';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  public user: User;
  public form: FormGroup;
  public isSubmitted: boolean = false;

  constructor(
    public navCtrl: NavController,
    private userProvider: UserProvider,
    private apiProvider: ApiProvider,
    private loaderService: LoaderService,
    private appVersion: AppVersion,
    private device: Device,
    private fb: FormBuilder,
    private platform: PlatformService,
    private alertService: AlertService,
    private translateService: TranslateService,
  ) { }

  public ngOnInit(): void {
    this.user = this.userProvider.getUser();
    this.form = this.fb.group({
      contactEmail: [this.user.email, [Validators.required, Validators.email]],
      message: ['', Validators.required],
      name: [`${this.user.name ? this.user.name + ' ' : ''}${this.user.surname || ''}`],
    });
  }

  public async onInputKeyDown(evt: KeyboardEvent, nextIonInput?: IonInput): Promise<void> {
    if (evt.key !== 'Enter') {
      return;
    }
    evt.preventDefault();
    if (nextIonInput) {
      nextIonInput.getInputElement().then(input => input.focus());
    } else {
      this.onSendClick();
    }
  }

  public async onSendClick(): Promise<void> {
    this.isSubmitted = true;
    if (!this.form.valid) {
      return;
    }
    await this.loaderService.presentLoader();
    try {
      await this.apiSend(
        this.form.value,
        this.user,
        await this.getMetadata(),
      );
    } catch (err) {
      throw err;
    } finally {
      await this.loaderService.dismissLoader();
    }
    this.alertService.displayToast(this.translateService.instant('view.feedback.toast-send-success'), 3000);
    this.navCtrl.pop();
  }

  private apiSend(
    formData: FeedbackForm,
    user: User,
    metadata: FeedbackMetadata
  ): Promise<void> {
    const postData = {
      email: user.email,
      ...formData,
      metadata,
    };
    return this.apiProvider.callRequest(`feedback`, 'post', postData).toPromise();
  }

  private async getMetadata(): Promise<FeedbackMetadata> {
    if (!this.platform.is('cordova')) {
      return {
        app: {
          version: '0.0.0',
        },
        device: {
          manufacturer: 'device.manufacturer',
          model: 'device.model',
          platform: 'device.platform',
          version: 'device.version',
        },
      };
    }
    return {
      app: {
        version: this.platform.is('cordova') ? await this.appVersion.getVersionNumber() : '0.0.0',
      },
      device: {
        manufacturer: this.device.manufacturer,
        model: this.device.model,
        platform: this.device.platform.toLowerCase(),
        version: this.device.version,
      },
    };
  }

}

interface FeedbackForm {
  contactEmail: string;
  name: string;
  message: string;
}

interface FeedbackMetadata {
  app: {
    version: string
  };
  device: {
    platform: string;
    model: string;
    version: string;
    manufacturer: string;
  };
}
