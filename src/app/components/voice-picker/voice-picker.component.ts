import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Voice } from 'src/app/pojo/voice';
import { AlertService } from 'src/app/service/AlertService';
import { AudioService } from 'src/app/service/audioService';
import { RouterOutletService } from 'src/app/service/RouterOutletService';

@Component({
  selector: 'app-voice-picker',
  templateUrl: './voice-picker.component.html',
  styleUrls: ['./voice-picker.component.scss'],
})
export class VoicePickerComponent {

  public voices: Voice[];
  public selected: number;
  public activePreview: number;

  constructor(
    private modalCtrl: ModalController,
    private routerOutletService: RouterOutletService,
    private alerts: AlertService,
    private translate: TranslateService,
    private audioService: AudioService,
  ) { }

  public ionViewWillEnter(): void {
    this.routerOutletService.blur = true;
  }

  public ionViewWillLeave(): void {
    this.routerOutletService.blur = false;
    this.audioService.stopFromUrl();
  }

  public onBackdropClick(): void {
    this.modalCtrl.dismiss();
  }

  public async onPlayPreviewClick(event: Event, voice: Voice): Promise<void> {
    event.stopPropagation();

    if (this.activePreview === voice.id) {
      this.audioService.stopFromUrl();
      return;
    }
    const preview = voice.previewUrl;
    if (!preview) {
      // ukazku tohoto hlasu nemame v DB
      this.alerts.displayToast(this.translate.instant('common.error.toast-default'));
      return;
    }
    this.activePreview = voice.id;
    try {
      await this.audioService.playFromUrl(preview);
    } catch (err) {
      this.alerts.displayToast(this.translate.instant('common.error.toast-default'));
    } finally {
      this.activePreview = undefined;
    }
  }

  public onSaveClick(): void {
    this.modalCtrl.dismiss(this.selected);
  }

}
