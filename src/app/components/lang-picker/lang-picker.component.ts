import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Config } from 'src/app/config';

@Component({
    selector: 'lang-picker',
    styleUrls: ['./lang-picker.component.scss'],
    templateUrl: './lang-picker.component.html'
})
export class LangPickerComponent implements OnInit {

    @Output() public langChange: EventEmitter<string> = new EventEmitter();

    public selectedLang: string;
    public availableLangs: string[] = Config.availableLanguages;

    constructor(
        private translateSvc: TranslateService,
    ) {
    }

    public ngOnInit(): void {
        this.selectedLang = this.translateSvc.currentLang;
    }

    public async onFlagClicked(id: string): Promise<void> {
        if (this.selectedLang === id) {
            return;
        }
        this.selectedLang = id;
        this.translateSvc.use(id);
        this.langChange.emit(id);
    }
}
