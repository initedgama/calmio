import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RouterOutletService } from 'src/app/service/RouterOutletService';

@Component({
  selector: 'app-lesson-picker',
  templateUrl: './lesson-picker.component.html',
  styleUrls: ['./lesson-picker.component.scss'],
})
export class LessonPickerComponent implements OnInit {

  public lessonsTotal: number;
  public lessonsArray: number[];

  constructor(
    private modalCtrl: ModalController,
    private routerOutletService: RouterOutletService,
  ) { }

  public ngOnInit(): void {
    this.lessonsArray = Array(this.lessonsTotal).fill(0);
  }

  public ionViewWillEnter(): void {
    this.routerOutletService.blur = true;
  }

  public ionViewWillLeave(): void {
    this.routerOutletService.blur = false;
  }

  public onBackdropClick(): void {
    this.modalCtrl.dismiss();
  }

  public onSelectLesson(index: number): void {
    this.modalCtrl.dismiss(index);
  }

}
