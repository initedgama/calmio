import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SignInPage } from './views/sign-in/sign-in.page';
import { SignUpPage } from './views/sign-up/sign-up.page';
import { IntroPage } from './views/intro/intro.page';
import { TermsPage } from './views/terms/terms.page';
import { WelcomePage } from './views/welcome/welcome.page';
import { ListPage } from './views/list/list.page';
import { CourseDetailPage } from './views/course-detail/course-detail.page';
import { PlaybackPage } from './views/playback/playback.page';
import { CourseProvider } from './provider/courseProvider';
import { UserProvider } from './provider/userProvider';
import { AudioService } from './service/audioService';
import { CoursePurchasePage } from './views/course-purchase/course-purchase.page';
import { FacebookAuthService } from './service/FacebookAuthService';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GoogleAuthService } from './service/GoogleAuthService';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AlertService } from './service/AlertService';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AboutPage } from './views/about/about.page';
import { FullVersionPage } from './views/full-version/full-version.page';
import { ProfilePage } from './views/profile/profile.page';
import { ApiProvider } from './provider/apiProvider';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { InitPage } from './views/init/init.page';
import { LoaderService } from './service/LoaderService';
import { HelperService } from './service/HelperService';
import { Brightness } from '@ionic-native/brightness/ngx';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { InAppPurchaseService } from './service/InAppPurchaseService';
import { HTTP } from '@ionic-native/http/ngx';
import { AnalyticsProvider } from './provider/analyticsProvider';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ShareService } from './service/ShareService';
import { FileStorageService } from './service/FileStorageService';
import { SyncService } from './service/SyncService';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { VideoService } from './service/VideoService';
import { RouterOutletService } from './service/RouterOutletService';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { TranslateModule, TranslateLoader, } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { LangPickerComponent } from './components/lang-picker/lang-picker.component';
import { VoiceService } from './service/VoiceService';
import { OnboardingVideoPage } from './views/onboarding/onboarding-video/onboarding-video.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SentryIonicErrorHandler } from './service/SentryService';
import { VerifyEmailPage } from './views/verify-email/verify-email.page';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { Device } from '@ionic-native/device/ngx';
import { FeedbackPage } from './views/feedback/feedback.page';
import { Smartlook } from '@ionic-native/smartlook/ngx';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

import { LessonPickerComponent } from './components/lesson-picker/lesson-picker.component';
import { VoicePickerComponent } from './components/voice-picker/voice-picker.component';
import { OnboardingSlidesPage } from './views/onboarding-slides/onboarding-slides.page';
import { InAppPurchaseFunctions } from './service/InAppPurchaseFunctions';

@NgModule({
  declarations: [
    AppComponent,
    InitPage,
    SignInPage,
    SignUpPage,
    IntroPage,
    TermsPage,
    WelcomePage,
    ListPage,
    CourseDetailPage,
    PlaybackPage,
    AboutPage,
    FullVersionPage,
    ProfilePage,
    CoursePurchasePage,
    OnboardingSlidesPage,
    LangPickerComponent,
    OnboardingVideoPage,
    VerifyEmailPage,
    FeedbackPage,
    LessonPickerComponent,
    VoicePickerComponent,
  ],
  entryComponents: [
    LessonPickerComponent,
    VoicePickerComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot({ backButtonText: '' }),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'cs',
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CourseProvider,
    UserProvider,
    AudioService,
    VideoService,
    FacebookAuthService,
    Facebook,
    GoogleAuthService,
    GooglePlus,
    AlertService,
    ApiProvider,
    LoaderService,
    HelperService,
    Brightness,
    InAppPurchase,
    InAppPurchase2,
    InAppPurchaseService,
    HTTP,
    FirebaseAnalytics,
    AnalyticsProvider,
    SocialSharing,
    ShareService,
    SyncService,
    FileStorageService,
    File,
    FileTransfer,
    WebView,
    RouterOutletService,
    BackgroundMode,
    AppVersion,
    Market,
    VoiceService,
    InAppBrowser,
    StreamingMedia,
    Deeplinks,
    Device,
    Smartlook,
    FirebaseDynamicLinks,
    OneSignal,
    SignInWithApple,
    ScreenOrientation,
    Diagnostic,
    InAppPurchaseFunctions,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: SentryIonicErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}
