import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { environment } from '../environments/environment';

import { IonRouterOutlet, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterOutletService } from './service/RouterOutletService';
import { UserProvider } from './provider/userProvider';
import { Config } from './config';
import { SentryService } from './service/SentryService';
import { File } from '@ionic-native/file/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { PlatformService } from './service/PlatformService';
import { MenuService } from './service/MenuService';
import { HelperService } from './service/HelperService';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  @ViewChild(IonRouterOutlet) public routerOutlet: IonRouterOutlet;

  public Config: any = Config;

  constructor(
    public userProvider: UserProvider,
    private platform: PlatformService,
    private statusBar: StatusBar,
    private routerOutletService: RouterOutletService,
    private navCtrl: NavController,
    private sentrySvc: SentryService,
    private file: File,
    private streamingMedia: StreamingMedia,
    private menuService: MenuService,
    private helperService: HelperService,
  ) {
    this.initializeApp();
  }

  public ngAfterViewInit(): void {
    this.routerOutletService.init(this.routerOutlet);
  }

  public onProfileItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('profile');
    });
  }
  public onFullVersionItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('full-version');
    });
  }
  public onAboutItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('about');
    });
  }
  public onMeditationPositionClicekd(onGround: boolean): void {
    this.menuService.close(() => {
      let videoName = 'video_zidle.mp4';
      if (onGround) {
        videoName = 'video_zem.mp4';
      }

      if (this.platform.is('ios')) {
        const url = `${this.file.applicationDirectory}/www/assets/videos/${videoName}`;
        this.streamingMedia.playVideo(url, {
          orientation: 'portrait',
        });
      } else {
        this.userProvider.showPositionFromMenu = true;
        this.userProvider.videoUrl = `../../../../assets/videos/${videoName}`;
        this.navCtrl.navigateForward('onboarding-video');
      }
    });
  }
  public onSetupItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('profile');
    });
  }
  public onFeedbackItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('feedback');
    });
  }
  public onTermsItemClicked(): void {
    this.menuService.close(() => {
      this.navCtrl.navigateForward('terms');
    });
  }
  public onBlogItemClicked(): void {
    this.menuService.close(() => {
      this.helperService.launchNestedBrowser(
        this.userProvider.getIsFullVersionUnlocked() ? Config.premiumUserBlogUrl : Config.freeUserBlogUrl
      );
    });
  }
  public onConsultationItemClicked(): void {
    this.menuService.close(() => {
      if (this.userProvider.getIsFullVersionUnlocked()) {
        this.helperService.launchNestedBrowser(Config.consultationUrl, '#f5f7fa');
      }
    });
  }

  private initializeApp(): void {
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        this.statusBar.styleDefault();
      }
      // sentry aktivni jen v produkcnim modu
      if (environment.production) {
        this.sentrySvc.initialize();
      }
    });
  }

}
