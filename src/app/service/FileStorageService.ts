import { Injectable } from '@angular/core';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';

@Injectable()
export class FileStorageService {

    private storageDir: string = this.file.dataDirectory;

    constructor(
        private file: File,
        private webview: WebView,
        private transfer: FileTransfer,
    ) {
    }

    public async getFileNativeUrl(url: string): Promise<string> {
        console.log('check file in storage ', url);
        try {
            const dirEntry = await this.file.resolveDirectoryUrl(this.storageDir);
            const file = await this.file.getFile(dirEntry, this.getFileNameFromUrl(url), { create: false });
            const nativeURL = this.webview.convertFileSrc(file.nativeURL);
            console.log('file found in storage', nativeURL);
            return nativeURL;
        } catch (e) {
            console.log(e);
            return undefined;
        }
    }

    public download(
        url: string,
        onProgress?: (perc: number) => void,
        onComplete?: (url: string) => void,
        onError?: (e: any) => void): () => void {

        const ft = this.transfer.create();
        ft.onProgress((
            (e) => {
                if (e.lengthComputable) {
                    if (onProgress) {
                        onProgress(100 * (e.loaded / e.total));
                    }
                }
            }));
        console.log('Downloading ', url);
        ft.download(
            url,
            this.storageDir + this.getFileNameFromUrl(url),
            false
        )
            .then((fileEntry: FileEntry) => {
                console.log('Download complete:', fileEntry);
                const nativeURL = this.webview.convertFileSrc(fileEntry.nativeURL);
                if (onComplete) {
                    onComplete(nativeURL);
                }
            })
            .catch(e => {
                console.log('Download error:', e);
                if (onError) {
                    onError(e);
                }
            });
        return () => {
            console.log('Aborting download');
            ft.abort();
        };
    }

    private getFileNameFromUrl(url: string): string {
        return url.slice(url.lastIndexOf('/') + 1);
    }

}
