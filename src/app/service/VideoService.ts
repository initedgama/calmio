import { Injectable } from '@angular/core';
import { FileStorageService } from './FileStorageService';
import { Observable, Observer, from, of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { PlatformService } from './PlatformService';

@Injectable()
export class VideoService {
    public currentLessonSrc: string;

    constructor(
        private platform: PlatformService,
        private fileStorage: FileStorageService,
    ) {
    }

    public preload(url: string): Observable<number> {
        if (this.platform.is('cordova') === false) {
            this.currentLessonSrc = url;
            return of(0);
        } else {
            return from(this.fileStorage.getFileNativeUrl(url))
                .pipe(concatMap((localFileRef: string) => {
                    return new Observable((observer: Observer<number>) => {
                        const onSuccess = (ref) => {
                            this.currentLessonSrc = ref;
                            observer.complete();
                        };
                        if (localFileRef) {
                            onSuccess(localFileRef);
                        } else {
                            const abort = this.fileStorage.download(
                                url,
                                (perc: number) => {
                                    observer.next(perc);
                                },
                                (nativeURL: string) => {
                                    onSuccess(nativeURL);
                                },
                                (e) => {
                                    observer.error(e);
                                }
                            );
                            return {
                                unsubscribe(): void {
                                    abort();
                                }
                            };
                        }
                    });
                }));
        }
    }
}
