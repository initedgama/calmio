import { Injectable } from '@angular/core';
import { Role } from '../pojo/role';
import { User } from '../pojo/user';
import { Storage } from '@ionic/storage';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';
import { ApiProvider } from '../provider/apiProvider';
import { UserProvider } from '../provider/userProvider';
import { UserEvent } from '../pojo/userEventEnum';
import { Device } from '@ionic-native/device/ngx';
import { OneSignal, OSNotification } from '@ionic-native/onesignal/ngx';
import { environment } from 'src/environments/environment';
import { ONESIGNAL_APP_KEY } from '../config';
import { Md5 } from 'ts-md5/dist/md5';
import { PlatformService } from './PlatformService';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ChangeEvent, DeviceState, PermissionChange, SubscriptionChange } from 'onesignal-cordova-plugin/dist/Subscription';

declare const window: any;
@Injectable({
    providedIn: 'root',
})
export class PushNotificationService {

    private user: User;
    private token: string;
    private hasPrompted: boolean;

    constructor(
        private mOneSignal: OneSignal,
        private storage: Storage,
        private platform: PlatformService,
        private apiProvider: ApiProvider,
        private userProvider: UserProvider,
        private device: Device,
        private alertController: AlertController,
        private translateSvc: TranslateService,
        private diagnostic: Diagnostic,
    ) {
        window.mOneSignal = mOneSignal; // debug
    }


    // Funkce pro manualni zapnuti/vypnuti notifikaci ze stranky profilu uzivatele.
    // - Nastavime priznak v profilu uzivatele na strane DB
    // - Pokusime se povolit notifikace v systemu
    public async setEnabled(enable: boolean): Promise<void> {
        if (enable !== this.user.pushNotification) {
            const success = await this.setEnabledOnUser(enable);
            if (!success) {
                return;
            }
        }
        if (enable && !(await this.isSystemEnabled())) {
            await this.trySystemEnable();
        }
    }

    // Detekce, zda jsou notifikace povoleny v systemovem nastaveni.
    public isSystemEnabled(): Promise<boolean> {
        if (!this.platform.is('cordova')) {
            return new Promise<boolean>(resolve => resolve(true));
        }
        return new Promise((resolve, reject) => {
            window.plugins.OneSignal.getDeviceState((response: DeviceState) => {
                return resolve(response.hasNotificationPermission);
            });
        });

    }

    // Pokusime se povolit notifikace:
    // - bud na konci onboardingu,
    // - nebo po prihlaseni, pokud uzivatel ma notifikace povolene v profilu
    public async tryEnable(): Promise<void> {
        if (await this.isSystemEnabled()) {
            // Notifikace jsou v systemu povolene, vse OK
            return;
        }
        // Nejprve zobrazime custom alert s dotazem, zda chce uzivatel povolit notifikace.
        // Teprve pokud souhlasi, pokusime se o aktivaci systemoveho povoleni notifikaci.
        const confirmed = await this.confirmSoftPrompt();
        if (confirmed) {
            await this.trySystemEnable();
        } else {
            // Uzivatel odmitnul notifikace, upravime nastaveni profilu
            await this.setEnabledOnUser(false);
        }
    }

    public async init(): Promise<void> {
        if (!this.platform.is('cordova')) {
            return;
        }
        if (!window || !window.plugins || !window.plugins.OneSignal) {
            return;
        }
        if (!environment.production) {
            window.plugins.OneSignal.setLogLevel(6, 0);
        }
        this.hasPrompted = await this.storage.get(StorageKeysEnum.FCM_HAS_PROMPTED);

        window.plugins.OneSignal.setAppId(ONESIGNAL_APP_KEY);

        window.plugins.OneSignal.setNotificationOpenedHandler((notif: any) => {
            this.onNotification(notif);
        });

        window.plugins.OneSignal.clearOneSignalNotifications();

        window.plugins.OneSignal.addSubscriptionObserver(async (event: ChangeEvent<SubscriptionChange>) => {
            console.log('OneSignal: on subscription change: ' + JSON.stringify(event));
            this.onTokenChange(event.to.pushToken);
        });

        window.plugins.OneSignal.addPermissionObserver(async (event: ChangeEvent<PermissionChange>) => {
            console.log('OneSignal: on permission change: ' + JSON.stringify(event));
            this.hasPrompted = event.to.hasPrompted;
            this.storage.set(StorageKeysEnum.FCM_HAS_PROMPTED, this.hasPrompted);
        });

        window.plugins.OneSignal.getDeviceState((response: DeviceState) => {
            console.log('OneSignal: getDeviceState: ' + JSON.stringify(response));
            this.onTokenChange(response.pushToken);
        });

        this.userProvider.userEvents$.subscribe(async (evt) => {
            switch (evt) {
                case UserEvent.ENTER_APP_CORE:
                    this.onUserChange(this.userProvider.getUser());
                    if (this.userProvider.isActiveAuthenticationSession &&
                        !this.userProvider.isOnboardingAuthenticationSession &&
                        this.user.pushNotification) {
                        // Pokusime se povolit notifikace, pokud:
                        // 1. Uzivatel se aktivne prihlasil pres formular
                        // 2. V teto session neprosel pres onboarding (kde uz pokus probehl)
                        // 3. V profilu ma povolene notifikace
                        this.tryEnable();
                    }
                    break;
                case UserEvent.PUSH_SETTINGS_CHANGE:
                case UserEvent.SIGNED_OUT:
                    this.onUserChange(this.userProvider.getUser());
                    break;
            }
        });
    }

    private onTokenChange(token: string): void {
        console.log('PushNotificationService onTokenChange', token);
        this.token = token;
        if (!this.user || !this.user.pushNotification) {
            return;
        }
        this.maybeApiSendToken();
    }

    private onUserChange(user: User): void {
        console.log('PushNotificationService onUserChange');
        this.user = user;
        if (!this.user || !this.user.pushNotification) {
            console.log('PushNotificationService unregister');
            window.plugins.OneSignal.removeExternalUserId();
            window.plugins.OneSignal.disablePush(true);
            return;
        }
        console.log('PushNotificationService register');
        window.plugins.OneSignal.disablePush(false);
        window.plugins.OneSignal.setExternalUserId(Md5.hashStr(this.user.email) as string);
        this.updateTags(this.user);
        this.maybeApiSendToken();
    }

    private onNotification(data: OSNotification): void {
        console.log('PushNotificationService onNotification', JSON.stringify(data));
    }

    private updateTags(user: User): Record<string, string> {
        const tester = user.roles.includes(Role.TEST);
        if (tester) {
            window.plugins.OneSignal.sendTag('tester', 'true');
        } else {
            window.plugins.OneSignal.deleteTag('tester');
        }
        const tags = {
            tester: '' + tester
        };
        return tags;
    }

    private async maybeApiSendToken(): Promise<void> {
        if (!await this.shouldSendPushTokenToApi()) {
            console.log('PushNotificationService shouldSendPushTokenToApi false');
            return;
        }
        console.log('PushNotificationService shouldSendPushTokenToApi true');
        try {
            await this.apiSendPushToken(this.token);
            console.log('PushNotificationService send token to API success');
        } catch (err) {
            console.log('PushNotificationService send token to API error', err);
            return;
        }
        // success
        if (this.user && this.token) {
            await this.storage.set(StorageKeysEnum.LAST_SYNCED_FCM_TOKEN_USER_PAIR, `${this.user.email}-${this.token}`);
        }
    }

    private async shouldSendPushTokenToApi(): Promise<boolean> {
        const lastSyncedUserTokenPair = await this.storage.get(StorageKeysEnum.LAST_SYNCED_FCM_TOKEN_USER_PAIR);
        if (this.user && this.token) {
            const userTokenPair = `${this.user.email}-${this.token}`;
            return !lastSyncedUserTokenPair || lastSyncedUserTokenPair !== userTokenPair;
        }
        return false;
    }

    private async apiSendPushToken(token: string): Promise<void> {
        try {
            const postData = {
                token,
                platform: this.device.platform.toLowerCase(),
            };
            await this.apiProvider.callRequest(`user/add-push-token`, 'post', postData, true).toPromise();
        } catch (e) {
            throw e;
        }
    }

    // Pokusime se povolit notifikace v telefonu (systemovy dialog).
    // Pokud tuto moznost nemame, upozornime na to uzivatele a nabidneme presmerovani do nastaveni telefonu.
    private async trySystemEnable(): Promise<void> {
        const hasPrompted = await this.hasSystemPushPrompted();
        if (hasPrompted) {
            const confirmed = await this.confirmOpenSettings();
            if (confirmed) {
                await this.diagnostic.switchToSettings();
            }
        } else {
            await new Promise((resolve) => {
                window.plugins.OneSignal.promptForPushNotificationsWithUserResponse(true, (response: boolean) => {
                    resolve();
                });
            });
        }
    }

    private async confirmSoftPrompt(): Promise<boolean> {
        const prompt = await this.alertController.create({
            header: this.translateSvc.instant('notification.alert-enableNotifications.title'),
            message: this.translateSvc.instant('notification.alert-enableNotifications.text'),
            buttons: [
                {
                    text: this.translateSvc.instant('notification.alert-enableNotifications.button-cancel'),
                    role: 'cancel',
                },
                {
                    text: this.translateSvc.instant('notification.alert-enableNotifications.button-confirm'),
                    role: 'confirm'
                },
            ],
        });
        await prompt.present();
        const result = await prompt.onDidDismiss();
        return result && result.role === 'confirm';
    }

    private async confirmOpenSettings(): Promise<boolean> {
        const prompt = await this.alertController.create({
            header: this.translateSvc.instant('notification.alert-systemDisabled.title'),
            message: this.translateSvc.instant('notification.alert-systemDisabled.text'),
            buttons: [
                {
                    text: this.translateSvc.instant('notification.alert-systemDisabled.button-cancel'),
                    role: 'cancel',
                },
                {
                    text: this.translateSvc.instant('notification.alert-systemDisabled.button-confirm'),
                    role: 'confirm'
                },
            ],
        });
        await prompt.present();
        const result = await prompt.onDidDismiss();
        return result && result.role === 'confirm';
    }

    // Nastaveni priznaku "Notifikace zapnuty" v profilu uzivatele
    private async setEnabledOnUser(enable: boolean): Promise<boolean> {
        const success = await this.userProvider.updateUser({ pushNotification: enable });
        if (!success) {
            return false;
        }
        this.onUserChange(this.userProvider.getUser());
        return true;
    }

    // Detekce, zda jiz byl v ramci instalace zobrazen systemovy dialog pro povoleni notifikaci.
    private async hasSystemPushPrompted(): Promise<boolean> {
        return this.hasPrompted;
    }

}
