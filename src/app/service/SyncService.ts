import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../provider/apiProvider';
import { Config } from '../config';

@Injectable()
export class SyncService {

    private syncInterval: any;
    private syncInProgress: boolean;
    private queue: {
        method: string,
        data: any,
        endpoint: string,
    }[];

    constructor(
        private storage: Storage,
        private apiProvider: ApiProvider,
    ) {
    }

    public async initialize(): Promise<void> {
        this.queue = (await this.storage.get('sync-queue')) || [];
        await this.trySync();
        this.restartTimer();
    }

    public async cleanQueue(): Promise<void> {
        clearInterval(this.syncInterval);
        this.syncInterval = undefined;
        this.queue = undefined;
        await this.storage.set('sync-queue', undefined);
    }

    public async addSyncItem(item: {
        method: string,
        data: any,
        endpoint: string,
    }): Promise<void> {
        this.queue.push(item);
        await this.storage.set('sync-queue', this.queue);
        this.restartTimer();
    }

    public hasPendingItems(): boolean {
        return this.queue && this.queue.length > 0;
    }

    private async trySync(): Promise<void> {
        if (this.syncInProgress || !this.queue || !this.queue.length) {
            return;
        }
        const rq = this.queue[0];
        console.log('sync start', this.queue, rq);
        this.syncInProgress = true;
        try {
            await this.apiProvider.callRequest(rq.endpoint, rq.method, rq.data, true).toPromise();
            this.queue.shift();
            await this.storage.set('sync-queue', this.queue);
            console.log('sync rq successful', rq, this.queue);
            this.syncInProgress = false;
            await this.trySync();
        } catch (e) {
            console.log('sync rq error', rq, e);
            this.syncInProgress = false;
            if (e.status !== undefined && e.status === 0) {
                // chyba pripojeni, zkusit znovu pozdeji
            } else {
                // server vraci chybu, uz nebudeme posilat
                this.queue.shift();
                await this.storage.set('sync-queue', this.queue);
                await this.trySync();
            }
        }
    }

    private restartTimer(): void {
        console.log('sync timer restart');
        clearInterval(this.syncInterval);
        this.trySync();
        this.syncInterval = setInterval(() => {
            this.trySync();
        }, Config.syncIntervalMs);
    }
}
