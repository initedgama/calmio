import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DateTime } from 'luxon';
import { PromoCode } from '../pojo/promoCode';
import { PromoCodeType } from '../pojo/promoCodeTypeEnum';
import { UserEvent } from '../pojo/userEventEnum';
import { UserPromoCode } from '../pojo/userPromoCode';
import { ApiProvider } from '../provider/apiProvider';
import { UserProvider } from '../provider/userProvider';
import { LoaderService } from './LoaderService';

@Injectable({
    providedIn: 'root'
}) export class PromoService {

    private ready: boolean = false;
    private pendingCode: PromoCode;
    private lastRedeemTimestamp: number;

    constructor(
        private userPvd: UserProvider,
        private apiPvd: ApiProvider,
        private loaderSvc: LoaderService,
        private alertCtrl: AlertController,
        private translateSvc: TranslateService,
    ) {
    }

    public initialize(): void {
        this.userPvd.userEvents$.subscribe(evt => {
            switch (evt) {
                case UserEvent.ACTIVATE_PROMO:
                    this.ready = true;
                    if (this.pendingCode) {
                        console.log(`PromoService redeeming pending code`, this.pendingCode);
                        this.redeemCode(this.pendingCode);
                    }
                    break;
                case UserEvent.SIGNED_OUT:
                    this.ready = false;
                    break;
            }
        });
    }

    public onPromoDeeplinkOpen = (code: PromoCode) => {
        if (!this.ready) {
            console.log(`onPromoDeeplinkOpen: user not ready, saving code for later`, code);
            this.pendingCode = code;
            return;
        }
        this.redeemCode(code);
    }


    private redeemCode = async (code: PromoCode) => {
        console.log(`redeemCode initiated`, code);
        const now = Date.now();
        if (this.lastRedeemTimestamp && now - this.lastRedeemTimestamp < 3000) {
            console.log(`redeemCode called multiple times, cancelling`);
            return;
        }
        this.lastRedeemTimestamp = now;
        const user = this.userPvd.getUser();
        if (user.activeSubscriptions.length) {
            console.log(`redeem failed - user has active subscription`);
            this.alertRedeemFailActiveSubscription(code);
            return;
        }

        await this.loaderSvc.presentLoader();
        try {
            await this.apiRedeemCode(code);
            await this.userPvd.reloadUser();
        } catch (err) {
            console.log('redeemCode API error', err);
            return;
        } finally {
            await this.loaderSvc.dismissLoader();
        }
        const updatedUser = this.userPvd.getUser();
        if (updatedUser.promo) {
            this.onRedeemSuccess(updatedUser.promo);
        }
    }

    private apiRedeemCode(code: PromoCode): Promise<void> {
        const url = code.type === PromoCodeType.PARTNER ?
            'user/apply-partner-code'
            : 'user/apply-voucher-code';
        return this.apiPvd.callRequest(url, 'post', { name: code.name }).toPromise();
    }

    private onRedeemSuccess = async (code: UserPromoCode) => {
        console.log(`on RedeemSuccess`, code);
        this.userPvd.userEvents$.next(UserEvent.APPLY_PROMO_CODE_SUCCESS);
        this.alertRedeemSuccess(code);
        this.pendingCode = undefined;
    }

    private alertRedeemSuccess = async (code: UserPromoCode) => {
        const expiration = DateTime.fromMillis(code.expires);
        const expirationString = expiration.toFormat('d. L. yyyy');
        const locKeyPrefix = code.type === PromoCodeType.PARTNER
            ? 'promo.alert-redeemSuccessPartner'
            : 'promo.alert-redeemSuccessVoucher';
        const options = {
            header: this.translateSvc.instant(`${locKeyPrefix}.title`),
            message: this.translateSvc.instant(`${locKeyPrefix}.message`, {
                date: expirationString,
                partner: code.partnerName,
            }),
            buttons: [
                {
                    text: this.translateSvc.instant(`${locKeyPrefix}.button`)
                }
            ]
        };
        const alert = await this.alertCtrl.create(options);
        await alert.present();
        await alert.onDidDismiss();
    }

    private alertRedeemFailActiveSubscription = async (code: PromoCode) => {
        const locKeyPrefix = code.type === PromoCodeType.PARTNER
            ? 'promo.alert-redeemFailPartnerActiveSubscription'
            : 'promo.alert-redeemFailVoucherActiveSubscription';
        const options = {
            header: this.translateSvc.instant(`${locKeyPrefix}.title`),
            message: this.translateSvc.instant(`${locKeyPrefix}.message`),
            buttons: [
                {
                    text: this.translateSvc.instant(`${locKeyPrefix}.button`)
                }
            ]
        };
        const alert = await this.alertCtrl.create(options);
        await alert.present();
        await alert.onDidDismiss();
    }
}
