import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';
import { OneSignalTrigger } from '../pojo/OneSignalTrigger';

@Injectable({
    providedIn: 'root',
})
export class InAppTriggerService {

    private overlayCheckInterval: any;
    private dashboardVisitCounter: number = 0;

    constructor(
        private router: Router,
        private oneSignal: OneSignal,
        private platform: Platform,
    ) {
    }

    public initialize(): void {
        if (!this.platform.is('cordova')) {
            return;
        }
        this.router.events.subscribe(async evt => {
            if (evt instanceof NavigationEnd) {
                const screen = evt.url.substring(1, evt.url.indexOf(';') > -1 ? evt.url.indexOf(';') : evt.url.length);
                this.onScreenChange(screen);
            }
        });
    }

    private onScreenChange(screen: string): void {
        this.clearOverlayCheckInterval();
        // vycistim trigger s ID stranky
        this.oneSignal.removeTriggerForKey(OneSignalTrigger.SCREEN);
        // stranku neposilam do OneSignal triggeru, dokud jsou zobrazene nejake alerty - kontroluji v intervalu
        this.overlayCheckInterval = setInterval(() => {
            if (this.isOverlayElementActive()) {
                return;
            }
            // vsechny alerty jsou zavrene, nastavim triggery
            this.onScreenChangeConfirmed(screen);
        }, 1000);
    }


    private onScreenChangeConfirmed(screen: string): void {
        this.clearOverlayCheckInterval();
        // nastavim trigger s IDckem stranky
        this.oneSignal.addTrigger(OneSignalTrigger.SCREEN, screen);
        if (screen === 'list') {
            // navysim pocitadlo pristupu na dashboard
            this.dashboardVisitCounter += 1;
            this.oneSignal.addTrigger(OneSignalTrigger.DASHBOARD_VISITS, this.dashboardVisitCounter);
        }
    }

    private clearOverlayCheckInterval(): void {
        clearInterval(this.overlayCheckInterval);
        this.overlayCheckInterval = undefined;
    }

    private isOverlayElementActive(): boolean {
        const overlayElementActive = !!document.querySelector('ion-backdrop');
        return overlayElementActive;
    }
}
