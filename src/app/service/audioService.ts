import { Injectable } from '@angular/core';
import { FileStorageService } from './FileStorageService';
import { Observable, Observer, from, of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { PlatformService } from './PlatformService';

@Injectable()
export class AudioService {
    private preloadedFiles: { [url: string]: HTMLAudioElement } = {};
    private reusableAudio: HTMLAudioElement = new Audio();

    constructor(
        private platform: PlatformService,
        private fileStorage: FileStorageService,
    ) {
    }

    public playFromUrl(url: string): Promise<void> {
        this.reusableAudio.src = url;
        this.reusableAudio.load();
        const promise = new Promise<void>((resolve, reject) => {
            this.reusableAudio.onended = this.reusableAudio.onpause = () => {
                resolve(undefined);
            };
            this.reusableAudio.onerror = err => {
                reject(err);
            };
            this.reusableAudio.play();
        });
        return promise;
    }

    public stopFromUrl(): void {
        this.reusableAudio.pause();
    }

    public preload(url: string): Observable<number> {
        if (this.platform.is('cordova') === false) {
            this.preloadedFiles[url] = new Audio();
            this.preloadedFiles[url].src = url;
            this.preloadedFiles[url].currentTime = 0;
            return of(0);
        } else {
            return from(this.fileStorage.getFileNativeUrl(url))
                .pipe(concatMap((localFileRef: string) => {
                    return new Observable((observer: Observer<number>) => {
                        const onSuccess = (ref) => {
                            this.preloadedFiles[url] = new Audio();
                            this.preloadedFiles[url].src = ref;
                            this.preloadedFiles[url].currentTime = 0;
                            observer.complete();
                        };
                        if (localFileRef) {
                            onSuccess(localFileRef);
                        } else {
                            const abort = this.fileStorage.download(
                                url,
                                (perc: number) => {
                                    observer.next(perc);
                                },
                                (nativeURL: string) => {
                                    onSuccess(nativeURL);
                                },
                                (e) => {
                                    observer.error(e);
                                }
                            );
                            return {
                                unsubscribe(): void {
                                    abort();
                                }
                            };
                        }
                    });
                }));
        }
    }

    public play(url: string, onComplete: () => void, onError: (err: string | Event) => void): boolean {
        this.preloadedFiles[url].onended = () => {
            onComplete();
        };
        this.preloadedFiles[url].onerror = (err) => {
            console.log('audio onerror', err);
            onError(err);
        };
        this.preloadedFiles[url].play();
        return true;
    }

    public pause(url: string): boolean {
        if (!this.preloadedFiles[url]) {
            return false;
        }
        this.preloadedFiles[url].pause();
        return true;
    }

    public stop(url: string): boolean {
        if (!this.preloadedFiles[url]) {
            return false;
        }
        this.preloadedFiles[url].pause();
        this.preloadedFiles[url].currentTime = 0;
        return true;
    }

    public getCurrentPosition(url: string): number {
        if (!url || !this.preloadedFiles[url]) {
            return undefined;
        }
        let pos = this.preloadedFiles[url].currentTime;
        if (pos < 0) {
            pos = 0;
        }
        return pos;
    }
    public getDuration(url: string): number {
        if (!url || !this.preloadedFiles[url]) {
            return undefined;
        }
        let dur = this.preloadedFiles[url].duration;
        if (dur < 0) {
            dur = 0;
        }
        return dur;
    }

    public resetAll(): void {
        Object.keys(this.preloadedFiles).forEach(k => {
            if (this.preloadedFiles[k].currentTime) {
                this.preloadedFiles[k].currentTime = 0;
            }
        });
    }

}
