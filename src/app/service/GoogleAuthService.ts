import { Injectable } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { ExternalProviderUser } from '../pojo/externalProviderUser';
import { OAuthProviderEnum } from '../pojo/oAuthProviderEnum';

@Injectable()
export class GoogleAuthService {

    constructor(
        private google: GooglePlus,
    ) {
    }

    public async signIn(): Promise<ExternalProviderUser> {
        try {
            await this.signOut();
            const loginResult: any = await this.google.login({});
            const user: ExternalProviderUser = {
                type: OAuthProviderEnum.GOOGLE,
                oauth_id: loginResult.accessToken,
            };
            console.log('GOOGLE LOGIN RESULT', loginResult, user);
            return user;
        } catch (err) {
            console.log('GOOGLE SIGN IN ERROR', err);
            throw err;
        }
    }

    public async signOut(): Promise<void> {
        try {
            await this.google.logout();
        } catch (err) {
            console.log('GOOGLE SIGN OUT ERROR', err);
        }
        try {
            await this.google.disconnect();
        } catch (err) {
            console.log('GOOGLE DISCONNECT ERROR', err);
        }
    }
}
