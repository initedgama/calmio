import { Injectable } from '@angular/core';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import AppRate from 'cordova-plugin-apprate';
import { UserEvent } from '../pojo/userEventEnum';
import { UserProvider } from '../provider/userProvider';

@Injectable({
    providedIn: 'root'
}) export class AppRateService {

    constructor(
        private translateService: TranslateService,
        private platform: Platform,
        private userProvider: UserProvider,
        private navCtrl: NavController,
        private alertController: AlertController,
    ) {
    }

    public initialize(): void {
        if (!this.platform.is('cordova')) {
            return;
        }
        // hodnoceni vyvolavam po vstupu prihlaseneho uzivatele do appky
        this.userProvider.userEvents$.subscribe(evt => {
            if (evt === UserEvent.ENTER_APP_CORE) {
                this.configure();
                AppRate.promptForRating(false);
            }
        });
    }

    private configure(): void {
        // konfigurace pluginu s lokalizovanymi texty
        if (this.platform.is('android')) {
            // na Androidu pouzivam Ionic alerty
            AppRate.setConfirmHandler(
                async (message: string, buttonClickHandler: (index: number) => void, header: string, buttonTextArray: string[]) => {
                const alert = await this.alertController.create({
                    message,
                    header,
                    cssClass: 'rating-alert-android',
                    buttons: buttonTextArray.map((b: string, index: number) => ({
                        text: b,
                        handler: () => buttonClickHandler(index + 1)
                    })),
                    backdropDismiss: false,
                });
                alert.present();
            });
        }
        AppRate.setPreferences({
            displayAppName: 'Calmio',
            usesUntilPrompt: 6,
            promptAgainForEachNewVersion: false,
            reviewType: {
                ios: 'AppStoreReview',
                android: 'InAppBrowser'
            },
            storeAppURL: {
                ios: '1476384493',
                android: 'market://details?id=cz.calmio.app',
            },
            customLocale: {
                title: this.translateService.instant('app-rate-dialog.title'),
                message: this.translateService.instant('app-rate-dialog.message'),
                cancelButtonLabel: this.translateService.instant('app-rate-dialog.cancelButtonLabel'),
                laterButtonLabel: this.translateService.instant('app-rate-dialog.laterButtonLabel'),
                rateButtonLabel: this.translateService.instant('app-rate-dialog.rateButtonLabel'),
                yesButtonLabel: this.translateService.instant('app-rate-dialog.yesButtonLabel'),
                noButtonLabel: this.translateService.instant('app-rate-dialog.noButtonLabel'),
                appRatePromptTitle: this.translateService.instant('app-rate-dialog.appRatePromptTitle'),
                appRatePromptMessage: this.translateService.instant('app-rate-dialog.appRatePromptMessage'),
                appRatePromptYesButton: this.translateService.instant('app-rate-dialog.appRatePromptYesButton'),
                appRatePromptNoButton: this.translateService.instant('app-rate-dialog.appRatePromptNoButton'),
                feedbackPromptTitle: this.translateService.instant('app-rate-dialog.feedbackPromptTitle'),
                feedbackPromptMessage: this.translateService.instant('app-rate-dialog.feedbackPromptMessage'),
                feedbackPromptYesButton: this.translateService.instant('app-rate-dialog.feedbackPromptYesButton'),
                feedbackPromptNoButton: this.translateService.instant('app-rate-dialog.feedbackPromptNoButton'),
            } as any,
            callbacks: {
                handleNegativeFeedback: () => {
                    // pokud user chce zaslat negativni feedback, zobrazim formular
                    this.navCtrl.navigateForward('feedback');
                }
            }
        });
    }
}
