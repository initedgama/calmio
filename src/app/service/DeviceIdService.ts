import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';
import { v4 as uuidV4 } from 'uuid';

@Injectable({
    providedIn: 'root'
})
export class DeviceIdService {

    public uuid: string;

    constructor(
        private storage: Storage
    ) {
    }

    public async initialize(): Promise<string> {
        // pokud nemam, vygeneruji UUID teto instalace a ulozim
        let uuid: string = await this.storage.get(StorageKeysEnum.DEVICE_UUID);
        if (!uuid) {
            uuid = uuidV4();
            await this.storage.set(StorageKeysEnum.DEVICE_UUID, uuid);
        }
        this.uuid = uuid;
        return uuid;
    }


}
