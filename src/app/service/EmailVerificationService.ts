import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
}) export class EmailVerificationService {

    private onVerifiedNavigation: string = 'list';

    constructor() {
    }

    public setOnVerifiedNavigation(page: string): void {
        this.onVerifiedNavigation = page;
    }

    public getOnVerifiedNavigation(): string {
        return this.onVerifiedNavigation;
    }

}
