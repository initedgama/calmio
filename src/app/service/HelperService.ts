import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PlatformService } from './PlatformService';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class HelperService {

    constructor(
        private iab: InAppBrowser,
        private translateSvc: TranslateService,
        private platform: PlatformService,
    ) {
    }

    public validateEmail(email: any): boolean {
        // tslint:disable-next-line
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    public launchNestedBrowser(url: string, toolbarColor: string = '#fffce8'): void {
        console.log('HelperService.launchNestedBrowser', url);
        if (this.platform.is('ios')) {
            this.iab.create(
                url,
                '_blank',
                {
                    location: 'no',
                    zoom: 'no',
                    hideurlbar: 'yes',
                    hidenavigationbuttons: 'no',
                    navigationbuttoncolor: '#696760',
                    closebuttoncolor: '#696760',
                    closebuttoncaption: this.translateSvc.instant('common.global.back'),
                    lefttoright: 'no',
                    fullscreen: 'no',
                    toolbarcolor: toolbarColor,
                    toolbarposition: 'top',
                    toolbartranslucent: 'no'
                });
        } else {
            this.iab.create(
                url,
                '_blank',
                {
                    location: 'yes',
                    zoom: 'no',
                    hideurlbar: 'yes',
                    hidenavigationbuttons: 'no',
                    navigationbuttoncolor: '#696760',
                    closebuttoncolor: '#696760',
                    closebuttoncaption: this.translateSvc.instant('common.global.back'),
                    lefttoright: 'yes',
                    fullscreen: 'no',
                    toolbarcolor: toolbarColor,
                    toolbarposition: 'top',
                    toolbartranslucent: 'no'
                });
        }
    }

}
