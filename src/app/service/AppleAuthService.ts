import { Injectable } from '@angular/core';
import { ExternalProviderUser } from '../pojo/externalProviderUser';
import { OAuthProviderEnum } from '../pojo/oAuthProviderEnum';
import { SignInWithApple, ASAuthorizationAppleIDRequest } from '@ionic-native/sign-in-with-apple/ngx';
import jwt_decode from 'jwt-decode';
import { Storage } from '@ionic/storage';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';

@Injectable({
  providedIn: 'root',
})
export class AppleAuthService {
  constructor(
    private apple: SignInWithApple,
    private storage: Storage,
  ) { }

  public async signIn(): Promise<ExternalProviderUser> {
    try {
      const result = await this.apple.signin({
        requestedScopes: [
          ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
          ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail
        ]
      });
      console.log('apple auth result', result);
      let user: ExternalProviderUser = {
        type: OAuthProviderEnum.APPLE,
        oauth_id: result.user,
        apple_auth_details: result,
      };
      user = this.updateUserWithJWTContents(user);
      user = await this.updateUserWithStoredName(user);
      return user;
    } catch (err) {
      console.log('AppleAuthService signIn error', err);
      throw err;
    }
  }

  private updateUserWithJWTContents(user: ExternalProviderUser): ExternalProviderUser {
    const decoded: { email: string } = jwt_decode(user.apple_auth_details.identityToken);
    const result = Object.assign({}, user);
    result.apple_auth_details.email = decoded.email;
    return result;
  }

  private async updateUserWithStoredName(user: ExternalProviderUser): Promise<ExternalProviderUser> {
    const result = Object.assign({}, user);
    try {
      let storedUsers: ExternalProviderUser[] = await this.storage.get(StorageKeysEnum.APPLE_AUTH);
      if (!storedUsers) {
        storedUsers = [];
      }
      const storedUser = storedUsers.find(it => it.apple_auth_details.user === user.apple_auth_details.user);
      if (storedUser
        && storedUser.apple_auth_details.fullName.givenName
        && !user.apple_auth_details.fullName.givenName) {
        // ve storage mam jmeno uzivatele a v response neprislo, pouziju storage
        result.apple_auth_details.fullName = storedUser.apple_auth_details.fullName;
      } else if (!storedUser && user.apple_auth_details.fullName.givenName) {
        // v response mam jmeno uzivatele, ulozim do storage
        storedUsers.push(user);
        await this.storage.set(StorageKeysEnum.APPLE_AUTH, storedUsers);
      }
    } catch (err) {
      console.log('AppleAuthService updateUserWithStoredName error', err);
      // pokracuji bez upravy jmena
    }
    return result;
  }
}
