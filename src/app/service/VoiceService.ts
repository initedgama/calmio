import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CourseDetail } from '../pojo/courseDetail';
import { Lesson } from '../pojo/lesson';
import { Voice } from '../pojo/voice';
import { VoiceGender } from '../pojo/voiceGender';

@Injectable()
export class VoiceService {

    private defaultVoiceGender: VoiceGender;

    constructor(private storage: Storage) {
    }

    public setDefaultVoiceGender(voice: VoiceGender): void {
        this.storage.set('voice-default', this.defaultVoiceGender);
    }

    public async getDefaultVoiceGender(): Promise<VoiceGender> {
        const defaultVoiceGender = (await this.storage.get('voice-default')) || VoiceGender.WOMAN;
        return defaultVoiceGender;
    }

    public async setCourseVoice(courseId: string, voice: Voice): Promise<void> {
        console.log('VOICE', 'set course voice id', courseId, voice);
        await this.storage.set(`named-voice-id-course-${courseId}`, voice.id);
    }

    public async getLessonPreferredVoiceId(lesson: Lesson, courseId: string): Promise<number> {
        const availableVoices = this.getLessonAvailableVoices(lesson);
        const courseVoiceId: number = await this.storage.get(`named-voice-id-course-${courseId}`);

        if (courseVoiceId !== undefined && availableVoices.find(v => v.id === courseVoiceId)) {
            // mam ulozeny konkretni hlas pro kurz a je dostupny pro tuto lekci
            console.log('VoiceService: using voice by user course preference:', availableVoices.find(v => v.id === courseVoiceId));
            return courseVoiceId;
        }
        // nemam ulozeny vybrany hlas pro kurz nebo ulozeny hlas neni dostupny pro tuto lekci
        // zkusime vybrat hlas podle preference pohlavi
        const defaultGender = await this.getDefaultVoiceGender();
        const voiceByGenderPref = availableVoices.find(it => it.gender === defaultGender);
        let selectedVoice: Voice;
        if (voiceByGenderPref) {
            console.log('VoiceService: using voice by gender preference:', voiceByGenderPref);
            selectedVoice = voiceByGenderPref;
        } else {
            // pokud nic nenajdu, vyberu prvni v poradi
            console.log('VoiceService: no preferred voice found');
            selectedVoice = availableVoices[0];
        }
        return selectedVoice ? selectedVoice.id : undefined;
    }

    public getLessonAvailableVoices(lesson: Lesson): Voice[] {
        const sources = lesson.source;
        const availableVoices = [];
        sources
            .filter(src => !!src.lecturer)
            .forEach(src => {
                if (!availableVoices.find(v => v.id === src.lecturer.id)) {
                    availableVoices.push(src.lecturer);
                }
            });
        availableVoices.sort((a, b) => {
            if (a.name < b.name) { return -1; }
            if (a.name > b.name) { return 1; }
            return 0;
        });
        return availableVoices;
    }
}
