import { Injectable } from '@angular/core';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { PlayStoreReceipt } from '@ionic-native/in-app-purchase-2/ngx';
import { Platform } from '@ionic/angular';
import { InAppSubscriptionId } from '../pojo/inAppSubscriptionIdEnum';
import { InAppPurchasePluginProduct } from '../pojo/InAppPurchasePluginProduct';
import { Config } from '../config';
import { InAppPurchaseProduct } from '../pojo/inAppPurchaseProduct';
import { AndroidTransaction } from '../pojo/AndroidTransaction';
import { AndroidPricingPhase, AndroidSubscriptionOffer } from '../pojo/AndroidSubscriptionOffer';

declare const CdvPurchase;

@Injectable({
    providedIn: 'root'
}) export class InAppPurchaseHelperService {

    private products: InAppPurchaseProduct[];

    private PLATFORM_IOS: 'ios' = 'ios';
    private ANDROID_PURCHASE_TYPE_BY_ID: {
        [key: string]: string
    };

    constructor(
        private platform: Platform,
        private iapIos: InAppPurchase,
    ) {
        if (this.platform.is(this.PLATFORM_IOS)) {
            return;
        }

        this.platform.ready().then(async () => {
            console.log('InAppPurchaseHelperService ready');

            this.ANDROID_PURCHASE_TYPE_BY_ID = {
                [InAppSubscriptionId.MONTHLY]: CdvPurchase.ProductType.PAID_SUBSCRIPTION,
                [InAppSubscriptionId.MONTHLY_2]: CdvPurchase.ProductType.PAID_SUBSCRIPTION,
                [InAppSubscriptionId.MONTHLY_3]: CdvPurchase.ProductType.PAID_SUBSCRIPTION,
                [InAppSubscriptionId.ANNUAL]: CdvPurchase.ProductType.PAID_SUBSCRIPTION,
                [InAppSubscriptionId.LIFETIME]: CdvPurchase.ProductType.NON_CONSUMABLE,
                [InAppSubscriptionId.LIFETIME_TEST]: CdvPurchase.ProductType.NON_CONSUMABLE,
            };
            // CdvPurchase.store.verbosity = CdvPurchase.LogLevel.DEBUG;

            // autofinish vsech nakupu
            CdvPurchase.store.when()
                .approved(transaction => {
                    console.log('approved', transaction);
                    transaction.verify();
                    transaction.finish();
                });

            // refresh provadim manualne
            (window as any).store.autoRefreshIntervalMillis = 0;

            this.registerAndroidProducts();
        });
    }

    public getProducts(): Promise<InAppPurchaseProduct[]> {
        if (this.platform.is(this.PLATFORM_IOS)) {
            return this.iapIos.getProducts(Config.inAppPurchasesProductIds)
                .then((result: InAppPurchasePluginProduct[]) => this.parseProducts(result));
        } else {
            return this.getProductsAndroid()
                .then(() => this.products);
        }
    }

    public async restoreAndroidPurchases(): Promise<PlayStoreReceipt[]>  {
        console.log('InAppPurchaseHeperService restoreAndroidPurchases');
        await new Promise(resolve => setTimeout(resolve, 1000));
        const transactions: AndroidTransaction[] = CdvPurchase.store.localTransactions || [];
        console.log('InAppPurchaseHeperService restoreAndroidPurchases localTransactions', transactions);
        const result: PlayStoreReceipt[] = [];
        for (const transaction of transactions) {
            if (transaction.nativePurchase &&
                (transaction.state === 'approved' || transaction.state === 'finished' || transaction.state === 'owned')) {
                result.push({
                    id: transaction.nativePurchase.orderId,
                    purchaseToken: transaction.nativePurchase.purchaseToken,
                    type: transaction.platform as 'android-playstore',
                    signature: transaction.nativePurchase.signature,
                    purchaseState: transaction.nativePurchase.purchaseState,
                    receipt: transaction.nativePurchase.receipt
                });
            }
        }
        return result;
    }

    public getIosReceipt(): Promise<string> {
        return this.iapIos.getReceipt();
    }

    public subscribe(product: InAppPurchaseProduct): Promise<{ receipt?: string } | void> {
        if (this.platform.is(this.PLATFORM_IOS)) {
            return this.iapIos.subscribe(product.productId);
        } else {
            return this.orderAndroid(product);
        }
    }

    public buy(product: InAppPurchaseProduct): Promise<{ receipt: string } | void> {
        if (this.platform.is(this.PLATFORM_IOS)) {
            return this.iapIos.buy(product.productId);
        } else {
            return this.orderAndroid(product);
        }
    }

    private async getProductsAndroid(): Promise<void> {
        console.log('InAppPurchaseHelperService getProducts');

        // Pokud ve storu nejsou produkty, zkusim znova je zaregistrovat a inicializovat
        let registeredStoreProducts: any[] = CdvPurchase.store.products || [];
        if (!registeredStoreProducts.length) {
            console.log('InAppPurchaseHelperService getProducts nejsou produkty, zkusim znova je zaregistrovat');
            await this.registerAndroidProducts();
            registeredStoreProducts = CdvPurchase.store.products || [];
        }

        const products: InAppPurchasePluginProduct[] = registeredStoreProducts.map(product => {
            const pricing = product.pricing;
            return {
                productId: product.id,
                priceAsDecimal: pricing.priceMicros / 1000000,
                price: pricing.price,
                currency: pricing.currency,
                getOffer: product.getOffer,
                ...product,
            };
        });
        this.products = this.parseProducts(products);
    }

    private orderAndroid(product: InAppPurchaseProduct): Promise<void> {
        const productId: string = product.productId;
        console.log('InAppPurchaseHelperService orderAndroid', product);
        return new Promise((resolve, reject) => {
            const offer = product.getOffer();
            console.log(offer);
            CdvPurchase.store.order(offer).then(resp => {
                // {isError: true, code: 6777003, message: "ITEM_ALREADY_OWNED"}
                // {isError: true, code: 6777006, message: "USER_CANCELED"}
                console.log(resp);
                if (resp && resp.isError) {
                    if (resp.message === 'ITEM_ALREADY_OWNED' || resp.code === 6777003) {
                        console.log('InAppPurchaseHelperService orderAndroid product valid');
                        reject(new Error('cancelled by user or payment failed'));
                        return;
                    }
                    if (resp.message === 'ITEM_ALREADY_OWNED' || resp.code === 6777003) {
                        console.log('InAppPurchaseHelperService orderAndroid product owned', resp);
                    }
                }
                resolve();
            });
        });
    }

    private parseProducts(pluginProducts: InAppPurchasePluginProduct[] | any): InAppPurchaseProduct[] {
        console.log('Parse products');
        const products: InAppPurchaseProduct[] = pluginProducts
            .map(product => {
                let price: string = product.price.replace(',00', '');
                let priceAsDecimal: number = product.priceAsDecimal;

                if (!this.platform.is(this.PLATFORM_IOS)) {
                    const firstOffer: AndroidSubscriptionOffer = product.getOffer();
                    // Prvni nabidka, mela by byt jen jedna.
                    if (firstOffer && firstOffer.pricingPhases && firstOffer.pricingPhases.length) {
                        // Vezmeme posledni fazi, je tam konecna cena po skonceni vsech fazi.
                        const lastPricingPhase: AndroidPricingPhase = firstOffer.pricingPhases[firstOffer.pricingPhases.length - 1];
                        price = lastPricingPhase.price.replace(',00', '');
                        priceAsDecimal = lastPricingPhase.priceMicros / 1000000;
                    }
                }

                return {
                    ...product,
                    priceAsDecimal,
                    productId: product.productId as InAppSubscriptionId,
                    title: product.title.replace('(Calmio)', ''),
                    price,
                    currency: product.currency === 'CZK' ? 'Kč' : product.currency,
                    type: Config.inAppPurchaseTypes[product.productId],
                    purchaseable: this.isPurchasable(product),
                };
            });
        console.log('InAppPurchaseHelperService parseProducts result', products);
        return products;
    }

    private isPurchasable(product: InAppPurchasePluginProduct): boolean {
        return Config.inAppPurchasesActiveProductIds.includes(product.productId);
    }

    private async registerAndroidProducts(): Promise<void> {
        console.log('InAppPurchaseHelperService registering products');
        const registrationInput = Config.inAppPurchasesProductIds.map(id => ({
            id,
            type: this.ANDROID_PURCHASE_TYPE_BY_ID[id] || CdvPurchase.ProductType.PAID_SUBSCRIPTION,
            platform: CdvPurchase.Platform.GOOGLE_PLAY
        }));
        console.log('register', registrationInput);
        CdvPurchase.store.register(registrationInput);

        await CdvPurchase.store.initialize([CdvPurchase.Platform.GOOGLE_PLAY]);
    }
}
