import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BackButtonEmitter } from '@ionic/angular/dist/providers/platform';
import { Subject } from 'rxjs';

declare var window;
@Injectable({
    providedIn: 'root'
}) export class PlatformService {

    constructor(
        private platform: Platform,
    ) {
    }

    public ready: any = this.platform.ready.bind(this.platform);
    public backButton: BackButtonEmitter = this.platform.backButton;
    public resume: Subject<void> = this.platform.resume;
    public is: any = this.platform.is.bind(this.platform);
}
