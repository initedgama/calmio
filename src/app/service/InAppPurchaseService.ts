
import { Injectable } from '@angular/core';
import { Config } from '../config';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';
import { InAppPurchaseProduct } from '../pojo/inAppPurchaseProduct';
import { StorageKeysEnum } from '../pojo/storageKeysEnum';
import { InAppSubscriptionId } from '../pojo/inAppSubscriptionIdEnum';
import { InAppPurchaseSubscription } from '../pojo/inAppPurchaseSubscription';
import { InAppSubscriptionType } from '../pojo/inAppSubscriptionTypeEnum';
import { SentryService } from './SentryService';
import { PlatformService } from './PlatformService';
import { ApiProvider } from '../provider/apiProvider';
import { InAppPurchaseHelperService } from './InAppPurchaseHelperService';
import { AlertService } from './AlertService';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class InAppPurchaseService {

    private products: InAppPurchaseProduct[];

    constructor(
        private iap: InAppPurchaseHelperService,
        private storage: Storage,
        private platform: PlatformService,
        private nativeHTTP: HTTP,
        private sentryService: SentryService,
        private apiProvider: ApiProvider,
        private alertService: AlertService,
        private translateSvc: TranslateService,
    ) {
    }

    public async subscribe(product: InAppPurchaseProduct): Promise<boolean> {
        const productId: InAppSubscriptionId = product.productId;
        console.log('Subscribe to product: ' + productId);
        try {
            const subscriptionType = Config.inAppPurchaseTypes[productId];
            let subscriptionResult: { receipt?: string } | void;
            if (subscriptionType === InAppSubscriptionType.LIFETIME) {
                subscriptionResult = await this.iap.buy(product);
            } else {
                subscriptionResult = await this.iap.subscribe(product);
            }
            console.log('platby - subscriptionResult', subscriptionResult);
            if (this.platform.is('ios')) {
                // na iOS result obsahuje receipt, preposleme dale at neni potreba znovu autorizovat getReceipt request heslem
                await this.restorePurchasesIOS(subscriptionResult && subscriptionResult.receipt);
            } else {
                await this.restorePurchasesAndroid(false);
            }
            return true;
        } catch (e) {
            console.log('IN APP PURCHASE SUBSCRIBE ERROR', e);
            try {
                console.log(JSON.stringify(e));
            } catch (err) { }
            if ((this.platform.is('ios') && e && e.errorCode === 2)
                || (this.platform.is('android') && e && e.message && e.message.includes('cancelled by user'))) {
                // nereportovat do Sentry, pokud proces nakupu zrusil uzivatel
                return false;
            }
            this.sentryService.captureException(e);
            return false;
        }
    }

    public async getProducts(): Promise<InAppPurchaseProduct[]> {
        this.products = await this.initializeProducts();
        return this.products;
    }

    public restorePurchases(): Promise<boolean> {
        if (!this.platform.is('cordova')) {
            return Promise.resolve(true);
        }
        if (this.platform.is('ios')) {
            return this.restorePurchasesIOS();
        } else {
            return this.restorePurchasesAndroid(false);
        }
    }

    public async restoreSubscriptionsFromStorage(): Promise<InAppPurchaseSubscription[]> {
        const subs = await this.storage.get(StorageKeysEnum.ACTIVE_SUBSCRIPTIONS);
        console.log('restoreSubscriptionsFromStorage: ', subs);
        return subs || [];
    }

    public persistSubscriptionsToStorage(subs: InAppPurchaseSubscription[]): Promise<any> {
        console.log('persistSubscriptionsToStorage: ', subs);
        return this.storage.set(StorageKeysEnum.ACTIVE_SUBSCRIPTIONS, subs);
    }

    // overovani platby iOS:
    // pomoci pluginu ziska receipt uzivatele a ten pak overuje na Apple backendu
    // informaci o expiraci predplatneho ulozi do storage
    public async restorePurchasesIOS(receipt?: string): Promise<boolean> {
        console.log('restorePurchasesIOS, receipt', receipt);
        if (!this.platform.is('cordova')) {
            return true;
        }
        try {
            if (!receipt) {
                receipt = await this.iap.getIosReceipt();
                console.log('getReceipt result', receipt);
            }
            await this.storage.set(StorageKeysEnum.IOS_IAP_LAST_RECEIPT, receipt);
            await this.serverSyncPurchases(false);

            try {
                await this.purchaseCheckFallbackIOS(receipt);
            } catch (e) {
                console.log('IN APP PURCHASE purchaseCheckFallbackIOS ERROR', e);
            }

            return true;
        } catch (e) {
            console.log('IN APP PURCHASE RESTORE ERROR', e);
            return false;
        }
    }


    // overovani platby Android:
    // pomoci pluginu zjistuje zda je predplatne aktivni a informaci ulozi do storage
    public async restorePurchasesAndroid(lazySync: boolean): Promise<boolean> {
        if (!this.platform.is('cordova')) {
            return true;
        }
        try {
            const result: { receipt: string }[] = await this.iap.restoreAndroidPurchases();
            console.log('platby - restorePurchasesAndroid result', result);
            // v response na restorePurchases vyhledame aktivni produkty
            const activeSubscriptions: InAppPurchaseSubscription[] = [];
            result.forEach(purchase => {
                try {
                    const receipt = JSON.parse(purchase.receipt);
                    // todo tento include se musi dat pryc, kdyz nekdo bude mit stare predplatne, tak se musi logovat na server
                    if (Config.inAppPurchasesProductIds.includes(receipt.productId) && receipt.purchaseState === 0) {
                        const sub: InAppPurchaseSubscription = {
                            id: receipt.productId,
                            type: Config.inAppPurchaseTypes[receipt.productId],
                            // na Androidu nezname expiraci produktu, aktivni produkty se kontroluji pri kazdem spusteni aplikace
                            expirationTimestamp: Number.MAX_VALUE,
                            receipt,
                        };
                        activeSubscriptions.push(sub);
                    }
                } catch (e) {
                    console.log(e);
                }
            });
            console.log('platby - restorePurchasesAndroid activeSubscriptions', activeSubscriptions);
            await this.persistSubscriptionsToStorage(activeSubscriptions);

            await this.serverSyncPurchases(lazySync);

            return true;
        } catch (e) {
            console.log('IN APP PURCHASE RESTORE ERROR', e);
            // tato chyba nastane i v offline rezimu, pokracujeme s posledni ulozenou hodnotou
            return true;
        }
    }

    // Odeslani receiptu a purchaseTokenu ulozenych lokalne na BE
    public async serverSyncPurchases(lazy: boolean): Promise<void> {
        let body: any;
        if (this.platform.is('ios')) {
            const receipt = await this.storage.get(StorageKeysEnum.IOS_IAP_LAST_RECEIPT);
            const subscriptions = await this.storage.get(StorageKeysEnum.ACTIVE_SUBSCRIPTIONS);
            if (receipt) {
                body = {
                    platform: 'ios',
                    receipts: [{
                        receipt
                    }],
                    subscriptions,
                    lazy,
                };
            }
        } else if (this.platform.is('android')) {
            const subscriptions: {
                id: InAppSubscriptionId;
                type: InAppSubscriptionType;
                expirationTimestamp: number;
                receipt: any;
            }[] = await this.storage.get(StorageKeysEnum.ACTIVE_SUBSCRIPTIONS);
            if (subscriptions && subscriptions.length) {
                body = {
                    platform: 'android',
                    receipts: subscriptions.map(sub => ({
                        productId: sub && sub.receipt && sub.receipt.productId,
                        receipt: sub && sub.receipt && sub.receipt.purchaseToken,
                    })),
                    subscriptions,
                    lazy,
                };
            }
        }
        if (!body) {
            return;
        }
        try {
            console.log('volam purchase-subscription-verification, body =', body);
            await this.apiProvider.callRequest(`purchase-subscription-verification`, 'post', body, true).toPromise();
        } catch (err) {
            console.log('InAppPurchaseService.serverSyncPurchases error', err);
        }
    }

    private async initializeProducts(): Promise<InAppPurchaseProduct[]> {

        try {
            let products: InAppPurchaseProduct[];
            if (this.platform.is('cordova') === false) {
                // for desktop testing
                products = [{
                    productId: 'subscription1', title: 'Měsíční',
                    purchaseable: false,
                    type: InAppSubscriptionType.MONTHLY,
                    description: 'Měsíční předplatné plné verze aplikace Calmio.', price: '189 Kč', currency: 'Kč', priceAsDecimal: 189
                },
                {
                    productId: 'medium2_renewable_test', title: 'Měsíční 2',
                    purchaseable: false,
                    type: InAppSubscriptionType.MONTHLY,
                    description: 'Měsíční předplatné plné verze aplikace Calmio.', price: '189 Kč', currency: 'Kč', priceAsDecimal: 189
                },
                {
                    productId: 'medium2_renewable_test', title: 'Roční',
                    purchaseable: true,
                    type: InAppSubscriptionType.ANNUAL,
                    description: 'Roční předplatné plné verze aplikace Calmio.', price: '1495 Kč', currency: 'Kč', priceAsDecimal: 1495
                },
                {
                    productId: 'lifetime_full_version', title: 'Lifetime',
                    purchaseable: true,
                    type: InAppSubscriptionType.LIFETIME,
                    description: 'Doživotní plná verze aplikace Calmio.', price: '8999 Kč', currency: 'Kč', priceAsDecimal: 8999
                }] as any;
            } else {
                products = await this.iap.getProducts();
                console.log('Registered products: ', products);
            }

            if (products && products.length) {
                // seradit produkty podle poradi v poli Config.inAppPurchasesProductIds
                products.sort((a, b) => {
                    return Config.inAppPurchasesProductIds.indexOf(a.productId) - Config.inAppPurchasesProductIds.indexOf(b.productId);
                });
                return products;
            } else {
                this.alertService.displayToast(this.translateSvc.instant('view.full-version.label-unableToLoadSubscriptionInfo'), 7000);
                throw new Error('Available products not found in iap.getProducts response.');
            }
        } catch (e) {
            console.log('IN APP PURCHASE GET PRODUCTS ERROR', e);
            throw e;
        }
    }

    private async purchaseCheckFallbackIOS(receipt: string): Promise<void> {
        const postData = {
            password: Config.inAppPurchaseSharedSecretIOS,
            'receipt-data': receipt,
        };
        const options = {
            method: 'post',
            data: postData,
            serializer: 'json',
            responseType: 'text',
        } as any;

        let postResponseData: {
            latest_receipt_info: {
                product_id: string, purchase_date_ms: number, expires_date_ms: number, is_upgraded: string
            }[]
        };

        // pouzivame nativni HTTP abychom se vyhnuli CORS ochrane prohlizece
        let postResponse = await this.nativeHTTP.sendRequest(Config.inAppPurchaseVerificationUrlIOS, options);
        if (postResponse.status !== 200) {
            throw new Error(postResponse.error);
        }
        postResponseData = JSON.parse(decodeURIComponent(postResponse.data));

        if (!postResponseData.latest_receipt_info) {
            console.log('error verifying iOS receipt for production', postResponse);
            console.log('retrying in sandbox...');
            // retry verfication with sandbox URL
            postResponse = await this.nativeHTTP.sendRequest(Config.inAppPurchaseVerificationUrlIOS_sandbox, options);
            if (postResponse.status !== 200) {
                throw new Error(postResponse.error);
            }
            postResponseData = JSON.parse(decodeURIComponent(postResponse.data));
        }

        console.log('in app purchase restore iOS data', postResponseData);

        // vyhledame objekty aktivniho predplatneho
        const activePurchases = postResponseData.latest_receipt_info
            .filter(item => {
                const subscriptionType = Config.inAppPurchaseTypes[item.product_id];
                if (subscriptionType !== InAppSubscriptionType.LIFETIME) {
                    // u periodickych predplatnych odfiltrujeme expirovane
                    if (+item.expires_date_ms < Date.now()) {
                        return false;
                    }
                    // u periodickych predplatnych odfiltrujeme upgradovane
                    if (item.is_upgraded === 'true') {
                        return false;
                    }
                }
                return true;
            });
        const subscriptions: InAppPurchaseSubscription[] = activePurchases.map(it => {
            const subscriptionType = Config.inAppPurchaseTypes[it.product_id];
            const expirationTimestamp = subscriptionType === InAppSubscriptionType.LIFETIME
                ? Number.MAX_VALUE // dozivotni predplatne neexpiruje
                : +it.expires_date_ms;
            return {
                id: it.product_id as InAppSubscriptionId,
                type: subscriptionType,
                expirationTimestamp,
                receipt: it,
            };
        });
        console.log('in app purchase active subscriptions', subscriptions);
        await this.persistSubscriptionsToStorage(subscriptions);
    }

}
