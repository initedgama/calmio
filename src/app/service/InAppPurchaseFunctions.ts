import { InAppSubscriptionId } from '../pojo/inAppSubscriptionIdEnum';
import { DateTime } from 'luxon';
import { InAppPurchaseProduct } from '../pojo/inAppPurchaseProduct';
import { InAppSubscriptionType } from '../pojo/inAppSubscriptionTypeEnum';
import { Injectable } from '@angular/core';
import { InAppPurchaseService } from './InAppPurchaseService';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LoaderService } from './LoaderService';
import { AlertService } from './AlertService';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { UserProvider } from '../provider/userProvider';
import { TranslateService } from '@ngx-translate/core';
import { File } from '@ionic-native/file/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';

@Injectable({
    providedIn: 'root'
})
export class InAppPurchaseFunctions {

    // public products: InAppPurchaseProduct[];

    constructor(private inAppPurchaseService: InAppPurchaseService,
                private userProvider: UserProvider,
                private iab: InAppBrowser,
                private platform: Platform,
                private translateSvc: TranslateService,
                private loading: LoaderService,
                private alertService: AlertService,
                private file: File,
                private streamingMedia: StreamingMedia,
                public navCtrl: NavController,
                public alertController: AlertController,
    ) {
    }

    public onRetryGetProductButtonClick(): void {
        this.loadProducts();
    }

    public onTermsClick(): void {
        this.navCtrl.navigateForward('terms');
    }

    public isProductActive(id: InAppSubscriptionId): boolean {
        return !!this.userProvider.getUser().activeSubscriptions.find(it => it.id === id);
    }

    public getExpirationDateLabel(id: InAppSubscriptionId): string {
        const sub = this.userProvider.getUser().activeSubscriptions.find(it => it.id === id);
        if (sub) {
            return DateTime.fromMillis(sub.expirationTimestamp).toFormat('d. L. yyyy');
        }
        return '';
    }

    public isCancellableProductActive(): boolean {
        return !!this.userProvider.getUser().activeSubscriptions.find(
            it => it.type === InAppSubscriptionType.ANNUAL
                || it.type === InAppSubscriptionType.MONTHLY
        );
    }

    public onProductClick(product: InAppPurchaseProduct, selectedItem: InAppPurchaseProduct): InAppPurchaseProduct {
        if (this.isProductActive(product.productId)) {
            return;
        }
        if (selectedItem === product) {
            selectedItem = undefined;
        } else {
            selectedItem = product;
        }
        return selectedItem;
    }

    // pokud prijdou v seznamu produktu z Apple dva mesicni produkty, tak schovam ten ktery clovek nema koupeny
    // pokud nema koupeny zadny, schovam product ID "subscription_monthly"
    public getShouldHideProduct(product: InAppPurchaseProduct, products: InAppPurchaseProduct[]): boolean {
        let result = false;
        if (!product.purchaseable && !this.userProvider.getUser().activeSubscriptions.find(p => p.id === product.productId)) {
            return true;
        }
        if (product.type === InAppSubscriptionType.MONTHLY) {
            const availableMonthlyProducts = products
                .filter(prod => prod.purchaseable)
                .filter(prod => prod.type === InAppSubscriptionType.MONTHLY);
            const boughtMonthlyProducts = availableMonthlyProducts
                .filter(prod => !!this.userProvider.getUser().activeSubscriptions.find(p => p.id === prod.productId));
            if (availableMonthlyProducts.length > 1) {
                if (boughtMonthlyProducts.length === 1) {
                    result = !boughtMonthlyProducts.find(prod => prod.productId === product.productId);
                } else if (boughtMonthlyProducts.length === 0) {
                    // v nabidce mesicnich produktu zobrazim vzdy ten, ktery ma clovek koupeny
                    // pokud nema zadny, zobrazim pouze ten nejnovejsi,  coz je ten
                    // ktery je uveden jako posledni v availableMonthlyProducts - serazeno podle pole Config.inAppPurchasesProductIds
                    result = availableMonthlyProducts
                        .findIndex(prod => prod.productId === product.productId) !== availableMonthlyProducts.length - 1;
                }
            }
        }
        return result;
    }

    public async onPurchaseSelectedClick(selectedItem: InAppPurchaseProduct, showVideo: boolean): Promise<InAppPurchaseProduct> {
        console.log('on Purchase Selected Click', selectedItem);

        const product = selectedItem;

        // android - upozorneni na zmenu predplatneho, pokud jiz ma periodicke predplatne aktivni
        if (this.platform.is('android') && this.isCancellableProductActive()) {
            // pokud ma uzivatel aktivni predplatne a chce si koupit jine,
            // upozornime ho ze si musi predchozi predplatne manualne zrusit
            const userConfirmed = await this.confirmSubscriptionChange();
            if (!userConfirmed) {
                return selectedItem;
            }
        }

        // iOS - upozorneni na zmenu predplatneho, pokud ma aktivni periodicke predplatne a chce prejit na lifetime
        // upgrade/downgrade periodickeho predplatneho funguje v Apple automaticky (stejna subscription group)
        if (this.platform.is('ios') && this.isCancellableProductActive()
            && product.type === InAppSubscriptionType.LIFETIME) {
            const userConfirmed = await this.confirmSubscriptionChange();
            if (!userConfirmed) {
                return selectedItem;
            }
        }

        await this.loading.presentLoader();
        const success = await this.inAppPurchaseService.subscribe(product);
        if (success) {
            await this.userProvider.reloadUser();
            await this.loading.dismissLoader();
            if (!!this.userProvider.getUser().activeSubscriptions.find(sub => sub.id === product.productId)) {
                this.alertService.displayToast(this.translateSvc.instant('view.full-version.toast-fullVersionPurchased'));
            }
        } else {
            await this.loading.dismissLoader();
            this.alertService.displayToast(this.translateSvc.instant('view.full-version.purchase-error'));
        }
        return undefined;
    }

    public async onVideoShow(): Promise<void> {
        const videoName = 'consultation.mp4';
        console.log('Open video ' + videoName);
        if (this.platform.is('ios')) {
            const url = `${this.file.applicationDirectory}/www/assets/videos/${videoName}`;
            this.streamingMedia.playVideo(url, {
                orientation: 'portrait',
            });
        } else {
            this.userProvider.showPositionFromMenu = true;
            this.userProvider.videoUrl = `../../../../assets/videos/${videoName}`;
            this.navCtrl.navigateForward('onboarding-video');
        }
    }

    public async onRefreshPurchasesButtonClick(): Promise<void> {
        await this.loading.presentLoader();
        const success = await this.inAppPurchaseService.restorePurchases();
        if (success) {
            await this.userProvider.reloadUser();
            await this.loading.dismissLoader();
            console.log(this.userProvider.getUser().activeSubscriptions);
            if (this.userProvider.getUser().activeSubscriptions.length) {
                this.alertService.displayToast(this.translateSvc.instant('view.full-version.toast-fullVersionPurchased'));
            } else {
                this.alertService.displayAlert(
                    {
                        header: this.translateSvc.instant('view.full-version.alert-refreshPurchasesFailed.title'),
                        message: this.translateSvc.instant('view.full-version.alert-refreshPurchasesFailed.text'),
                        buttons: [
                            {
                                text: this.translateSvc.instant('view.full-version.alert-refreshPurchasesFailed.button-back'),
                            },
                            {
                                text: this.translateSvc.instant('view.full-version.alert-refreshPurchasesFailed.button-retry'),
                                handler: () => {
                                    this.onRefreshPurchasesButtonClick();
                                }
                            }
                        ]
                    });
            }
        } else {
            await this.loading.dismissLoader();
            this.alertService.displayToast(this.translateSvc.instant(this.translateSvc.instant('common.error.toast-default')));
        }
    }

    public async onCancelSubscruptionButtonClick(): Promise<void> {
        const message = this.platform.is('ios') ?
            this.translateSvc.instant('view.full-version.alert-cancelSubscription.text-ios')
            : this.translateSvc.instant('view.full-version.alert-cancelSubscription.text-android');
        const alert = await this.alertController.create({
            header: this.translateSvc.instant('view.full-version.alert-cancelSubscription.title'),
            message,
            buttons: [
                {
                    text: this.translateSvc.instant('view.full-version.alert-cancelSubscription.button-cancel'),
                    role: 'cancel',
                }, {
                    text: this.translateSvc.instant('view.full-version.alert-cancelSubscription.button-confirm'),
                    role: 'confirm',
                }
            ]
        });
        await alert.present();
        const result = await alert.onDidDismiss();
        if (result.role === 'confirm') {
            this.iab.create(this.platform.is('ios')
                    ? this.translateSvc.instant('view.full-version.link-ios-cancelSubscriptionGuide')
                    : this.translateSvc.instant('view.full-version.link-android-cancelSubscriptionGuide'),
                '_system');
        }
    }

    public async confirmSubscriptionChange(): Promise<boolean> {
        const alert = await this.alertController.create({
            header: this.translateSvc.instant('view.full-version.alert-duplicateSubscription.title'),
            message: this.translateSvc.instant('view.full-version.alert-duplicateSubscription.text'),
            buttons: [
                {
                    text: this.translateSvc.instant('view.full-version.alert-duplicateSubscription.button-cancel'),
                    role: 'cancel',
                },
                {
                    text: this.translateSvc.instant('view.full-version.alert-duplicateSubscription.button-cancelSubcriptionGuide'),
                    role: 'show-guide',
                },
                {
                    text: this.translateSvc.instant('view.full-version.alert-duplicateSubscription.button-continue'),
                    role: 'confirm',
                }
            ]
        });
        await alert.present();
        const result = await alert.onDidDismiss();
        if (result.role === 'confirm') {
            return true;
        }
        if (result.role === 'show-guide') {
            this.iab.create(this.platform.is('ios')
                    ? this.translateSvc.instant('view.full-version.link-ios-cancelSubscriptionGuide')
                    : this.translateSvc.instant('view.full-version.link-android-cancelSubscriptionGuide'),
                '_system');
        }
        return false;
    }

    public getAnnualPriceForMonthlySub(products: InAppPurchaseProduct[]): string {
        try {
            const monthlyProducts = products
                .filter(prod => prod.purchaseable)
                .filter(prod => prod.type === InAppSubscriptionType.MONTHLY);
            const latestMonthlyProduct = monthlyProducts[monthlyProducts.length - 1];
            return Math.ceil(latestMonthlyProduct.priceAsDecimal * 12) + ' ' + latestMonthlyProduct.currency;
        } catch (err) {
            return '';
        }
    }

    public getMonthlyPriceForAnnualSub(products: InAppPurchaseProduct[]): string {
        try {
            const annual = products
                .filter(prod => prod.purchaseable)
                .find(prod => prod.type === InAppSubscriptionType.ANNUAL);
            return Math.ceil(annual.priceAsDecimal / 12) + ' ' + annual.currency + ' / ' +
                this.translateSvc.instant('view.full-version.label-monthly');
        } catch (err) {
            return '';
        }
    }

    public getAnnualPriceDiscount(products: InAppPurchaseProduct[]): string {
        try {
            const monthlyProducts = products
                .filter(prod => prod.purchaseable)
                .filter(prod => prod.type === InAppSubscriptionType.MONTHLY);
            const latestMonthlyProduct = monthlyProducts[monthlyProducts.length - 1];
            const annual = products
                .filter(prod => prod.purchaseable)
                .find(prod => prod.type === InAppSubscriptionType.ANNUAL);
            const annualPriceForMonthly = latestMonthlyProduct.priceAsDecimal * 12;
            const annualPrice = annual.priceAsDecimal;
            const discount = (annualPriceForMonthly - annualPrice) / (annualPriceForMonthly / 100);
            return '-' + Math.ceil(discount) + '%';
        } catch (err) {
            return '';
        }
    }

    public async loadProducts(): Promise<InAppPurchaseProduct[]> {
        try {
            return await this.inAppPurchaseService.getProducts();
        } catch (e) {
            // retry - uzivatel muze pouzit tlacitko
        }
    }

}
