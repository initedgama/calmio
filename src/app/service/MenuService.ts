import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AlertService } from './AlertService';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private isAnimating: boolean = false;

    constructor(
        private menuController: MenuController,
        private alertService: AlertService,
    ) {
    }

    public async close(callback?: () => void): Promise<void> {
        if (this.isAnimating) {
            return;
        }
        this.isAnimating = true;
        if (callback) {
            callback();
        }
        try {
            await this.menuController.close();
        } catch (err) {
            console.log(err);
        }
        this.isAnimating = false;
    }
    public async open(callback?: () => void): Promise<void> {
        if (this.isAnimating) {
            return;
        }
        this.isAnimating = true;
        if (callback) {
            callback();
        }
        try {
            await this.menuController.open();
        } catch (err) {
            console.log(err);
        }
        this.isAnimating = false;
    }
}
