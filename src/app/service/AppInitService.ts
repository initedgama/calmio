import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
}) export class AppInitService {

    public ready: boolean = false;

    constructor(
        private navCtrl: NavController,
    ) {
    }

    public canActivate(): boolean {
        if (!this.ready) {
            this.navCtrl.navigateRoot('init', {
                animated: false
            });
            return false;
        }
        return true;
    }

}
