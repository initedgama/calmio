import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AlertService {

  constructor(
    private toasts: ToastController,
    private alerts: AlertController,
    private translate: TranslateService,
  ) { }

  public async displayAlert(options: AlertOptions): Promise<void> {
    const alert = await this.alerts.create(options);
    return alert.present();
  }

  public async displayErrorAlert(message: string): Promise<void> {
    return this.displayAlert({
      header: this.translate.instant('common.error.alert-default.title'),
      message,
      buttons: [{
        text: this.translate.instant('common.error.alert-default.button-ok'),
      }]
    });
  }

  public async displayToast(message: string, duration: number = 2000, warning: boolean = false): Promise<void> {
    const msg = warning ? '<img src="assets/imgs/exclamation-triangle.png"/>' + message : message;
    const toast = await this.toasts.create({ message: msg, duration, color: 'primary', cssClass: warning ? 'toast-warning' : undefined });
    if (warning) {
      const style = document.createElement('style');
        // tslint:disable-next-line
      style.innerHTML = 'img{width: 29px; position:absolute;left:16px;top:11px;}.toast-content{padding-left:58px!important;font-weight: 500!important;margin-top:1px!important;}';
      toast.shadowRoot.appendChild(style);
    }
    return toast.present();
  }

}
