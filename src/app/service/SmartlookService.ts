import { Injectable } from '@angular/core';
import { Smartlook, SmartlookSetupConfig } from '@ionic-native/smartlook/ngx';
import { SMARTLOOK_API_KEY } from '../config';
import { PlatformService } from './PlatformService';

@Injectable({
    providedIn: 'root'
}) export class SmartlookService {

    private config: SmartlookSetupConfig = {
        smartlookAPIKey: SMARTLOOK_API_KEY,
    } as any;

    constructor(
        private smartlook: Smartlook,
        private platform: PlatformService,
    ) {
    }

    public init(): void {
        if (this.platform.is('ios') === false) {
            // Smartlook zatim pouzivame pouze na iOS
            return;
        }

        this.smartlook.setupAndStartRecording(this.config);
    }
}
