import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class LoaderService {
    private loader: HTMLIonLoadingElement;

    constructor(
        public loadingCtrl: LoadingController,
    ) {
    }

    public async presentLoader(): Promise<void> {
        this.loader = await this.loadingCtrl.create({
            spinner: 'crescent'
        });
        return this.loader.present();
    }

    public dismissLoader(): Promise<boolean> {
        return this.loader && this.loader.dismiss();
    }

}
