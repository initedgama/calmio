import { Injectable } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { CourseShort } from '../pojo/courseShort';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ShareService {

  private SHARE_CONFIG: any = {
    url: this.translateSvc.instant('share-dialog.message.url'),
    subject: this.translateSvc.instant('share-dialog.message.title'),
    chooserTitle: this.translateSvc.instant('share-dialog.label-pickApp'),
    message: '',
  };

  constructor(
    private sharing: SocialSharing,
    private translateSvc: TranslateService,
  ) { }

  public async shareLessonFinished(
    totalMeditatedMinutes: number,
    lessonIndex: number,
    course: CourseShort
  ): Promise<void> {
    const opts = Object.assign({}, this.SHARE_CONFIG);
    opts.message = this.translateSvc.instant('share-dialog.message.text', {
      lessonNumber: lessonIndex + 1,
      courseName: course.name,
      totalMeditatedMinutes
    });
    return this.sharing.shareWithOptions(opts);
  }

}
