import { Injectable } from '@angular/core';
import { IonRouterOutlet, Platform } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class RouterOutletService {
    private routerOutlet: IonRouterOutlet;

    constructor(
        private platform: Platform
    ) { }

    public init(routerOutlet: IonRouterOutlet): void {
        if (!this.platform.is('ios')) {
            return;
        }
        this.routerOutlet = routerOutlet;
        this.routerOutlet.swipeGesture = true;
    }

    get swipebackEnabled(): boolean {
        if (this.routerOutlet) {
            return this.routerOutlet.swipeGesture;
        } else {
            throw new Error('Call init() first!');
        }
    }

    set swipebackEnabled(value: boolean) {
        if (!this.platform.is('ios')) {
            return;
        }
        if (this.routerOutlet) {
            this.routerOutlet.swipeGesture = value;
        } else {
            throw new Error('Call init() first!');
        }
    }

    set blur(shouldBlur: boolean) {
        if (shouldBlur) {
            document.querySelector('ion-router-outlet').classList.add('modal-show-prepare');
            setTimeout(() => {
                document.querySelector('ion-router-outlet').classList.add('modal-shown');
            });
        } else {
            document.querySelector('ion-router-outlet').classList.remove('modal-shown');
            setTimeout(() => {
                document.querySelector('ion-router-outlet').classList.remove('modal-show-prepare');
            }, 300);
        }
    }
}
