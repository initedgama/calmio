import { Injectable, ErrorHandler } from '@angular/core';
import { SENTRY_DSN } from '../config';

declare var Sentry: any;

@Injectable({
  providedIn: 'root'
}) export class SentryService {

  public initialize(): void {
    Sentry.init({
      dsn: SENTRY_DSN,
      dist: (window as any).SENTRY_RELEASE.id,
    });
  }

  public captureException(error: any): void {
    try {
      if (error && error.errorMessage && error.errorCode) {
        Sentry.captureException(new Error(`${error.errorCode} - ${error.errorMessage}`));
      } else {
        Sentry.captureException(error.originalError || error.error || error);
      }
    } catch (e) {
      console.error(e);
    }
  }
}


export class SentryIonicErrorHandler extends ErrorHandler {
  public handleError(error: any): void {
    super.handleError(error);
    if (error.promise && error.rejection) {
      // Promise rejection wrapped by zone.js
      error = error.rejection;
    }
    try {
      Sentry.captureException(error.originalError || error.error || error);
    } catch (e) {
      console.error(e);
    }
  }
}
