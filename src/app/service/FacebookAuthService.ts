import { Injectable } from '@angular/core';
import { Facebook } from '@ionic-native/facebook/ngx';
import { ExternalProviderUser } from '../pojo/externalProviderUser';
import { OAuthProviderEnum } from '../pojo/oAuthProviderEnum';

@Injectable()
export class FacebookAuthService {
  constructor(private fb: Facebook) { }

  public async signIn(retries: number = 0): Promise<ExternalProviderUser> {
    try {
      await this.signOut();
      const result = await this.fb.login(['email', 'public_profile']);
      if (result && result.authResponse && result.authResponse.accessToken) {
        const user: ExternalProviderUser = {
          type: OAuthProviderEnum.FACEBOOK,
          oauth_id: result.authResponse.accessToken
        };
        return user;
      } else {
        throw new Error('Facebook login - unknown error');
      }
    } catch (err) {
      console.log(err);
      if (
        err &&
        err.errorMessage &&
        typeof err.errorMessage === 'string' &&
        err.errorMessage.includes('CONNECTION_FAILURE') &&
        retries < 10
      ) {
        // Facebook login obcas nahodne vraci tuto chybu, proto zkusit retry max 10x
        return await this.signIn(retries + 1);
      } else {
        if (err && err.errorMessage) {
          err.errorMessage = err.errorMessage.replace(/^.+message]: /g, '').replace('[extra]: ', '');
        }
        throw err;
      }
    }
  }

  public async signOut(): Promise<void> {
    try {
      await this.fb.logout();
    } catch (err) {
      console.log('FB SIGN OUT ERROR', err);
    }
  }
}
