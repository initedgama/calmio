import { Injectable } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ApiProvider } from '../provider/apiProvider';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from '@ionic/angular';
import { Market } from '@ionic-native/market/ngx';
import * as semverGt from 'semver/functions/gt';
import { PlatformService } from './PlatformService';

const REPEAT_ALERT_TRESHOLD_MS = 12 * 3600 * 1000;

@Injectable({
    providedIn: 'root'
})
export class AppVersionService {

    private packageName: string = 'cz.calmio.app';
    private appStoreAppId: string = 'id1476384493';
    private alertDisplayed: boolean = false;
    private lastAlertTimestamp: number = 0;

    constructor(
        private appVersion: AppVersion,
        private apiProv: ApiProvider,
        private alerts: AlertController,
        private translateSvc: TranslateService,
        private market: Market,
        private platform: PlatformService,
    ) {
    }

    public initialize(): void {
        this.platform.resume.subscribe(() => {
            this.checkVersionMaybeAlertWhenOutdated();
        });
        this.checkVersionMaybeAlertWhenOutdated();
    }

    private async checkVersionMaybeAlertWhenOutdated(): Promise<void> {
        try {
            const currentVersion = await this.appVersion.getVersionNumber();
            const { latestVersion, forceUpdateVersion } = await this.getLatestVersion().toPromise();
            const now = Date.now();
            const softUpdate = latestVersion && semverGt(latestVersion, currentVersion);
            const forceUpdate = forceUpdateVersion && semverGt(forceUpdateVersion, currentVersion);
            if (
                !this.alertDisplayed
                && (forceUpdate
                    || (softUpdate && (now - this.lastAlertTimestamp) > REPEAT_ALERT_TRESHOLD_MS))
            ) {
                this.lastAlertTimestamp = now;
                this.alertOutdated(forceUpdate);
            }
        } catch (e) {
            console.log('checkVersionAlertWhenOutdated error', e);
        }
    }

    private getLatestVersion(): Observable<{ latestVersion: string, forceUpdateVersion: string }> {
        return this.apiProv.callRequest('app-version', 'get', undefined, true);
    }

    private async alertOutdated(forceUpdate: boolean): Promise<void> {
        const buttons: any = [{
            text: this.translateSvc.instant('common.alert-appOutdated.button-update'),
            handler: () => {
                this.openMarketplace();
                return false;
            },
        }];
        if (!forceUpdate) {
            buttons.push({
                text: this.translateSvc.instant('common.alert-appOutdated.button-cancel'),
            });
        }
        this.alertDisplayed = true;
        const alert = await this.alerts.create({
            header: this.translateSvc.instant('common.alert-appOutdated.title'),
            message: this.translateSvc.instant('common.alert-appOutdated.text'),
            buttons,
            backdropDismiss: !forceUpdate,
        });
        await alert.present();
        await alert.onDidDismiss();
        this.alertDisplayed = false;
    }

    private openMarketplace(): void {
        this.market.open(this.platform.is('ios') ? this.appStoreAppId : this.packageName);
    }
}
