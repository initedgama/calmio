import { Injectable, NgZone } from '@angular/core';
import { PromoService } from './PromoService';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { PromoCodeType } from '../pojo/promoCodeTypeEnum';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';
import { AnalyticsProvider } from '../provider/analyticsProvider';
import { PlatformService } from './PlatformService';

@Injectable({
    providedIn: 'root'
}) export class DeeplinkService {

    constructor(
        private promoService: PromoService,
        private deeplinks: Deeplinks,
        private ngZone: NgZone,
        private platform: PlatformService,
        private dynamicLinks: FirebaseDynamicLinks,
        private analyticsProvider: AnalyticsProvider,
    ) {
    }

    public initialize(): void {
        if (!this.platform.is('cordova')) {
            return;
        }
        this.initDeeplinks();
        this.initDynamicLinks();
    }

    private initDeeplinks = () => {
        this.deeplinks.route({})
            .subscribe(
                () => { },
                evt => {
                    this.onDeeplinkOpen(evt);
                    // always resubscribe due to plugin issue, https://github.com/ionic-team/ionic-plugin-deeplinks/issues/77
                    this.initDeeplinks();
                },
            );
    }

    private initDynamicLinks = () => {
        this.dynamicLinks.onDynamicLink().subscribe(evt => {
            console.log('Dynamic link opened', evt);
            const url = evt.deepLink;
            const split = url.split('?');
            this.onDeeplinkOpen({
                $link: { path: split[0], queryString: split[1] }
            });
        });
    }

    private onDeeplinkOpen = (
        event: { $link: { path: string, queryString: string } }
    ) => {
        console.log(`Deeplink opened:`, event);
        const link = event.$link;
        this.ngZone.run(() => {
            if (link.path.includes('partner') || link.path.includes('voucher')) {
                // Partnersky promo kod nebo darkovy voucher
                const code = this.promoCodeFromPath(link.path);
                const type = link.path.includes('partner') ? PromoCodeType.PARTNER : PromoCodeType.VOUCHER;
                if (!code) {
                    console.warn(`onPromoDeeplinkOpen error: missing code`);
                    return;
                }
                this.promoService.onPromoDeeplinkOpen({
                    type,
                    name: code,
                });
                this.analyticsProvider.reportCampaign({
                    source: 'deeplink',
                    medium: 'referral',
                    campaign: type,
                    content: code
                });
            } else if (link.queryString && link.queryString.includes('utm_campaign')) {
                // deeplink pro identifikaci kampane
                const params = new URLSearchParams(link.queryString);
                this.analyticsProvider.reportCampaign({
                    source: params.get('utm_source'),
                    medium: params.get('utm_medium'),
                    campaign: params.get('utm_campaign'),
                    content: params.get('utm_content')
                });
            } else {
                console.warn(`Deeplink not matched: ${link}`);
            }
        });
    }

    private promoCodeFromPath = (path: string): string => {
        const split = path.split('/');
        const code = split[split.length - 1] ? split[split.length - 1] : split[split.length - 2];
        return code;
    }

}
