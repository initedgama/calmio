import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInPage } from './views/sign-in/sign-in.page';
import { SignUpPage } from './views/sign-up/sign-up.page';
import { IntroPage } from './views/intro/intro.page';
import { TermsPage } from './views/terms/terms.page';
import { WelcomePage } from './views/welcome/welcome.page';
import { ListPage } from './views/list/list.page';
import { CourseDetailPage } from './views/course-detail/course-detail.page';
import { PlaybackPage } from './views/playback/playback.page';
import { CoursePurchasePage } from './views/course-purchase/course-purchase.page';
import { ProfilePage } from './views/profile/profile.page';
import { FullVersionPage } from './views/full-version/full-version.page';
import { AboutPage } from './views/about/about.page';
import { InitPage } from './views/init/init.page';
import { AppInitService } from './service/AppInitService';
import { VerifyEmailPage } from './views/verify-email/verify-email.page';
import { FeedbackPage } from './views/feedback/feedback.page';
import { OnboardingSlidesPage } from './views/onboarding-slides/onboarding-slides.page';
import { OnboardingVideoPage } from './views/onboarding/onboarding-video/onboarding-video.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'init',
    pathMatch: 'full'
  },
  {
    path: 'init',
    component: InitPage
  },
  {
    path: '',
    canActivate: [AppInitService],
    children: [
      {
        path: 'intro',
        component: IntroPage
      },
      {
        path: 'sign-in',
        component: SignInPage
      },
      {
        path: 'sign-up',
        component: SignUpPage
      },
      {
        path: 'terms',
        component: TermsPage
      },
      {
        path: 'welcome',
        component: WelcomePage
      },
      {
        path: 'list',
        component: ListPage
      },
      {
        path: 'course-detail',
        component: CourseDetailPage
      },
      {
        path: 'playback',
        component: PlaybackPage
      },
      {
        path: 'course-purchase',
        component: CoursePurchasePage
      },
      {
        path: 'profile',
        component: ProfilePage
      },
      {
        path: 'full-version',
        component: FullVersionPage
      },
      {
        path: 'about',
        component: AboutPage
      },
      {
        path: 'onboarding-slides',
        component: OnboardingSlidesPage,
      },
      {
        path: 'onboarding-video',
        component: OnboardingVideoPage
      },
      {
        path: 'verify-email',
        component: VerifyEmailPage
      },
      {
        path: 'feedback',
        component: FeedbackPage
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
