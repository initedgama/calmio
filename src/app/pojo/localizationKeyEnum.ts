export enum LocalizationKeyEnum {
    USER_ALREADY_EXISTS = 'user_already_exists',
    FACEBOOK_AUTH_MISSING_EMAIL = 'facebook_auth_missing_email',
    INVALID_CREDENTIALS = 'invalid_credentials',
    PROMO_CODE_INVALID = 'promo_code_invalid',
}
