import { CourseOwned } from './courseOwned';
import { InstallReasonEnum } from './installReasonEnum';
import { ExperienceAnswerEnum } from './experienceAnswerEnum';
import { VoiceGender } from './voiceGender';
import { Role } from './role';
import { InAppPurchaseSubscription } from './inAppPurchaseSubscription';
import { AuthType } from './authTypeEnum';
import { UserPromoCode } from './userPromoCode';

export interface User {
    email: string;
    auth: AuthType[];
    name: string;
    surname: string;
    coursesOwned: CourseOwned[];
    activeSubscriptions: InAppPurchaseSubscription[];
    installReason?: InstallReasonEnum[];
    experience?: ExperienceAnswerEnum;
    language?: string;
    preferredVoice?: VoiceGender;
    roles?: Array<Role>;
    pushNotification?: boolean;
    verificationRequired: boolean;
    promo: UserPromoCode;
}
