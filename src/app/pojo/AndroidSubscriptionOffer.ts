export class AndroidSubscriptionOffer {
    public className: string;
    public id: string;
    public platform: string;
    public token: string;
    public productId: string;
    public pricingPhases: AndroidPricingPhase[];
}

export class AndroidPricingPhase {
    public billingCycles: number;
    public billingPeriod: string;
    public currency: string;
    public paymentMode: string;
    public price: string;
    public priceMicros: number;
    public recurrenceMode: string;
}
