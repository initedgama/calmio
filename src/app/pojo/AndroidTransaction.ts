export class AndroidTransaction {
    public className: string;
    public isAcknowledged: boolean;
    public isPending: boolean;
    public nativePurchase: AndroidPurchase;
    public platform: string;
    public products: Array<{ id: string }>;
    public purchaseDate: Date;
    public purchaseId: string;
    public renewalIntent: string;
    public state: string;
    public transactionId: string;
}

export class AndroidPurchase {
    public accountId: string;
    public acknowledged: boolean;
    public autoRenewing: boolean;
    public developerPayload: string;
    public getPurchaseState: number;
    public orderId: string;
    public packageName: string;
    public productId: string;
    public productIds: string[];
    public profileId: string;
    public purchaseState: number;
    public purchaseTime: number;
    public purchaseToken: string;
    public quantity: number;
    public receipt: string;
    public signature: string;
}
