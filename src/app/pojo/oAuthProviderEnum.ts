export enum OAuthProviderEnum {
    FACEBOOK = 'FACEBOOK',
    GOOGLE = 'GOOGLE',
    APPLE = 'APPLE',
}
