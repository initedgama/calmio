export enum AnalyticsUserPropertyEnum {
    HASH = 'hash', // hash emailu uzivatele
    VOICE = 'preferredVoice', // preferovany hlas z onboardingu
    EXPERIENCE_ANSWER = 'experience_answer', // zkusenost s meditaci z onboardingu
    INSTALL_REASONS = 'install_reasons', // duvody instalace z onboardingu
    TOTAL_TIME_MEDITATED = 'total_time_meditated', // promeditovane minuty celkem
}
