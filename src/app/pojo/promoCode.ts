import { PromoCodeType } from './promoCodeTypeEnum';

export interface PromoCode {
    type: PromoCodeType;
    name: string;
}
