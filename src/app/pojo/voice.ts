import { VoiceGender } from './voiceGender';

export interface Voice {
    id: number;
    gender: VoiceGender;
    name: string;
    previewUrl: string;
}
