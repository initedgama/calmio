export interface CourseOwned {
    id: string;
    lessonsFinished: {
        courseId: string;
        id: string;
        time: number;
        completed: boolean;
        currentSessionCompleted: boolean;
        unfinishedTime: number;
        timestamp: number;
    }[];
}
