export enum AnalyticsEventsEnum {
    APP_OPEN = 'app_open', // otevreni aplikace
    LOGIN = 'login', // prihlaseni
    LOGOUT = 'logout', // odhlaseni
    SIGN_UP = 'sign_up', // registrace
    TUTORIAL_BEGIN = 'tutorial_begin', // spusteni onboardingu
    TUTORIAL_COMPLETE = 'tutorial_complete', // ukonceni onboardingu
    COURSE_PURCHASE = 'course_purchase', // prihlaseni na kurz
    SELECT_COURSE_OWNED_DETAIL = 'select_course_owned_detail', // vstup na detail prihlaseneho kurzu
    SELECT_COURSE_PURCHASE_DETAIL = 'select_course_purchase_detail', // vstup na detail neprihlaseneho kurzu
    SELECT_LESSON_DETAIL = 'select_lesson_detail', // vstup na detail lekce = prehravani
    LESSON_STARTED = 'lesson_started', // spusteni lekce
    LESSON_FINISHED = 'lesson_finished', // dokonceni lekce
    LESSON_UNFINISHED = 'lesson_unfinished', // opusteni lekce pred dokoncenim
    SHARE = 'share', // sdileni dokoncene lekce
}
