export interface SignUpPasswordApiRequest {
    email: string;
    password: string;
    name: string;
    surname: string;
    language: string;
}
