export enum AuthType {
    PASSWORD = 'password',
    FACEBOOK = 'facebook',
    GOOGLE = 'google',
}
