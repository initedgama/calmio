import { Source } from './source';

export interface Lesson {
    id: string;
    name: string;
    order: number;
    source: Source[];
    description: string;
    type: 'VIDEO' | 'AUDIO';
    intro?: {
        text?: any,
        video?: string;
    };
}
