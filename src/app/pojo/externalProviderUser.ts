import { AppleSignInResponse } from '@ionic-native/sign-in-with-apple/ngx';
import { OAuthProviderEnum } from './oAuthProviderEnum';

export interface ExternalProviderUser {
    type: OAuthProviderEnum;
    oauth_id: string;
    apple_auth_details?: AppleSignInResponse;
}
