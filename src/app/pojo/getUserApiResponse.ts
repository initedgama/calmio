import { ExperienceAnswerEnum } from './experienceAnswerEnum';
import { InstallReasonEnum } from './installReasonEnum';
import { CourseOwned } from './courseOwned';
import { VoiceGender } from './voiceGender';
import { Role } from './role';
import { AuthType } from './authTypeEnum';
import { UserPromoCode } from './userPromoCode';
import { InAppSubscriptionType } from './inAppSubscriptionTypeEnum';
import { InAppSubscriptionId } from './inAppSubscriptionIdEnum';

export interface GetUserApiResponse {
    email: string;
    name: string;
    auth: AuthType[];
    surname: string;
    coursesOwned: CourseOwned[];
    installReason?: InstallReasonEnum[];
    experience?: ExperienceAnswerEnum;
    language?: string;
    preferredVoice?: VoiceGender;
    roles?: Array<Role>;
    pushNotification?: boolean;
    verificationRequired: boolean;
    promo: UserPromoCode;
    subscriptions: {
        validTo: number;
        type: InAppSubscriptionType;
        productId: InAppSubscriptionId;
    }[];
}
