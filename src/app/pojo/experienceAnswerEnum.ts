export enum ExperienceAnswerEnum {
    NO_EXPERIENCE = 'NO_EXPERIENCE',
    MILD_EXPERIENCE = 'MILD_EXPERIENCE',
    LOTS_OF_EXPERIENCE = 'LOTS_OF_EXPERIENCE',
}
