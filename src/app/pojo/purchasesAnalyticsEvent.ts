import { InAppPurchaseSubscription } from './inAppPurchaseSubscription';

export interface PurchasesAnalyticsEvent {
    email: string;
    timestamp: number;
    subscriptions: InAppPurchaseSubscription[];
    platform: 'ios' | 'android';
}
