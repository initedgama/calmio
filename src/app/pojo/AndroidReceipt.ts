import { AndroidTransaction } from './AndroidTransaction';

export class AndroidReceipt {
    public orderId: string;
    public platform: string;
    public purchaseToken: string;
    public transactions: AndroidTransaction[];
    public className: string;
}

