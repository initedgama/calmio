import { AppStoreReceipt, PlayStoreReceipt } from '@ionic-native/in-app-purchase-2/ngx';
import { AndroidSubscriptionOffer } from './AndroidSubscriptionOffer';

// Android
export interface InAppPurchasePluginProduct {
    productId: string;
    title: string;
    description: string;
    price: string;
    currency: string;
    priceAsDecimal: number;
    purchaseable: boolean;
    state?: string;
    transaction?: PlayStoreReceipt | AppStoreReceipt;

    getOffer(): AndroidSubscriptionOffer;
}
