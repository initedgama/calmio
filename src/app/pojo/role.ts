export enum Role {
    USER = 'USER',
    TEST = 'TEST',
    ADMIN = 'ADMIN'
}
