import { CourseShort } from './courseShort';

export interface CoursesAllApiResponse {
    dailyMeditations: CourseShort[];
    coursesAll: CourseShort[];
}
