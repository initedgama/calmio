import { AnalyticsEventsEnum } from './analyticsEventsEnum';

export interface AnalyticsEvent {
    name: AnalyticsEventsEnum;
    params?: { [key: string]: any };
}
