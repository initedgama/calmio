export enum InAppSubscriptionId {
    MONTHLY = 'subscription1',
    MONTHLY_2 = 'subscription_monthly',
    MONTHLY_3 = 'subscription_monthly_11_2021',
    ANNUAL = 'subscription_annual',
    LIFETIME = 'lifetime_full_version',
    MONTHLY_TEST = 'short_renewable_test',
    ANNUAL_TEST = 'calmio_medium_renewable_test',
    ANNUAL_TEST_2 = 'medium2_renewable_test',
    LIFETIME_TEST = 'lifetime_test',
}
