export enum AnalyticsLoginMethod {
    UNKNOWN = 'UNKNOWN',
    EMAIL = 'EMAIL',
    FACEBOOK = 'FACEBOOK',
    GOOGLE = 'GOOGLE',
    APPLE = 'APPLE',
}
