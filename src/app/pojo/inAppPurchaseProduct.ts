import { InAppSubscriptionId } from './inAppSubscriptionIdEnum';
import { InAppSubscriptionType } from './inAppSubscriptionTypeEnum';
import { AndroidSubscriptionOffer } from './AndroidSubscriptionOffer';

// iOS
export interface InAppPurchaseProduct {
    productId: InAppSubscriptionId;
    type: InAppSubscriptionType;
    title: string;
    description: string;
    currency: string;
    price: string;
    priceAsDecimal: number;
    purchaseable: boolean;

    // Nepouziva se, ale aby nekricel kompiler, ze mu ta funkce chybi
    getOffer(): AndroidSubscriptionOffer;
}
