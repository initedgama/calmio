import { CourseTypeEnum } from './courseTypeEnum';
import { Lesson } from './lesson';
export interface CourseDetail {
    type: CourseTypeEnum;
    id: string;
    free: boolean;
    name: string;
    description: string;
    subtitle: string;
    titlePhoto: string;
    thumbnail: string;
    tags: string[];
    lessons: Lesson[];
    languages?: string[];
}
