export interface SignInPasswordApiRequest {
    email: string;
    password: string;
}
