export enum InAppSubscriptionType {
    MONTHLY = 'MONTHLY',
    ANNUAL = 'ANNUAL',
    LIFETIME = 'LIFETIME',
}
