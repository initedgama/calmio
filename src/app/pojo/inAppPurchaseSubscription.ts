import { InAppSubscriptionId } from './inAppSubscriptionIdEnum';
import { InAppSubscriptionType } from './inAppSubscriptionTypeEnum';

export interface InAppPurchaseSubscription {
    id: InAppSubscriptionId;
    type: InAppSubscriptionType;
    expirationTimestamp: number;
    receipt: any;
}
