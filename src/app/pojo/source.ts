import { Voice } from './voice';
import { VoiceGender } from './voiceGender';

export interface Source {
    title: string;
    src: string;
    voice: VoiceGender;
    lecturer: Voice;
}
