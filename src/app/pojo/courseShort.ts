import { CourseTypeEnum } from './courseTypeEnum';
import { Lesson } from './lesson';
export interface CourseShort {
    type: CourseTypeEnum;
    id: string;
    name: string;
    subtitle: string;
    description: string;
    thumbnail: string;
    titlePhoto: string;
    tags: string[];
    free: boolean;
    lessons: Lesson[];
    languages?: string[];
}
