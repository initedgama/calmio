import { PromoCodeType } from './promoCodeTypeEnum';

export interface UserPromoCode {
    name: string;
    type: PromoCodeType;
    partnerName: string;
    expires: number;
}
