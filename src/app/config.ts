import { InAppSubscriptionId } from './pojo/inAppSubscriptionIdEnum';
import { InAppSubscriptionType } from './pojo/inAppSubscriptionTypeEnum';


// prepinani mezi testovacimi a produkcnimi produkty
export const InAppPurchaseTesting = false;

export const Config: any = {
    availableLanguages: [
        'cs',
        // 'sk', 'en', 'pl', 'hu'
    ],
    hideFullVersion: false, // skryje moznost zakoupit plnou verzi
    errorMessageMap: {
        generic: {
            default: 'common.error.toast-default',
            noInternetAccess: 'common.error.toast-connection',
            status: {
                exact: {
                    401: 'common.error.toast-userLoggedOut'
                },
                startsWith: {
                },
            },
            message: {
                // 'Invalid old password.': 'error.invalidOldPassword',
                // 'Bad credentials': 'error.badCredentials',
                // 'User already exists': 'error.userExists'
            }
        },
    },
    // chybove hlasky z API, ktere se zobrazi v alertu namisto defaultniho toastu
    apiErrorsWithAlert: [
        'facebook_auth_missing_email',
        'promo_code_invalid',
        'promo_code_already_used',
        'promo_another_code_active',
        'promo_code_cannot_be_used',
    ],
    inAppPurchasesProductIds: InAppPurchaseTesting
        ? [InAppSubscriptionId.MONTHLY_TEST, InAppSubscriptionId.ANNUAL_TEST, InAppSubscriptionId.ANNUAL_TEST_2,
            InAppSubscriptionId.LIFETIME_TEST]
        : [InAppSubscriptionId.MONTHLY, InAppSubscriptionId.MONTHLY_2, InAppSubscriptionId.MONTHLY_3,
        InAppSubscriptionId.ANNUAL, InAppSubscriptionId.LIFETIME],
    // Nove jsou aktivni jen lifetime a rocni nakupy. Pokud ale uzivatele maji stare mesicni, tak je muzou dal pouzivat.
    inAppPurchasesActiveProductIds: InAppPurchaseTesting
        ? [InAppSubscriptionId.ANNUAL_TEST, InAppSubscriptionId.ANNUAL_TEST_2, InAppSubscriptionId.LIFETIME_TEST]
        : [InAppSubscriptionId.ANNUAL, InAppSubscriptionId.LIFETIME],
    inAppPurchaseTypes: {
        [InAppSubscriptionId.MONTHLY]: InAppSubscriptionType.MONTHLY,
        [InAppSubscriptionId.MONTHLY_2]: InAppSubscriptionType.MONTHLY,
        [InAppSubscriptionId.MONTHLY_3]: InAppSubscriptionType.MONTHLY,
        [InAppSubscriptionId.MONTHLY_TEST]: InAppSubscriptionType.MONTHLY,
        [InAppSubscriptionId.ANNUAL]: InAppSubscriptionType.ANNUAL,
        [InAppSubscriptionId.ANNUAL_TEST]: InAppSubscriptionType.ANNUAL,
        [InAppSubscriptionId.ANNUAL_TEST_2]: InAppSubscriptionType.ANNUAL,
        [InAppSubscriptionId.LIFETIME]: InAppSubscriptionType.LIFETIME,
        [InAppSubscriptionId.LIFETIME_TEST]: InAppSubscriptionType.LIFETIME,
    },
    inAppPurchaseVerificationUrlIOS: 'https://buy.itunes.apple.com/verifyReceipt',
    inAppPurchaseVerificationUrlIOS_sandbox: 'https://sandbox.itunes.apple.com/verifyReceipt',
    inAppPurchaseSharedSecretIOS: 'e30a1c3aaa4246eabfd2fd92db0a1237',
    syncIntervalMs: 10000, // jak casto zkouset na pozadi synchronizovat data o dokoncenych lekcich
    // minimalni interval pro pokus o stazeni aktualnich dat o kurzech a uzivateli ze serveru,
    // pokud aplikace neni mezitim restartovana
    minContentServerReloadIntervalMs: 12 * 60 * 60 * 1000,
    premiumUserBlogUrl: 'https://www.calmio.cz/blog-platici',
    freeUserBlogUrl: 'https://www.calmio.cz/blog-neplatici',
    consultationUrl: 'https://www.calmio.cz/rezervace',
};

export const SENTRY_DSN = 'https://82c4b06a2528475680a513dfacd154c0@o329963.ingest.sentry.io/5412989';

// Calmio account pro smartlook
export const SMARTLOOK_API_KEY = 'd885321b1639a419ad5d669733829b2d5629302b';

export const ONESIGNAL_APP_KEY = '528246a8-8747-4057-9300-7f7131e4a7b1';
