#!/bin/sh
APP=platforms/android/app/build/outputs/bundle/release/app-release.aab
VERSION=`grep -e "widget.*version" config.xml | sed "s/.*version=\"\([0-9.]*\)\".*/\1/;s/\./_/g"`
export SENTRY_SKIP_AUTO_RELEASE=true
export SENTRY_RELEASE=$VERSION-adist

ionic cordova build --prod --release --device android

rm -f release.aab
mv $APP release.aab
